import React from "react";
import { Route, BrowserRouter, Switch } from "react-router-dom";

import Dashboard from "./pages/Dashboard";
import BscRoute from "./routes/bsc";
import Page404 from "./pages/Page404";
import "./App.css";

function App() {
  return (
    <BrowserRouter basename="/qc">
      <Switch>
        <Route exact path="/">
          <Dashboard />
        </Route>
        <Route path="/bsc" component={BscRoute} />

        <Route path="*">
          <Page404 />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
