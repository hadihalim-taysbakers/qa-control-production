import React from "react";
import Main from "../layout/main";
class Page404 extends React.Component {
  render() {
    return (
      <Main>
        <h1>404 Page Not Found</h1>
      </Main>
    );
  }
}
export default Page404;
