import React from "react";
import moment from "moment";
import "moment/locale/id";
import axios from "axios";
import config from "../../../config";
import Field from "../../../components/Field";
import * as Validator from "validatorjs";
import Form from "./../../../components/Form";
import Label from "./../../../components/Label";
import "./../../../App.css";

class rmForm extends React.Component {
  constructor(props) {
    super(props);
    this.fields = {
      waktu_cuci_start: {
        name: "waktu_cuci_start",
        id: "waktu_cuci_start",
        label: "Start",
        attributeName: "Start",
        type: "time",
        rules: ["required"],
      },
      waktu_cuci_stop: {
        name: "waktu_cuci_stop",
        id: "waktu_cuci_stop",
        label: "End",
        attributeName: "End",
        type: "time",
        rules: ["required"],
      },
      fisik_utuh: {
        name: "fisik_utuh",
        id: "fisik_utuh",
        label: "Utuh ",
        attributeName: "Utuh ",
        type: "number",
        min: 0,
        max: 100,
        step: 1,
        rules: ["required", "numeric"],
      },
      fisik_busuk: {
        name: "fisik_busuk",
        id: "fisik_busuk",
        label: "Busuk",
        attributeName: "Busuk",
        type: "number",
        min: 0,
        max: 100,
        step: 1,
        rules: ["required", "numeric"],
      },

      organ_warna: {
        name: "organ_warna",
        id: "organ_warna",
        label: "Warna ",
        attributeName: "Warna",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      organ_bau: {
        name: "organ_bau",
        id: "organ_bau",
        label: "Bau",
        attributeName: "Bau",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      organ_bentuk: {
        name: "organ_bentuk",
        id: "organ_bentuk",
        label: "Bentuk ",
        attributeName: "Bentuk",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      waktu_masak_start: {
        name: "waktu_masak_start",
        id: "waktu_masak_start",
        label: "Start",
        attributeName: "Start",
        type: "time",
        rules: ["required"],
      },
      waktu_masak_stop: {
        name: "waktu_masak_stop",
        id: "waktu_masak_stop",
        label: "End",
        attributeName: "End",
        type: "time",
        rules: ["required"],
      },
      suhu_setting: {
        name: "suhu_setting",
        id: "suhu_setting",
        label: "Setting",
        attributeName: "Setting",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric"],
      },
      suhu_actual: {
        name: "suhu_actual",
        id: "suhu_actual",
        label: "Actual",
        attributeName: " Actual",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric"],
      },
      kontaminasi_logam: {
        name: "kontaminasi_logam",
        id: "kontaminasi_logam",
        label: "Kontaminasi Logam",
        attributeName: "Kontaminasi Logam",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      kontaminasi_plastik: {
        name: "kontaminasi_plastik",
        id: "kontaminasi_plastik",
        label: "Kontaminasi Plastik",
        attributeName: "Kontaminasi Plastik",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },

      kontaminasi_benang: {
        name: "kontaminasi_benang",
        id: "kontaminasi_benang",
        label: "Kontaminasi Benang",
        attributeName: "Kontaminasi Benang",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      kontaminasi_others: {
        name: "kontaminasi_others",
        id: "kontaminasi_others",
        label: "Kontaminasi Lainnya",
        attributeName: "Kontaminasi Lainnya",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },

      // kondisi: {
      //   name: "kondisi",
      //   id: "kondisi",
      //   label: "Empuk",
      //   attributeName: "Empuk",
      //   type: "checkbox",
      //   values: { 0: "0", 1: "1" },
      // },
      kondisi: {
        name: "kondisi",
        label: "Kondisi Fisik (√)",
        id: "kondisi",
        attributeName: "Kondisi Fisik",
        type: "radio",
        values: { empuk: "Empuk", keras: "Keras" },
        rules: ["required"],
      },
      status: {
        name: "status",
        id: "status",
        label: "Status",
        attributeName: "Status",
        type: "checkbox",

        values: { 0: "0", 1: "1" },
      },
      keterangan: {
        name: "keterangan",
        id: "keterangan",
        label: "Keterangan",
        attributeName: "Keterangan",
        type: "text",
      },
    };
    this.state = {
      loading: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  getFields() {
    const fields = { ...this.fields };

    return fields;
  }
  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState(
      {
        datas: { ...this.state.datas, [name]: value },
      },
      () => {
        const valid = this.validateField(name, value);
        if (valid.valid === true) {
          this.setState({
            errors: { ...this.state.errors, [name]: [] },
          });
        } else {
          this.setState({
            errors: { ...this.state.errors, [name]: valid.errors[name] },
          });
        }
      }
    );
  }
  validateField(name, value) {
    const messages = Validator.getMessages("id");
    Validator.setMessages("id", messages);
    Validator.setAttributeNames(this.attributeNames);
    const validation = new Validator(
      { [name]: value },
      { [name]: this.rules[name] },
      messages
    );
    if (validation.passes()) {
      return { valid: true, errors: null };
    } else {
      const errors = validation.errors.all();
      return {
        valid: false,
        errors: errors,
      };
    }
  }
  InsertRecord(datas) {
    let formData = new FormData();
    for (const key in this.props.header) {
      formData.append(key, this.props.header[key]);
    }
    for (const key in datas) {
      formData.append(key, datas[key]);
    }
    this.setState({ loading: "insert" }, () => {
      axios({
        method: "post",
        url: config.api + config.path.rm,
        data: formData,
        params: {
          tanggal: this.state.tanggal,
          shiftr: this.state.shiftr,
          bahan: this.state.bahan,
        },
        config: { headers: { "Content-Type": "multipart/form-data" } },
      })
        .then(
          function (response) {
            this.setState({ loading: false }, () => {
              this.props.onAfterInsert(response.data.datas);
              alert("Berhasil untuk menyimpan data");
            });
          }.bind(this)
        )

        .catch((e) => {
          this.setState({ loading: false });
          alert("Gagal untuk menyimpan data");
        });
    });
  }
  DeleteRecord() {
    if (this.props.data && this.props.data.id_rm_detail) {
      this.setState({ loading: "delete" }, () => {
        axios
          .delete(config.api + config.path.rm, {
            params: { idDetail: this.props.data.id_rm_detail },
          })
          .then((res) => {
            if (res.data.success) {
              this.setState({ loading: false }, () => {
                this.props.onAfterDelete(res.data.idDetail);
                alert("Berhasil untuk menghapus data");
              });
            } else {
              alert("gagal delete");
            }
          });
      });
    }
  }
  EditRecord(datas) {
    if (this.props.data) {
      let formData = new FormData();
      for (const key in this.props.header) {
        formData.append(key, this.props.header[key]);
      }
      for (const key in datas) {
        formData.append(key, datas[key]);
      }
      this.setState({ loading: "edit" }, () => {
        axios
          .patch(config.api + config.path.rm, formData, {
            params: {
              idDetail: this.props.data.id_rm_detail,
            },
          })
          .then(
            function (response) {
              this.setState({ loading: false }, () => {
                if (response.data.success) {
                  this.props.onAfterEdit(response.data.data);
                  alert("Berhasil untuk merubah data");
                } else {
                  alert("Gagal untuk menyimpan data");
                }
              });
            }.bind(this)
          )
          .catch((e) => {
            this.setState({ loading: false });
            alert("Gagal untuk menyimpan data");
          });
      });
    }
  }
  onSubmit(datas) {
    if (this.props.data) {
      this.EditRecord(datas);
    } else {
      this.InsertRecord(datas);
    }
  }
  render() {
    const fields = this.getFields();
    return (
      <div
        className={this.props.show ? "modal fade show" : "modal"}
        style={this.props.show ? { display: "block" } : undefined}
        tabIndex="-1"
        role="dialog">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            {this.props.data === undefined ? undefined : (
              <>
                <Form
                  fields={fields}
                  onSubmit={this.onSubmit}
                  loading={this.state.loading}
                  defaultValues={this.props.data ? this.props.data : {}}>
                  <div className="modal-header">
                    <h5 className="modal-title">
                      {this.props.data !== undefined
                        ? this.props.data &&
                          this.props.data.id_rm_detail === null
                          ? "EDIT"
                          : "INSERT"
                        : ""}
                      PEMERIKSAAN PENCUCIAN & PEMASAKAN BAHAN BAKU
                    </h5>
                    {this.props.onClose ? (
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        onClick={this.props.onClose}>
                        <span aria-hidden="true">&times;</span>
                      </button>
                    ) : undefined}
                  </div>
                  <div className="modal-body">
                    <div className="container">
                      <div className="row mb-3">
                        <div className="col-sm-4">
                          <label htmlFor="tanggal" className="form-label">
                            Tanggal :
                          </label>
                          <br />
                          <b>
                            {moment(this.props.header.tanggal)
                              .locale("id")
                              .format("dddd, DD MMM YYYY")}
                          </b>
                        </div>

                        <div className="col-sm-4">
                          <label htmlFor="" className="form-label">
                            Shift :
                          </label>
                          <br />
                          <b>{this.props.header.shiftLabel}</b>
                        </div>
                        <div className="col-sm-4">
                          <label htmlFor="bahan" className="form-label">
                            Bahan :
                          </label>
                          <br />
                          <b>{this.props.header.bahanLabel}</b>
                        </div>
                      </div>
                      <hr />
                      <div className="row mb-3">
                        <label className="col-sm-2">Waktu Cuci</label>
                        <Label
                          name="waktu_cuci_start"
                          className="col-sm-2 col-form-label text-right"
                        />
                        <div className="col-sm-3">
                          <Field
                            name="waktu_cuci_start"
                            className="form-control"
                          />
                        </div>
                        <Label
                          name="waktu_cuci_stop"
                          className="col-sm-2 col-form-label text-right"
                        />
                        <div className="col-sm-3">
                          <Field
                            name="waktu_cuci_stop"
                            className="form-control"
                          />
                        </div>
                      </div>

                      <div className="row mb-3">
                        <label className="col-sm-2">Kondisi Fisik</label>
                        <Label
                          name="fisik_utuh"
                          className="col-sm-2 col-form-label text-right"
                        />
                        <div className="col-sm-3">
                          <Field name="fisik_utuh" className="form-control" />
                        </div>
                        <Label
                          name="fisik_busuk"
                          className="col-sm-2 col-form-label text-right"
                        />
                        <div className="col-sm-3">
                          <Field name="fisik_busuk" className="form-control " />
                        </div>
                      </div>

                      <div className="row mb-3">
                        <label className="col-sm-2">Waktu Masak</label>
                        <Label
                          name="waktu_masak_start"
                          className="col-sm-2 col-form-label text-right"
                        />
                        <div className="col-sm-3">
                          <Field
                            name="waktu_masak_start"
                            className="form-control"
                          />
                        </div>
                        <Label
                          name="waktu_masak_stop"
                          className="col-sm-2 col-form-label text-right"
                        />
                        <div className="col-sm-3">
                          <Field
                            name="waktu_masak_stop"
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="row mb-3">
                        <label className="col-sm-2">Suhu Masak</label>
                        <Label
                          name="suhu_setting"
                          className="col-sm-2 col-form-label text-right"
                        />
                        <div className="col-sm-3 ">
                          <Field name="suhu_setting" className="form-control" />
                        </div>
                        <Label
                          name="suhu_actual"
                          className="col-sm-2 col-form-label text-right"
                        />
                        <div className="col-sm-3">
                          <Field name="suhu_actual" className="form-control" />
                        </div>
                      </div>
                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <table className="table table-bordered table-hovered table-striped table-hover ">
                            <thead>
                              <tr>
                                <th colSpan={12} align="center">
                                  Organoleptik
                                </th>
                              </tr>
                              <tr>
                                <th rowSpan={"2"} className="title vertical">
                                  <span>Color (Warna)</span>
                                </th>
                                <th rowSpan={"2"} className="title vertical">
                                  <span>Smell (Bau)</span>
                                </th>
                                <th rowSpan={"2"} className="title vertical">
                                  <span>Shape (Bentuk)</span>
                                </th>
                                <th colSpan={6} align="center">
                                  Visual / Kontaminasi (pcs)
                                </th>
                              </tr>
                              <tr>
                                <th align="center">Logam</th>
                                <th align="center">Plastik</th>
                                <th align="center">Benang</th>
                                <th align="center">Other</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td align="center">
                                  <Field
                                    name="organ_warna"
                                    className="form-check-input "
                                  />
                                </td>
                                <td align="center">
                                  <Field
                                    name="organ_bau"
                                    className="form-check-input "
                                  />
                                </td>
                                <td align="center">
                                  <Field
                                    name="organ_bentuk"
                                    className="form-check-input "
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="kontaminasi_logam"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="kontaminasi_plastik"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="kontaminasi_benang"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="kontaminasi_others"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row mb-3">
                        <Label name="kondisi" className="col-sm-3" />
                        <Field
                          name="kondisi"
                          className="col-sm-9"

                        />
                      </div>

                      <div className="row mb-3">
                        <Label name="status" className="col-sm-1 form-label " />
                        <br />
                        <div className="col-sm-1  ">
                          <Field name="status" className="form-check-input" />
                        </div>

                        <Label
                          name="keterangan"
                          className="col-sm-2 form-label text-right"
                        />
                        <br />
                        <div className="col-sm-8 ">
                          <Field name="keterangan" className="form-control" />
                        </div>
                      </div>

                      <div className="modal-footer">
                        {this.props.onClose ? (
                          <button
                            type="button"
                            className="btn btn-outline-secondary"
                            data-bs-dismiss="modal"
                            onClick={this.props.onClose}
                            disabled={this.state.loading}>
                            Close
                          </button>
                        ) : undefined}
                        {this.props.data !== undefined ? (
                          this.props.data === null ? (
                            <>
                              <button
                                type="reset"
                                className="btn btn-outline-warning"
                                disabled={this.state.loading}>
                                Reset
                              </button>
                              <button
                                className="btn btn-primary"
                                type="submit"
                                disabled={this.state.loading}>
                                {this.state.loading === "insert" ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm"
                                      role="status"
                                      aria-hidden="true"></span>
                                    <span className="sr-only">Saving...</span>
                                  </>
                                ) : (
                                  "Save"
                                )}
                              </button>
                            </>
                          ) : (
                            <>
                              <button
                                className="btn btn-danger"
                                type="button"
                                disabled={this.state.loading}
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.DeleteRecord();
                                }}>
                                {this.state.loading === "delete" ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm"
                                      role="status"
                                      aria-hidden="true"></span>
                                    <span className="sr-only">Deleting...</span>
                                  </>
                                ) : (
                                  "Delete"
                                )}
                              </button>
                              <button
                                type="reset"
                                className="btn btn-outline-warning"
                                disabled={this.state.loading}>
                                Reset
                              </button>
                              <button
                                className="btn btn-primary"
                                type="submit"
                                disabled={this.state.loading}>
                                {this.state.loading === "edit" ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm"
                                      role="status"
                                      aria-hidden="true"></span>
                                    <span className="sr-only">Saving...</span>
                                  </>
                                ) : (
                                  "Save"
                                )}
                              </button>
                            </>
                          )
                        ) : undefined}
                      </div>
                    </div>
                  </div>
                </Form>
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default rmForm;
