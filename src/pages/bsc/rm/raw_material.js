import React from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import Select from "react-select";
import Approval from "../../../Approval";
import { formatTime } from "../../../utils/formatter";
import config from "../../../config";
import RMForm from "./rmForm";
import Main from "../../../layout/main";
class raw_material extends React.Component {
  state = {
    formData: undefined,
    exists: [],
    approveCreator: undefined,
    approveVerifier: undefined,
    approveKnower: undefined,
    creator_name: undefined,
    creator_title: undefined,
    creator_sign: undefined,
    created: undefined,
    verifier_name: undefined,
    verifier_title: undefined,
    verifier_sign: undefined,
    verified: undefined,
    knower_name: undefined,
    knower_title: undefined,
    knower_sign: undefined,
    knowed: undefined,
    edit: undefined,
    loading: false,
    name: "",
    email: "",
    nodoc: "FRM.QCRD.BSC.02.01; Rev.02",
    tanggal: "",
    shiftr: "",
    bahan: "",
    waktu_cuci_start: "",
    waktu_cuci_stop: "",
    fisik_utuh: "",
    fisik_busuk: "",
    organ_warna: false,
    organ_bau: false,
    organ_bentuk: false,
    waktu_masak_start: "",
    waktu_masak_stop: "",
    suhu_setting: "",
    suhu_actual: "",
    kontaminasi_logam: "",
    kontaminasi_plastik: "",
    kontaminasi_benang: "",
    kontaminasi_others: "",
    kondisi: "",
    status: false,
    keterangan: "",
    datas: [],
    query: "",

    searchString: [],
    SelectShiftOptions: [],
    SelectBahanOptions: [],
    aquaticCreatures: [],
    backgroundColor: "backgroundColor: #078e72",
  };
  // oldEditData = undefined;
  componentDidUpdate(prevProps, prevState) {
    if (
      (prevState.tanggal !== this.state.tanggal ||
        prevState.shiftr !== this.state.shiftr ||
        prevState.bahan !== this.state.bahan) &&
      this.state.tanggal &&
      this.state.shiftr &&
      this.state.bahan
    ) {
      const url = config.api + config.path.rm;
      this.setState({ loading: true }, () => {
        axios
          .get(url, {
            params: {
              tanggal: this.state.tanggal,
              shiftr: this.state.shiftr,
              bahan: this.state.bahan,
            },
          })
          .then((response) => response.data)
          .then((data) => {
            this.setState({
              datas: data.datas,
              creator_name: data.header ? data.header.creator_name : undefined,
              creator_title: data.header
                ? data.header.creator_title
                : undefined,
              creator_sign: data.header ? data.header.creator_sign : undefined,
              created: data.header ? data.header.created : undefined,
              verifier_name: data.header
                ? data.header.verifier_name
                : undefined,
              verifier_title: data.header
                ? data.header.verifier_title
                : undefined,
              verifier_sign: data.header
                ? data.header.verifier_sign
                : undefined,
              verified: data.header ? data.header.verified : undefined,
              knower_name: data.header ? data.header.knower_name : undefined,
              knower_title: data.header ? data.header.knower_title : undefined,
              knower_sign: data.header ? data.header.knower_sign : undefined,
              knowed: data.header ? data.header.knowed : undefined,
              loading: false,

              operator: data.header ? data.header.operator : "",
            });
          });
      });
    } else if (
      prevState.waktu_masak_start !== this.state.waktu_masak_start ||
      prevState.waktu_masak_stop !== this.state.waktu_masak_stop
    ) {
      if (this.state.waktu_masak_stop && this.state.waktu_masak_start) {
        this.setState({
          suhu_setting: "0",
          suhu_actual: "0",
          kontaminasi_logam: "0",
          kontaminasi_plastik: "0",
          kontaminasi_benang: "0",
          kontaminasi_others: "0",
          kondisi: "empuk",
          status: false,
        });
      } else {
        this.setState({
          suhu_setting: "",
          suhu_actual: "",
          kontaminasi_logam: "",
          kontaminasi_plastik: "",
          kontaminasi_benang: "",
          kontaminasi_others: "",
          kondisi2: "keras",
          status: "",
        });
      }
    } else if (
      prevState.waktu_cuci_start !== this.state.waktu_cuci_start ||
      prevState.waktu_cuci_stop !== this.state.waktu_cuci_stop
    ) {
      if (this.state.waktu_cuci_start && this.state.waktu_cuci_stop) {
        this.setState({
          fisik_utuh: "0",
          fisik_busuk: "0",
          organ_warna: false,
          organ_bau: false,
          organ_bentuk: false,
        });
      } else {
        this.setState({
          fisik_utuh: "",
          fisik_busuk: "",
          organ_warna: "",
          organ_bau: "",
          organ_bentuk: "",
        });
      }
    }
    if (prevState.tanggal !== this.state.tanggal && this.state.tanggal) {
      const url = config.api + config.path.rm;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          this.setState({
            exists: data.datas,
          });
        });
    }
  }

  submitCreatorApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("creator_name", data.name);
      formData.append("creator_title", data.title);
      formData.append("creator_sign", data.sign);
      axios
        .patch(config.api + config.path.rm, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            bahan: this.state.bahan,
          },
        })
        .then((res) => {
          this.setState({
            approveCreator: undefined,
            creator_name: res.data.header.creator_name,
            creator_title: res.data.header.creator_title,
            creator_sign: res.data.header.creator_sign,
            created: res.data.header.creator_sign,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveCreator: undefined,
            creator_name: undefined,
            creator_title: undefined,
            creator_sign: undefined,
            created: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitVerifierApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("verifier_name", data.name);
      formData.append("verifier_title", data.title);
      formData.append("verifier_sign", data.sign);
      axios
        .patch(config.api + config.path.rm, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            bahan: this.state.bahan,
          },
        })
        .then((res) => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: res.data.header.verifier_name,
            verifier_title: res.data.header.verifier_title,
            verifier_sign: res.data.header.verifier_sign,
            verified: res.data.header.verified,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: undefined,
            verifier_title: undefined,
            verifier_sign: undefined,
            verified: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitKnowerApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("knower_name", data.name);
      formData.append("knower_title", data.title);
      formData.append("knower_sign", data.sign);
      axios
        .patch(config.api + config.path.rm, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            bahan: this.state.bahan,
          },
        })
        .then((res) => {
          this.setState({
            approveKnower: undefined,
            knower_name: res.data.header.knower_name,
            knower_title: res.data.header.knower_title,
            knower_sign: res.data.header.knower_sign,
            knowed: res.data.header.knowed,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveKnower: undefined,
            knower_name: undefined,
            knower_title: undefined,
            knower_sign: undefined,
            knowed: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }

  componentDidMount() {
    const yourDate = new Date();
    this.setState({ tanggal: yourDate.toISOString().split("T")[0] });
  }

  handleFormSubmit(event) {
    event.preventDefault();
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("nodoc", this.state.nodoc);
      formData.append("waktu_cuci_start", this.state.waktu_cuci_start);
      formData.append("waktu_cuci_stop", this.state.waktu_cuci_stop);
      formData.append("fisik_utuh", this.state.fisik_utuh);
      formData.append("fisik_busuk", this.state.fisik_busuk);
      formData.append("organ_warna", this.state.organ_warna);
      formData.append("organ_bau", this.state.organ_bau);
      formData.append("organ_bentuk", this.state.organ_bentuk);
      formData.append("waktu_masak_start", this.state.waktu_masak_start);
      formData.append("waktu_masak_stop", this.state.waktu_masak_stop);
      formData.append("suhu_setting", this.state.suhu_setting);
      formData.append("suhu_actual", this.state.suhu_actual);
      formData.append("kontaminasi_logam", this.state.kontaminasi_logam);
      formData.append("kontaminasi_plastik", this.state.kontaminasi_plastik);
      formData.append("kontaminasi_benang", this.state.kontaminasi_benang);
      formData.append("kontaminasi_others", this.state.kontaminasi_others);
      formData.append("kondisi", this.state.kondisi);
      formData.append("status", this.state.status);
      formData.append("keterangan", this.state.keterangan);
      axios({
        method: "post",
        url: config.api + "koneksi_rm.php/",
        data: formData,
        params: {
          tanggal: this.state.tanggal,
          shiftr: this.state.shiftr,
          bahan: this.state.bahan,
        },
        config: {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        },
      })
        .then(
          function (response) {
            this.setState(
              { datas: response.data.datas, loading: false },
              (() => {
                alert("Berhasil menyimpan data");
              }).bind()
            );
          }.bind(this)
        )
        .catch(() => {
          this.setState({ loading: false });
          alert("Gagal untuk menyimpan data");
        });
    });
  }
  datas = [];
  async getOptions() {
    const resShift = await axios.get(config.api + config.path.master_shift);
    const dataShift = resShift.data;
    const shiftOptions = dataShift.map((d) => ({
      value: d.id,
      label: d.judul,
    }));
    this.setState({ SelectShiftOptions: shiftOptions });
    const resBahan = await axios.get(config.api + config.path.master_bahanbaku);
    const dataBahan = resBahan.data;
    const bahanOptions = dataBahan.map((d) => ({
      value: d.Id,
      label: d.Nama,
    }));
    this.setState({ SelectBahanOptions: bahanOptions });
  }

  componentWillMount() {
    this.getOptions();
  }

  setEdit(ind) {
    this.setState({
      formData: this.state.datas[ind],
    });
  }
  removeExistData(bahan, shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (exists[i].bahan === bahan && exists[i].shift === shift) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) - 1;
      }
    }
    if (isExist === false) {
      exists.push({
        bahan: bahan,
        shift: shift,
        qty: "0",
      });
    }
    return exists;
  }
  addExistData(bahan, shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (exists[i].bahan === bahan && exists[i].shift === shift) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) + 1;
      }
    }
    if (isExist === false) {
      exists.push({
        bahan: bahan,
        shift: shift,
        qty: "1",
      });
    }
    return exists;
  }

  getBahanOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.SelectBahanOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.bahan) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["bahan"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getBahanName(id) {
    const options = JSON.parse(JSON.stringify(this.state.SelectBahanOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getBahanOptions() {
    const options = JSON.parse(JSON.stringify(this.state.SelectBahanOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
          parseInt(exists[j]["bahan"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }

  getShiftOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.SelectShiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.shiftr) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getShiftName(id) {
    const options = JSON.parse(JSON.stringify(this.state.SelectShiftOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getShiftOptions() {
    const options = JSON.parse(JSON.stringify(this.state.SelectShiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }

  isRouded() {
    for (let i = 0; i < this.datas.length; i++) {
      if (
        this.state.data === this.datas[i].id_rm_detail &&
        this.datas[i].this.state.waktu_cuci_start > 0
      ) {
        return true;
      }
    }
    return false;
  }
  render() {
    return (
      <>
        <Main>
          <br />

          <div className="container-fluid">
            <div className="row">
              <div className="col-sm">
                <h4 className="page-header text-center">
                  PEMERIKSAAN PENCUCIAN & PEMASAKAN BAHAN BAKU
                </h4>
              </div>
            </div>
            <form>
              <div className="row">
                <div className="col-sm form-group">
                  <label htmlFor="tanggal">Date :</label>
                  <input
                    disabled={this.state.loading}
                    required
                    type="date"
                    name="tanggal"
                    id="tanggal"
                    className="form-control"
                    value={this.state.tanggal}
                    onChange={(e) => this.setState({ tanggal: e.target.value })}
                  />
                </div>
                <div className="col-sm form-group">
                  <label htmlFor="shiftr">Shift :</label>
                  <Select
                    isDisabled={this.state.loading}
                    name="shiftr"
                    id="shiftr"
                    options={this.getShiftOptions()}
                    value={this.getShiftOptionValue()}
                    onChange={(e) => {
                      this.setState({ shiftr: e.value });
                    }}
                  />
                </div>
                <div className="col-sm form-group">
                  <label htmlFor="bahan">Nama Bahan Baku :</label>
                  <Select
                    name="bahan"
                    isDisabled={this.state.loading}
                    id="bahan"
                    options={this.getBahanOptions()}
                    value={this.getBahanOptionValue()}
                    onChange={(e) => {
                      this.setState({ bahan: e.value });
                    }}
                  />
                </div>
              </div>
              {this.state.bahan && this.state.tanggal && this.state.shiftr ? (
                <>
                  <div className="row">
                    <div className="col-sm-12 text-right">
                      <font size="2">No. Dok : {this.state.nodoc}</font>
                    </div>
                    <div className="col-sm-12">
                      <table className="table data table-bordered table-striped table-hover rm">
                        <thead>
                          <tr>
                            <th colSpan="2">Waktu Cuci</th>
                            <th colSpan="2">Kondisi Fisik</th>
                            <th colSpan="3">Organoleptik (√)</th>
                            <th colSpan="2">Waktu Masak</th>
                            <th colSpan="2">Suhu Masak (&#7506;C)</th>
                            <th colSpan="4">Kontaminasi (pcs)</th>
                            <th colSpan="2">Kondisi Fisik (√)</th>
                            <th rowSpan="2">Status</th>
                            <th rowSpan="2">Keterangan</th>
                          </tr>
                          <tr>
                            <th>Start</th>
                            <th>Stop</th>
                            <th>Utuh</th>
                            <th>Busuk</th>
                            <th>Warna</th>
                            <th>Bau</th>
                            <th>Bentuk</th>
                            <th>Start</th>
                            <th>Stop</th>
                            <th>Setting</th>
                            <th>Actual</th>
                            <th>Logam</th>
                            <th>Plastik</th>
                            <th>Benang</th>
                            <th>Others</th>
                            <th>Empuk</th>
                            <th>Keras</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.datas.map((rm, index) => {
                            return (
                              <tr key={index}>
                                <td className="action">
                                  {!this.state.verifier_name ? (
                                    <>
                                      <button
                                        type="button"
                                        className="btn btn-outline-info btn-block btn-sm"
                                        onClick={() => {
                                          this.setEdit(index);
                                        }}>
                                        &#128269;
                                      </button>

                                      {formatTime(rm.waktu_cuci_start)}
                                    </>
                                  ) : (
                                    <>{formatTime(rm.waktu_cuci_start)} </>
                                  )}
                                </td>
                                <td className="action">
                                  {formatTime(rm.waktu_cuci_stop)}
                                </td>

                                <td>
                                  {rm.waktu_cuci_start !== null &&
                                  rm.waktu_cuci_stop !== null
                                    ? rm.fisik_utuh
                                    : ""}
                                </td>
                                <td>
                                  {rm.waktu_cuci_start !== null &&
                                  rm.waktu_cuci_stop !== null
                                    ? rm.fisik_busuk
                                    : ""}
                                </td>
                                <td align="center">
                                  {rm.waktu_cuci_start !== null &&
                                  rm.waktu_cuci_stop !== null ? (
                                    <>
                                      {rm.organ_warna === "1" ? (
                                        <>&#10003;</>
                                      ) : (
                                        <>&#10005;</>
                                      )}
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </td>
                                <td align="center">
                                  {rm.waktu_cuci_start !== null &&
                                  rm.waktu_cuci_stop !== null ? (
                                    <>
                                      {rm.organ_bau === "1" ? (
                                        <>&#10003;</>
                                      ) : (
                                        <>&#10005;</>
                                      )}
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </td>
                                <td align="center">
                                  {rm.waktu_cuci_start !== null &&
                                  rm.waktu_cuci_stop !== null ? (
                                    <>
                                      {rm.organ_bentuk === "1" ? (
                                        <>&#10003;</>
                                      ) : (
                                        <>&#10005;</>
                                      )}
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </td>
                                <td>
                                  {rm.waktu_masak_start !== null
                                    ? formatTime(rm.waktu_masak_start)
                                    : ""}
                                </td>
                                <td>
                                  {rm.waktu_masak_stop !== null
                                    ? formatTime(rm.waktu_masak_stop)
                                    : ""}
                                </td>
                                <td>
                                  {rm.waktu_masak_start !== null &&
                                  rm.waktu_masak_stop !== null
                                    ? rm.suhu_setting
                                    : ""}
                                </td>
                                <td>
                                  {rm.waktu_masak_start !== null &&
                                  rm.waktu_masak_stop !== null
                                    ? rm.suhu_actual
                                    : ""}
                                </td>
                                <td>
                                  {rm.waktu_masak_start !== null &&
                                  rm.waktu_masak_stop !== null
                                    ? rm.kontaminasi_logam
                                    : ""}
                                </td>
                                <td>
                                  {rm.waktu_masak_start !== null &&
                                  rm.waktu_masak_stop !== null
                                    ? rm.kontaminasi_plastik
                                    : ""}
                                </td>
                                <td>
                                  {rm.waktu_masak_start !== null &&
                                  rm.waktu_masak_stop !== null
                                    ? rm.kontaminasi_benang
                                    : ""}
                                </td>
                                <td>
                                  {rm.waktu_masak_start !== null &&
                                  rm.waktu_masak_stop !== null
                                    ? rm.kontaminasi_others
                                    : ""}
                                </td>
                                <td align="center">
                                  {rm.waktu_masak_start !== null &&
                                  rm.waktu_masak_stop !== null ? (
                                    <>
                                      {rm.kondisi === "empuk" ? (
                                        <>&#10003;</>
                                      ) : (
                                        <>&#10005;</>
                                      )}
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </td>
                                <td align="center">
                                  {rm.waktu_masak_start !== null &&
                                  rm.waktu_masak_stop !== null ? (
                                    <>
                                      {rm.kondisi === "keras" ? (
                                        <>&#10003;</>
                                      ) : (
                                        <>&#10005;</>
                                      )}
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </td>
                                <td align="center">
                                  {rm.waktu_masak_start !== null &&
                                  rm.waktu_masak_stop !== null ? (
                                    <>
                                      {rm.status === "1" ? (
                                        <>&#10003;</>
                                      ) : (
                                        <>&#10005;</>
                                      )}
                                    </>
                                  ) : (
                                    ""
                                  )}
                                </td>
                                <td>
                                  <font size="2">{rm.keterangan}</font>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                        {!this.state.verifier_name ? (
                          <tfoot>
                            <tr>
                              <td
                                colSpan={this.isRouded() ? 32 : 35}
                                align="center">
                                <button
                                  type="button"
                                  className="btn btn-primary  "
                                  onClick={(e) => {
                                    this.setState({
                                      formData: null,
                                    });
                                  }}
                                  value="Add">
                                  &#10010; Add
                                </button>
                              </td>
                            </tr>
                          </tfoot>
                        ) : undefined}
                      </table>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6 xs-12 petunjuk"></div>
                    <div className="col-md-6 xs-12">
                      <table className="sign" cellPadding={3}>
                        <tbody>
                          <tr>
                            <th width={"25%"}>&nbsp;</th>
                            <th width={"25%"}> Dibuat Oleh,</th>
                            <th width={"25%"}> Diverifikasi Oleh,</th>
                            <th width={"25%"}>Mengetahui Oleh, </th>
                          </tr>
                          <tr>
                            <th>Tanda Tangan</th>
                            {this.state.creator_sign ? (
                              <td align="center" valign="middle">
                                <img
                                  src={this.state.creator_sign}
                                  alt="creator sign"
                                  width={"100%"}
                                />
                              </td>
                            ) : (
                              <td rowSpan={4} align="center" valign="middle">
                                <button
                                  className="btn btn-primary"
                                  disabled={
                                    this.state.datas.length < 1 ||
                                    this.state.edit
                                  }
                                  onClick={(e) => {
                                    e.preventDefault();
                                    this.setState({
                                      approveCreator: {
                                        name: "",
                                        title: "Quality Control",
                                        sign: undefined,
                                      },
                                    });
                                  }}>
                                  Approve
                                </button>
                              </td>
                            )}
                            {this.state.verifier_sign ? (
                              <td align="center" valign="middle">
                                <img
                                  src={this.state.verifier_sign}
                                  width={"100%"}
                                  alt="verifier sign"
                                />
                              </td>
                            ) : (
                              <td rowSpan={4} align="center" valign="middle">
                                <button
                                  className="btn btn-primary"
                                  disabled={
                                    this.state.datas.length < 1 ||
                                    !this.state.creator_name ||
                                    this.state.edit
                                  }
                                  onClick={(e) => {
                                    e.preventDefault();
                                    this.setState({
                                      approveVerifier: {
                                        name: "",
                                        title: "Supervisor QC",
                                        sign: undefined,
                                      },
                                    });
                                  }}>
                                  Approve
                                </button>
                              </td>
                            )}
                            {this.state.knower_sign ? (
                              <td align="center" valign="middle">
                                <img
                                  src={this.state.knower_sign}
                                  width={"100%"}
                                  alt="knower sign"
                                />
                              </td>
                            ) : (
                              <td rowSpan={4} align="center" valign="middle">
                                <button
                                  className="btn btn-primary"
                                  disabled={
                                    this.state.datas.length < 1 ||
                                    !this.state.creator_name ||
                                    !this.state.verifier_name ||
                                    this.state.edit
                                  }
                                  onClick={(e) => {
                                    e.preventDefault();
                                    this.setState({
                                      approveKnower: {
                                        name: "",
                                        title: "ManagerQA",
                                        sign: undefined,
                                      },
                                    });
                                  }}>
                                  Approve
                                </button>
                              </td>
                            )}
                          </tr>
                          <tr>
                            <th>Nama</th>
                            {this.state.creator_name ? (
                              <td align="center" valign="middle">
                                {this.state.creator_name}
                              </td>
                            ) : undefined}
                            {this.state.verifier_name ? (
                              <td align="center" valign="middle">
                                {this.state.verifier_name}
                              </td>
                            ) : undefined}
                            {this.state.knower_name ? (
                              <td align="center" valign="middle">
                                {this.state.knower_name}
                              </td>
                            ) : undefined}
                          </tr>
                          <tr>
                            <th>Jabatan</th>
                            {this.state.creator_title ? (
                              <td align="center" valign="middle">
                                {this.state.creator_title}
                              </td>
                            ) : undefined}
                            {this.state.verifier_title ? (
                              <td align="center" valign="middle">
                                {this.state.verifier_title}
                              </td>
                            ) : undefined}
                            {this.state.knower_title ? (
                              <td align="center" valign="middle">
                                {this.state.knower_title}
                              </td>
                            ) : undefined}
                          </tr>
                          <tr>
                            <th>Tanggal</th>
                            {this.state.created ? (
                              <td align="center" valign="middle">
                                {new Date(
                                  this.state.created
                                ).toLocaleDateString("id-ID", {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                })}
                              </td>
                            ) : undefined}
                            {this.state.verified ? (
                              <td align="center" valign="middle">
                                {new Date(
                                  this.state.verified
                                ).toLocaleDateString("id-ID", {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                })}
                              </td>
                            ) : undefined}
                            {this.state.knowed ? (
                              <td align="center" valign="middle">
                                {new Date(this.state.knowed).toLocaleDateString(
                                  "id-ID",
                                  {
                                    year: "numeric",
                                    month: "short",
                                    day: "numeric",
                                    hour: "numeric",
                                    minute: "numeric",
                                  }
                                )}
                              </td>
                            ) : undefined}
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </>
              ) : undefined}
            </form>
          </div>
        </Main>

        <Approval
          loading={this.state.loading}
          title="Dibuat oleh,"
          show={this.state.approveCreator ? true : false}
          data={this.state.approveCreator}
          onSubmit={(data) => {
            this.submitCreatorApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveCreator: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Diverifikasi oleh,"
          show={this.state.approveVerifier ? true : false}
          data={this.state.approveVerifier}
          onSubmit={(data) => {
            this.submitVerifierApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveVerifier: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Mengetahui oleh,"
          show={this.state.approveKnower ? true : false}
          data={this.state.approveKnower}
          onSubmit={(data) => {
            this.submitKnowerApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveKnower: undefined,
            });
          }}
        />

        <RMForm
          header={{
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            shiftLabel: this.getShiftName(this.state.shiftr),
            bahan: this.state.bahan,
            bahanLabel: this.getBahanName(this.state.bahan),
          }}
          data={this.state.formData}
          show={this.state.formData !== undefined}
          onAfterDelete={(id_rm_detail) => {
            const currDatas = [...this.state.datas];
            const newDatas = [];
            for (let i = 0; i < currDatas.length; i++) {
              if (
                parseInt(currDatas[i].id_rm_detail) !== parseInt(id_rm_detail)
              ) {
                newDatas.push(currDatas[i]);
              }
            }
            this.setState({
              exists: this.removeExistData( this.state.bahan,this.state.shiftr),
              datas: newDatas,
              formData: undefined,
            });
          }}
          onAfterEdit={(data) => {
            const currDatas = [...this.state.datas];
            for (let i = 0; i < currDatas.length; i++) {
              if (parseInt(currDatas[i].id_rm_detail) === parseInt(data.id_rm_detail)) {
                currDatas[i] = data;
              }
            }
            this.setState({
              datas: currDatas,
              formData: undefined,
            });
          }}
          onAfterInsert={(datas) => {
            this.setState({
              exists: this.addExistData(this.state.bahan, this.state.shiftr),
              datas: datas,
              formData: undefined,
            });
          }}
          onClose={() => {
            this.setState({
              formData: undefined,
            });
          }}
        />
      </>
    );
  }
}

export default raw_material;
