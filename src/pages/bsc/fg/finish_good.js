import React from "react";
import axios from "axios";
import config from "../../../config";
import Select from "react-select";
import Approval from "../../../Approval";
import Main from "../../../layout/main";
import FGForm from "./fgForm";

class fg extends React.Component {
  state = {
    formData: undefined,
    exists: [],
    tanggal: "",
    no_doc: "FRM.QCRD.CRP.03.01; Rev.03",
    approveCreator: undefined,
    approveVerifier: undefined,
    approveKnower: undefined,
    creator_name: undefined,
    creator_title: undefined,
    creator_sign: undefined,
    created: undefined,
    verifier_name: undefined,
    verifier_title: undefined,
    verifier_sign: undefined,
    verified: undefined,
    knower_name: undefined,
    knower_title: undefined,
    knower_sign: undefined,
    knowed: undefined,
    edit: undefined,
    loading: false,
    name: "",
    email: "",
    shiftr: "",
    jenis_produksi: "",
    customer_spk: "",
    cek: "",
    karton: false,
    innerbox: false,
    film: false,
    layer_hangger: false,
    toples_kaleng: false,
    karton_per_pcs: "",
    satuan: "",
    inner_per_pcs: "",
    kodeprod_karton: false,
    kodeprod_inner: false,
    kodeprod_film: false,
    prod_exp: "",
    mesin: "",
    typeprod: "",
    sealing: false,
    benda_asing: false,
    kondisi_perekat: false,
    kondisi_kemasan: false,
    other: false,
    keterangan: "",
    datas: [],
    query: "",
    searchString: [],
    SelectShiftOptions: [],
  };
  componentDidUpdate(prevProps, prevState) {
    if (
      (prevState.tanggal !== this.state.tanggal ||
        prevState.shiftr !== this.state.shiftr) &&
      this.state.tanggal &&
      this.state.shiftr
    ) {
      const url = config.api + config.path.fg;
      this.setState({ loading: true }, () => {
        axios
          .get(url, {
            params: {
              tanggal: this.state.tanggal,
              shiftr: this.state.shiftr,
            },
          })
          .then((response) => response.data)
          .then((data) => {
            this.setState({
              datas: data.datas,
              creator_name: data.header ? data.header.creator_name : undefined,
              creator_title: data.header
                ? data.header.creator_title
                : undefined,
              creator_sign: data.header ? data.header.creator_sign : undefined,
              created: data.header ? data.header.created : undefined,
              verifier_name: data.header
                ? data.header.verifier_name
                : undefined,
              verifier_title: data.header
                ? data.header.verifier_title
                : undefined,
              verifier_sign: data.header
                ? data.header.verifier_sign
                : undefined,
              verified: data.header ? data.header.verified : undefined,
              knower_name: data.header ? data.header.knower_name : undefined,
              knower_title: data.header ? data.header.knower_title : undefined,
              knower_sign: data.header ? data.header.knower_sign : undefined,
              knowed: data.header ? data.header.knowed : undefined,
              loading: false,

              operator: data.header ? data.header.operator : "",
            });
          });
      });
    }
    if (prevState.tanggal !== this.state.tanggal && this.state.tanggal) {
      const url = config.api + config.path.fg;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          this.setState({
            exists: data.datas,
          });
        });
    }
  }

  submitCreatorApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("creator_name", data.name);
      formData.append("creator_title", data.title);
      formData.append("creator_sign", data.sign);
      axios
        .patch(config.api + config.path.fg, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
          },
        })
        .then((res) => {
          this.setState({
            approveCreator: undefined,
            creator_name: res.data.header.creator_name,
            creator_title: res.data.header.creator_title,
            creator_sign: res.data.header.creator_sign,
            created: res.data.header.created,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveCreator: undefined,
            creator_name: undefined,
            creator_title: undefined,
            creator_sign: undefined,
            created: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitVerifierApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("verifier_name", data.name);
      formData.append("verifier_title", data.title);
      formData.append("verifier_sign", data.sign);
      axios
        .patch(config.api + config.path.fg, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
          },
        })
        .then((res) => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: res.data.header.verifier_name,
            verifier_title: res.data.header.verifier_title,
            verifier_sign: res.data.header.verifier_sign,
            verified: res.data.header.verified,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: undefined,
            verifier_title: undefined,
            verifier_sign: undefined,
            verified: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitKnowerApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("knower_name", data.name);
      formData.append("knower_title", data.title);
      formData.append("knower_sign", data.sign);
      axios
        .patch(config.api + config.path.fg, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
          },
        })
        .then((res) => {
          this.setState({
            approveKnower: undefined,
            knower_name: res.data.header.knower_name,
            knower_title: res.data.header.knower_title,
            knower_sign: res.data.header.knower_sign,
            knowed: res.data.header.knowed,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveKnower: undefined,
            knower_name: undefined,
            knower_title: undefined,
            knower_sign: undefined,
            knowed: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }

  componentDidMount() {
    const yourDate = new Date();
    this.setState({ tanggal: yourDate.toISOString().split("T")[0] });
  }
  handleFormSubmit(event) {
    event.preventDefault();
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("no_doc", this.state.no_doc);
      formData.append("jenis_produksi", this.state.jenis_produksi);
      formData.append("customer_spk", this.state.customer_spk);
      formData.append("cek", this.state.cek);
      formData.append("karton", this.state.karton);
      formData.append("innerbox", this.state.innerbox);
      formData.append("film", this.state.film);
      formData.append("layer_hangger", this.state.layer_hangger);
      formData.append("toples_kaleng", this.state.toples_kaleng);
      formData.append("karton_per_pcs", this.state.karton_per_pcs);
      formData.append("satuan", this.state.satuan);
      formData.append("karton_per_kg", this.state.karton_per_kg);
      formData.append("inner_per_kg", this.state.inner_per_kg);
      formData.append("kodeprod_karton", this.state.kodeprod_karton);
      formData.append("kodeprod_inner", this.state.kodeprod_inner);
      formData.append("kodeprod_film", this.state.kodeprod_film);
      formData.append("prod_exp", this.state.prod_exp);
      formData.append("mesin", this.state.mesin);
      formData.append("sealing", this.state.sealing);
      formData.append("benda_asing", this.state.benda_asing);
      formData.append("kondisi_perekat", this.state.kondisi_perekat);
      formData.append("kondisi_kemasan", this.state.kondisi_kemasan);
      formData.append("other", this.state.other);
      formData.append("not_ok_per_pcs", this.state.not_ok_per_pcs);
      formData.append("keterangan", this.state.keterangan);
      axios({
        method: "post",
        url: config.api + config.path.fg,
        data: formData,
        params: {
          tanggal: this.state.tanggal,
          shiftr: this.state.shiftr,
        },
        config: {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        },
      })
        .then(
          function (response) {
            this.setState(
              { datas: response.data.datas, loading: false },
              (() => {
                alert("Berhasil menyimpan data");
              }).bind()
            );
          }.bind(this)
        )
        .catch(() => {
          this.setState({ loading: false });
          alert("Gagal untuk menyimpan data");
        });
    });
  }
  datas = [];
  async getOptions() {
    const resShift = await axios.get(config.api + config.path.master_shift);
    const dataShift = resShift.data;
    const shiftOptions = dataShift.map((d) => ({
      value: d.id,
      label: d.judul,
    }));
    this.setState({ SelectShiftOptions: shiftOptions });
  }
  componentWillMount() {
    this.getOptions();
  }
  setEdit(ind) {
    this.setState({
      formData: this.state.datas[ind],
    });
  }
  removeExistData(shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (exists[i].shift === shift) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) - 1;
      }
    }
    if (isExist === false) {
      exists.push({
        shift: shift,
        qty: "0",
      });
    }
    return exists;
  }
  addExistData(shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (exists[i].shift === shift) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) + 1;
      }
    }
    if (isExist === false) {
      exists.push({
        shift: shift,
        qty: "1",
      });
    }
    return exists;
  }
  // getProductOptionValue() {
  //   const options = JSON.parse(JSON.stringify(this.state.productOptions));
  //   const exists = JSON.parse(JSON.stringify(this.state.exists));
  //   let selected = undefined;
  //   for (let i = 0; i < options.length; i++) {
  //     if (parseInt(this.state.typeprod) === parseInt(options[i].value)) {
  //       selected = options[i];
  //       for (let j = 0; j < exists.length; j++) {
  //         if (
  //           parseInt(exists[j]["type_produksi"]) ===
  //             parseInt(options[i].value) &&
  //           parseInt(exists[j]["qty"]) > 0
  //         ) {
  //           selected.label = <b>{selected.label}</b>;
  //         }
  //       }
  //     }
  //   }
  //   return selected;
  // }
  getProductOptions() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["type_produksi"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getProductName(id) {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return;
  }

  getShiftOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.SelectShiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.shiftr) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getShiftName(id) {
    const options = JSON.parse(JSON.stringify(this.state.SelectShiftOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getShiftOptions() {
    const options = JSON.parse(JSON.stringify(this.state.SelectShiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }

  getMesinOptions() {
    const options = this.state.mesinOptions;
    const exists = this.state.exists;
    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getMesinOptionValue() {
    const options = this.state.mesinOptions;
    const exists = this.state.exists;
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.mesin) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getMesinName(id) {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  isRouded() {
    for (let i = 0; i < this.datas.length; i++) {
      if (
        this.state.data === this.datas[i].id_fg_detail &&
        this.datas[i].this.state.jenis_produksi > 0
      ) {
        return true;
      }
    }
    return false;
  }
  render() {
    return (
      <>
        <Main>
          <br />
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm">
                <h4 className="page-header text-center">
                  FORM PEMERIKSAAN PRODUK AKHIR
                </h4>
              </div>
            </div>
            <form>
              <div className="row">
                <div className="col-sm form-group">
                  <label htmlFor="tanggal">Date :</label>
                  <input
                    className="form-control"
                    disabled={this.state.loading}
                    required
                    type="date"
                    name="tanggal"
                    id="tanggal"
                    value={this.state.tanggal}
                    onChange={(e) => this.setState({ tanggal: e.target.value })}
                  />
                </div>
                <div className="col-sm form-group">
                  <label htmlFor="shiftr">Shift :</label>
                  <Select
                    isDisabled={this.state.loading}
                    name="shiftr"
                    id="shiftr"
                    options={this.getShiftOptions()}
                    value={this.getShiftOptionValue()}
                    onChange={(e) => {
                      this.setState({ shiftr: e.value });
                    }}
                  />
                </div>
              </div>
              {this.state.tanggal && this.state.shiftr ? (
                <>
                  <div className="row">
                    <div className="col-sm-12 text-right">
                      <font size="2">No. Dok : {this.state.no_doc} </font>
                    </div>
                    <div className="col-sm-12">
                      <table className="table data table-bordered table-striped table hover fg">
                        <thead>
                          <tr>
                            <th rowSpan="2">Jenis Produksi</th>
                            <th rowSpan="2">Customer & PO/SPK </th>
                            <th rowSpan="2"> Cek Ke </th>
                            <th colSpan="5"> Kesesuian Item & Type</th>
                            <th colSpan="2">Kesesuaian Jumlah </th>
                            <th colSpan="2">Kesesuaian Berat</th>
                            <th colSpan="4">Kode Produksi</th>
                            <th rowSpan="2">Printing MFG/ Prod & EXP</th>
                            <th rowSpan="2"> No.Mesin </th>
                            <th colSpan="5"> Kasus Secara Visual</th>
                            <th rowSpan="2"> Jumlah N_OK (pcs)</th>
                            <th rowSpan="2"> Keterangan</th>
                          </tr>
                          <tr>
                            <th className="vertical">
                              <span>Karton</span>
                            </th>
                            <th className="vertical">
                              <span>Inner</span>
                            </th>
                            <th className="vertical">
                              <span>Film</span>
                            </th>
                            <th className="vertical">
                              <span>Layer / Hangger</span>
                            </th>
                            <th className="vertical">
                              <span>Toples / Kaleng</span>
                            </th>

                            <th>Karton</th>
                            <th>Inner (pcs)</th>
                            <th>Karton (gr)</th>
                            <th>Inner (gr)</th>
                            <th className="vertical">
                              <span>Karton (&radic;/x)</span>
                            </th>
                            <th className="vertical">
                              <span>Inner (&radic;/x)</span>
                            </th>
                            <th className="vertical">
                              <span>Toples / Kaleng (&radic;/x)</span>
                            </th>
                            <th className="vertical">
                              <span>Film (&radic;/x)</span>
                            </th>
                            <th>Sealing</th>
                            <th>Ada Benda Asing</th>
                            <th>Kondisi Perekat</th>
                            <th>Kondisi Kemasan</th>
                            <th>Other</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.datas.map((fg, index) => {
                            const sealing = parseInt(fg.sealing);
                            const benda_asing = parseInt(fg.benda_asing);
                            const kondisi_perekat = parseInt(
                              fg.kondisi_perekat
                            );
                            const kondisi_kemasan = parseInt(
                              fg.kondisi_kemasan
                            );
                            const other = parseInt(fg.other);

                            const not_ok =
                              sealing +
                              benda_asing +
                              kondisi_perekat +
                              kondisi_kemasan +
                              other;
                            parseInt(fg.not_ok);
                            return (
                              <tr key={index}>
                                <td className="action">
                                  {!this.state.verifier_name ? (
                                    <>
                                      <button
                                        type="button"
                                        className="btn btn-outline-info btn-block btn-sm"
                                        onClick={() => {
                                          this.setEdit(index);
                                        }}
                                      >
                                        &#128269;
                                      </button>

                                      {fg.jenis_produksi}
                                    </>
                                  ) : (
                                    <>{fg.jenis_produksi} </>
                                  )}
                                </td>
                                <td>{fg.customer_spk}</td>
                                <td>{fg.cek}</td>

                                <td align="center">
                                  {fg.karton === "1" ? (
                                    <>&#10003;</>
                                  ) : (
                                    <>&#10005;</>
                                  )}
                                </td>
                                <td align="center">
                                  {fg.innerbox === "1" ? (
                                    <>&#10003;</>
                                  ) : (
                                    <>&#10005;</>
                                  )}
                                </td>
                                <td align="center">
                                  {fg.film === "1" ? (
                                    <>&#10003;</>
                                  ) : (
                                    <>&#10005;</>
                                  )}
                                </td>
                                <td align="center">
                                  {fg.layer_hangger === "1" ? (
                                    <>&#10003;</>
                                  ) : (
                                    <>&#10005;</>
                                  )}
                                </td>
                                <td align="center">
                                  {fg.toples_kaleng === "1" ? (
                                    <>&#10003;</>
                                  ) : (
                                    <>&#10005;</>
                                  )}
                                </td>

                                <td>
                                  {fg.karton_per_pcs} &nbsp;
                                  {fg.satuan}
                                </td>
                                <td>{fg.inner_per_pcs}</td>
                                <td>{fg.karton_per_gr}</td>
                                <td>{fg.inner_per_gr}</td>
                                <td align="center">
                                  {fg.kodeprod_karton === "1" ? (
                                    <>&#10003;</>
                                  ) : (
                                    <>&#10005;</>
                                  )}
                                </td>
                                <td align="center">
                                  {fg.kodeprod_inner === "1" ? (
                                    <>&#10003;</>
                                  ) : (
                                    <>&#10005;</>
                                  )}
                                </td>
                                <td align="center">
                                  {fg.kodeprod_toples === "1" ? (
                                    <>&#10003;</>
                                  ) : (
                                    <>&#10005;</>
                                  )}
                                </td>
                                <td align="center">
                                  {fg.kodeprod_film === "1" ? (
                                    <>&#10003;</>
                                  ) : (
                                    <>&#10005;</>
                                  )}
                                </td>
                                <td>{fg.prod_exp}</td>
                                <td>{fg.mesin_name}</td>

                                <td>{fg.sealing}</td>
                                <td>{fg.benda_asing}</td>
                                <td>{fg.kondisi_perekat}</td>
                                <td>{fg.kondisi_kemasan}</td>
                                <td>{fg.other}</td>
                                <td>
                                  <span>{not_ok.toFixed()}</span>
                                </td>
                                <td>{fg.keterangan}</td>
                              </tr>
                            );
                          })}
                        </tbody>
                        {!this.state.verifier_name ? (
                          <tfoot>
                            <tr>
                              <td
                                colSpan={this.isRouded() ? 32 : 35}
                                align="center"
                              >
                                <button
                                  type="button"
                                  className="btn-primary btn"
                                  onClick={(e) => {
                                    this.setState({
                                      formData: null,
                                    });
                                  }}
                                  value="Add"
                                >
                                  &#10010;Add
                                </button>
                              </td>
                            </tr>
                          </tfoot>
                        ) : undefined}
                      </table>
                      <div className="row">
                        <div className="col-md-6 xs-12"></div>
                        <div className="col-md-6 xs-12">
                          <table className="sign" cellPadding={3}>
                            <tbody>
                              <tr>
                                <th width={"25%"}></th>
                                <th width={"25%"}> Dibuat Oleh,</th>
                                <th width={"25%"}> Diverifikasi Oleh,</th>
                                <th width={"25%"}>Mengetahui Oleh, </th>
                              </tr>
                              <tr>
                                <th>Tanda Tangan</th>
                                {this.state.creator_sign ? (
                                  <td align="center" valign="middle">
                                    <img
                                      src={this.state.creator_sign}
                                      alt="creator sign"
                                      width={"100%"}
                                    />
                                  </td>
                                ) : (
                                  <td
                                    rowSpan={4}
                                    align="center"
                                    valign="middle"
                                  >
                                    <button
                                      className="btn btn-primary"
                                      disabled={
                                        this.state.datas.length < 1 ||
                                        this.state.edit
                                      }
                                      onClick={(e) => {
                                        e.preventDefault();
                                        this.setState({
                                          approveCreator: {
                                            name: "",
                                            title: "Quality Control",
                                            sign: undefined,
                                          },
                                        });
                                      }}
                                    >
                                      Approve
                                    </button>
                                  </td>
                                )}
                                {this.state.verifier_sign ? (
                                  <td align="center" valign="middle">
                                    <img
                                      src={this.state.verifier_sign}
                                      width={"100%"}
                                      alt="verifier sign"
                                    />
                                  </td>
                                ) : (
                                  <td
                                    rowSpan={4}
                                    align="center"
                                    valign="middle"
                                  >
                                    <button
                                      className="btn btn-primary"
                                      disabled={
                                        this.state.datas.length < 1 ||
                                        !this.state.creator_name ||
                                        this.state.edit
                                      }
                                      onClick={(e) => {
                                        e.preventDefault();
                                        this.setState({
                                          approveVerifier: {
                                            name: "",
                                            title: "Supervisor QC",
                                            sign: undefined,
                                          },
                                        });
                                      }}
                                    >
                                      Approve
                                    </button>
                                  </td>
                                )}
                                {this.state.knower_sign ? (
                                  <td align="center" valign="middle">
                                    <img
                                      src={this.state.knower_sign}
                                      width={"100%"}
                                      alt="knower sign"
                                    />
                                  </td>
                                ) : (
                                  <td
                                    rowSpan={4}
                                    align="center"
                                    valign="middle"
                                  >
                                    <button
                                      className="btn btn-primary"
                                      disabled={
                                        this.state.datas.length < 1 ||
                                        !this.state.creator_name ||
                                        !this.state.verifier_name ||
                                        this.state.edit
                                      }
                                      onClick={(e) => {
                                        e.preventDefault();
                                        this.setState({
                                          approveKnower: {
                                            name: "",
                                            title: "Manager QA",
                                            sign: undefined,
                                          },
                                        });
                                      }}
                                    >
                                      Approve
                                    </button>
                                  </td>
                                )}
                              </tr>
                              <tr>
                                <th>Nama</th>
                                {this.state.creator_name ? (
                                  <td align="center" valign="middle">
                                    {this.state.creator_name}
                                  </td>
                                ) : undefined}
                                {this.state.verifier_name ? (
                                  <td align="center" valign="middle">
                                    {this.state.verifier_name}
                                  </td>
                                ) : undefined}
                                {this.state.knower_name ? (
                                  <td align="center" valign="middle">
                                    {this.state.knower_name}
                                  </td>
                                ) : undefined}
                              </tr>
                              <tr>
                                <th>Jabatan</th>
                                {this.state.creator_title ? (
                                  <td align="center" valign="middle">
                                    {this.state.creator_title}
                                  </td>
                                ) : undefined}
                                {this.state.verifier_title ? (
                                  <td align="center" valign="middle">
                                    {this.state.verifier_title}
                                  </td>
                                ) : undefined}
                                {this.state.knower_title ? (
                                  <td align="center" valign="middle">
                                    {this.state.knower_title}
                                  </td>
                                ) : undefined}
                              </tr>
                              <tr>
                                <th>Tanggal</th>
                                {this.state.created ? (
                                  <td align="center" valign="middle">
                                    {new Date(
                                      this.state.created
                                    ).toLocaleDateString("id-ID", {
                                      year: "numeric",
                                      month: "short",
                                      day: "numeric",
                                      hour: "numeric",
                                      minute: "numeric",
                                    })}
                                  </td>
                                ) : undefined}
                                {this.state.verified ? (
                                  <td align="center" valign="middle">
                                    {new Date(
                                      this.state.verified
                                    ).toLocaleDateString("id-ID", {
                                      year: "numeric",
                                      month: "short",
                                      day: "numeric",
                                      hour: "numeric",
                                      minute: "numeric",
                                    })}
                                  </td>
                                ) : undefined}
                                {this.state.knowed ? (
                                  <td align="center" valign="middle">
                                    {new Date(
                                      this.state.knowed
                                    ).toLocaleDateString("id-ID", {
                                      year: "numeric",
                                      month: "short",
                                      day: "numeric",
                                      hour: "numeric",
                                      minute: "numeric",
                                    })}
                                  </td>
                                ) : undefined}
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              ) : undefined}
            </form>
          </div>
        </Main>
        <Approval
          loading={this.state.loading}
          title="Dibuat oleh,"
          show={this.state.approveCreator ? true : false}
          data={this.state.approveCreator}
          onSubmit={(data) => {
            this.submitCreatorApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveCreator: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Diverifikasi oleh,"
          show={this.state.approveVerifier ? true : false}
          data={this.state.approveVerifier}
          onSubmit={(data) => {
            this.submitVerifierApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveVerifier: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Mengetahui oleh,"
          show={this.state.approveKnower ? true : false}
          data={this.state.approveKnower}
          onSubmit={(data) => {
            this.submitKnowerApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveKnower: undefined,
            });
          }}
        />
        <FGForm
          header={{
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            shiftLabel: this.getShiftName(this.state.shiftr),
            //mesin: this.state.mesin,
            //opsimesin: this.getMesinOptions(this.state.mesin),
            //valuemesin: this.getMesinOptionValue(this.state.mesin),
            //satuan: this.state.satuan,
            //opsiSatuan: this.getOpsiSatuan(),
            //typeprod: this.state.typeprod,
            //opsiTypeprod: this.getProductOptions(),
            //valueTypeprod: this.getProductOptionValue(this.state.typeprod),
            //typerodLabel: this.getProductName(this.state.typeprod),
          }}
          data={this.state.formData}
          show={this.state.formData !== undefined}
          onAfterDelete={(id_fg_detail) => {
            const currDatas = [...this.state.datas];
            const newDatas = [];
            for (let i = 0; i < currDatas.length; i++) {
              if (
                parseInt(currDatas[i].id_fg_detail) !== parseInt(id_fg_detail)
              ) {
                newDatas.push(currDatas[i]);
              }
            }
            this.setState({
              exists: this.removeExistData(this.state.shiftr),
              datas: newDatas,
              formData: undefined,
            });
          }}
          onAfterEdit={(data) => {
            const currDatas = [...this.state.datas];
            for (let i = 0; i < currDatas.length; i++) {
              if (currDatas[i].id_fg_detail === data.id_fg_detail) {
                currDatas[i] = data;
              }
            }
            this.setState({
              datas: currDatas,
              formData: undefined,
            });
          }}
          onAfterInsert={(datas) => {
            this.setState({
              exists: this.addExistData(this.state.shiftr),
              datas: datas,
              formData: undefined,
            });
          }}
          onClose={() => {
            this.setState({
              formData: undefined,
            });
          }}
        />
      </>
    );
  }
}
export default fg;
