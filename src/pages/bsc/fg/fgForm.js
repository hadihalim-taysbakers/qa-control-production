import React from "react";
import moment from "moment";
import "moment/locale/id";
import axios from "axios";
import config from "../../../config";
import Label from "../../../components/Label";
import * as Validator from "validatorjs";
import Form from "../../../components/Form";
import Field from "../../../components/Field";


class FGForm extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      mesinOptions:[],
      productOptions:[]
    }
    this.fields = {
      customer_spk: {
        name: "customer_spk",
        id: "customer_spk",
        label: "Customer & PO/SPK",
        attributeName: "Customer & PO/SPK",
        type: "text",
        rules: ["required"],
      },
      cek: {
        name: "cek",
        id: "cek",
        label: "Cek Ke",
        attributeName: "Cek",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      karton: {
        name: "karton",
        id: "karton",
        label: "Karton",
        attributeName: "Karton",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      innerbox: {
        name: "innerbox",
        id: "innerbox",
        label: "Inner",
        attributeName: "Inner",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      film: {
        name: "film",
        id: "film",
        label: "Film",
        attributeName: "Film",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      layer_hangger: {
        name: "layer_hangger",
        id: "layer_hangger",
        label: "Layer / Hangger",
        attributeName: "Layer / Hangger",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      toples_kaleng: {
        name: "toples_kaleng",
        id: "toples_kaleng",
        label: "Toples / Kaleng",
        attributeName: "Toples / Kaleng",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      karton_per_pcs: {
        name: "karton_per_pcs",
        id: "karton_per_pcs",
        label: "Karton",
        attributeName: "Karton",
        type: "number",
        min: 0,
        rules: ["numeric", "min:0", "required"],
      },

      inner_per_pcs: {
        name: "inner_per_pcs",
        id: "inner_per_pcs",
        label: "Inner  (Sachet)",
        attributeName: " Inner   (Sachet)",
        type: "number",
        min: 0,
        rules: ["numeric", "min:0", "required"],
      },
      karton_per_gr: {
        name: "karton_per_gr",
        id: "karton_per_gr",
        label: "Karton (gr)",
        attributeName: "Karton (gr)",
        type: "number",
        min: 0,
        rules: ["numeric", "min:0", "required"],
      },
      inner_per_gr: {
        name: "inner_per_gr",
        id: "inner_per_gr",
        label: "Inner (gr)",
        attributeName: "Inner (gr) ",
        type: "number",
        min: 0,
        rules: ["numeric", "min:0", "required"],
      },
      kodeprod_karton: {
        name: "kodeprod_karton",
        id: "kodeprod_karton",
        label: "Karton",
        attributeName: "Karton",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      kodeprod_inner: {
        name: "kodeprod_inner",
        id: "kodeprod_inner",
        label: "Inner",
        attributeName: "Inner",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      kodeprod_toples: {
        name: "kodeprod_toples",
        id: "kodeprod_toples",
        label: "Toples / Kaleng",
        attributeName: "Toples / Kaleng",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      kodeprod_film: {
        name: "kodeprod_film",
        id: "kodeprod_film",
        label: "Film",
        attributeName: "Film",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      prod_exp: {
        name: "prod_exp",
        id: "prod_exp",
        label: "MFG / Prod & EXP",
        attributeName: "MFG / Prod & EXP",
        type: "textarea",
        rules: ["required"],
      },

      sealing: {
        name: "sealing",
        id: "sealing",
        label: "Sealing",
        attributeName: "Sealing",
        type: "number",
        min: 0,
        rules: ["numeric", "min:0", "required"],
      },

      benda_asing: {
        name: "benda_asing",
        id: "benda_asing",
        label: "Benda Asing",
        attributeName: "Benda Asing",
        type: "number",
        min: 0,
        rules: ["numeric", "min:0", "required"],
      },
      kondisi_perekat: {
        name: "kondisi_perekat",
        id: "kondisi_perekat",
        label: "Kondisi Perekat",
        attributeName: "Kondisi Perekat",
        type: "number",
        min: 0,
        rules: ["numeric", "min:0", "required"],
      },
      kondisi_kemasan: {
        name: "kondisi_kemasan",
        id: "kondisi_kemasan",
        label: "Kondisi Kemasan",
        attributeName: "Kondisi Kemasan",
        type: "number",
        min: 0,
        rules: ["numeric", "min:0", "required"],
      },
      other: {
        name: "other",
        id: "other",
        label: "Other",
        attributeName: "Other",
        type: "number",
        min: 0,
        rules: ["numeric", "min:0", "required"],
      },
      keterangan: {
        name: "keterangan",
        id: "keterangan",
        label: "Keterangan",
        attributeName: "Keterangan",
        type: "text",
      },
      mesin: {
        name: "mesin",
        id: "mesin",
        label: "No. Mesin",
        attributeName: "nomor mesin",
        type: "select",
        rules: ["required"],
      },
      jenis_produksi: {
        name: "jenis_produksi",
        id: "jenis_produksi",
        label: "Jenis Produksi",
        attributeName: "jenis produksi",
        type: "select",
        rules: ["required"],
      },
      satuan: {
        name: "satuan",
        id: "satuan",
        label: "Satuan",
        attributeName: "satuan",
        type: "select",
      },
    };
    this.state = {
      loading: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  state = {
    loading: false,
  };
  getFields() {
    const fields = { ...this.fields };

    return fields;
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState(
      {
        datas: { ...this.state.datas, [name]: value },
      },
      () => {
        const valid = this.validateField(name, value);
        if (valid.valid === true) {
          this.setState({
            errors: { ...this.state.errors, [name]: [] },
          });
        } else {
          this.setState({
            errors: { ...this.state.errors, [name]: valid.errors[name] },
          });
        }
      }
    );
  }
  getProductOptions() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["type_produksi"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  async getOptions() {
    const resMesin = await axios.get(config.api + config.path.master_mesin, {
      params: { posisi: config.page.sealing.posisi },
    });
    const dataMesin = resMesin.data;
    const mesinOptions = dataMesin.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    this.setState({ mesinOptions: mesinOptions });
    const resProduct = await axios.get(
      config.api + config.path.master_product2
    );
    const dataProduct = resProduct.data;
    const productOptions = dataProduct.map((d) => ({
      value: d.id,
      label: d.product_name,
    }));
    this.productTypes = [...dataProduct];
    this.setState({ productOptions: productOptions });
  }
  componentWillMount() {
    this.getOptions();
  }
  validateField(name, value) {
    const messages = Validator.getMessages("id_fg");
    Validator.setMessages("id_fg", messages);
    Validator.setAttributeNames(this.attributeNames);
    const validation = new Validator(
      { [name]: value },
      { [name]: this.rules[name] },
      messages
    );
    if (validation.passes()) {
      return { valid: true, errors: null };
    } else {
      const errors = validation.errors.all();
      return {
        valid: false,
        errors: errors,
      };
    }
  }
  getOpsiSatuan() {
    const satuan = [
      {
        label: "Innerbox",
        value: "innerbox",
      },
      {
        label: "Innerbag",
        value: "innerbag",
      },
      {
        label: "Toples",
        value: "toples",
      },
      {
        label: "Kaleng",
        value: "kaleng",
      },
    ];

    return satuan;
  }
  InsertRecord(datas) {
    let formData = new FormData();
    for (const key in this.props.header) {
      formData.append(key, this.props.header[key]);
    }
    for (const key in datas) {
      formData.append(key, datas[key]);
    }
    formData.append("nodoc", "FRM.QCRD.CRP.03.01; Rev.02");
    this.setState({ loading: "insert" }, () => {
      axios({
        method: "post",
        url: config.api + config.path.fg,
        data: formData,
        params: {
          tanggal: this.props.header.tanggal,
          shiftr: this.props.header.shiftr,
        },
        config: { headers: { "Content-Type": "multipart/form-data" } },
      })
        .then(
          function (response) {
            if (response.data.success) {
              this.setState({ loading: false }, () => {
                this.props.onAfterInsert(response.data.datas);
                alert("Berhasil untuk menyimpan data");
              });
            } else {
              this.setState({ loading: false }, () => {
                alert(
                  "Gagal untuk menyimpan data : " + response.data.error.messages
                );
              });
            }
          }.bind(this)
        )
        .catch((e) => {
          this.setState({ loading: false });
          alert("Gagal untuk menyimpan data");
        });
    });
  }
  DeleteRecord() {
    if (this.props.data && this.props.data.id_fg_detail) {
      this.setState({ loading: "delete" }, () => {
        axios
          .delete(config.api + config.path.fg, {
            params: { id_fg_detail: this.props.data.id_fg_detail },
          })
          .then((res) => {
            if (res.data.success) {
              this.setState({ loading: false }, () => {
                this.props.onAfterDelete(res.data.id_fg_detail);
                alert("Berhasil untuk menghapus data");
              });
            } else {
              alert("Gagal menghapus data : " + res.data.error.messages);
            }
          });
      });
    }
  }
  EditRecord(datas) {
    if (this.props.data) {
      let formData = new FormData();
      for (const key in this.props.header) {
        formData.append(key, this.props.header[key]);
      }
      for (const key in datas) {
        formData.append(key, datas[key]);
      }
      this.setState({ loading: "edit" }, () => {
        axios
          .patch(config.api + config.path.fg, formData, {
            params: {
              id_fg_detail: this.props.data.id_fg_detail,
            },
          })
          .then(
            function (response) {
              this.setState({ loading: false }, () => {
                if (response.data.success) {
                  this.props.onAfterEdit(response.data.data);
                  alert("Berhasil untuk merubah data");
                } else {
                  alert(
                    "Gagal untuk merubah data : " + response.data.error.messages
                  );
                }
              });
            }.bind(this)
          )
          .catch((e) => {
            this.setState({ loading: false });
            alert("Gagal untuk menyimpan data");
          });
      });
    }
  }
  onSubmit(datas) {
    if (this.props.data) {
      this.EditRecord(datas);
    } else {
      this.InsertRecord(datas);
    }
  }

  render() {
    const fields = this.getFields();
    return (
      <div
        className={this.props.show ? "modal fade show" : "modal"}
        style={this.props.show ? { display: "block" } : undefined}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            {this.props.data === undefined ? undefined : (
              <>
                <Form
                  fields={fields}
                  onSubmit={this.onSubmit}
                  loading={this.state.loading}
                  defaultValues={this.props.data ? this.props.data : {}}
                >
                  <div className="modal-header">
                    <h5 className="modal-title">
                      {this.props.data !== undefined
                        ? this.props.data &&
                          this.props.data.id_fg_detail === null
                          ? "EDIT"
                          : "INSERT"
                        : ""}
                          FORM PEMERIKSAAN PRODUK AKHIR
                    </h5>
                    {this.props.onClose ? (
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        onClick={this.props.onClose}
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    ) : undefined}
                  </div>
                  <div className="modal-body">
                    <div className="container">
                      <div className="row mb-3">
                        <div className="col-sm-8">
                          <label htmlFor="tanggal" className="form-label">
                            Tanggal : &nbsp;&nbsp;
                          </label>

                          <b>
                            {moment(this.props.header.tanggal)
                              .locale("id")
                              .format("dddd, DD MMM YYYY")}
                          </b>
                        </div>
                        <div className="col-sm-4">
                          <label htmlFor="" className="form-label">
                            Shift : &nbsp;&nbsp;
                          </label>
                          <b>{this.props.header.shiftLabel}</b>
                        </div>
                      </div>
                      <hr />
                      <div className="row mb-3">
                        <Label
                          name="jenis_produksi"
                          className="col-sm-3 col-form-label "
                        />
                        <div className="col-sm-9">
                          <Field
                            name="jenis_produksi"
                            options={this.state.productOptions}
                          />
                        </div>
                      </div>
                      <div className="row mb-3">
                        <Label
                          name="customer_spk"
                          className="col-sm-3 col-form-label "
                        />

                        <div className="col-sm-9">
                          <Field name="customer_spk" className="form-control" />
                        </div>
                      </div>
                      <div className="row mb-3">
                        <Label
                          name="cek"
                          className="col-sm-3 col-form-label "
                        />

                        <div className="col-sm-9">
                          <Field name="cek" className="form-control" />
                        </div>
                      </div>
                      <br />
                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <fieldset>
                            <legend>Kesesuian Item & Type</legend>
                            <table className="table table-bordered table-hovered table-striped table-hover">
                              <thead>
                                <tr>
                                  <th
                                    align="center"
                                    className="col-sm-2 text-center"
                                  >
                                    <Label name="karton" />
                                  </th>
                                  <th
                                    align="center"
                                    className="col-sm-2 text-center"
                                  >
                                    <Label name="innerbox" />
                                  </th>
                                  <th
                                    align="center"
                                    className="col-sm-2 text-center"
                                  >
                                    <Label name="film" />
                                  </th>
                                  <th
                                    align="center"
                                    className="col-sm-3 text-center"
                                  >
                                    <Label name="layer_hangger" />
                                  </th>
                                  <th
                                    align="center"
                                    className="col-sm-3 text-center"
                                  >
                                    <Label name="toples_kaleng" />
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <Field
                                      name="karton"
                                      className="form-check-input "
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="innerbox"
                                      className="form-check-input "
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="film"
                                      className="form-check-input  "
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="layer_hangger"
                                      className="form-check-input "
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="toples_kaleng"
                                      className="form-check-input"
                                    />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </fieldset>
                        </div>
                      </div>

                      <fieldset>
                        <legend>Kesesuaian Jumlah</legend>
                        <div className="row mb-3">
                          <Label
                            name="karton_per_pcs"
                            className="col-sm-3 form-label"
                          />
                          <div className="col-sm-2">
                            <Field
                              name="karton_per_pcs"
                              className=" form-control"
                            />
                          </div>
                          <div className="col-sm-2">
                          <Field
                              name="satuan"
                              options={this.getOpsiSatuan()}
                            />
                          </div>
                          <Label
                            name="inner_per_pcs"
                            className="col-sm-2 form-label"
                          />
                          <div className="col-sm-3">
                            <Field
                              name="inner_per_pcs"
                              className="form-control"
                            />
                          </div>
                        </div>
                      </fieldset>

                      <fieldset>
                        <legend>Kesesuaian Berat</legend>
                        <div className="row mb-3">
                          <Label
                            name="karton_per_gr"
                            className="col-sm-3 form-label"
                          />
                          <div className="col-sm-4">
                            <Field
                              name="karton_per_gr"
                              className="form-control"
                            />
                          </div>
                          <Label
                            name="inner_per_gr"
                            className="col-sm-2 form-label"
                          />
                          <div className="col-sm-3">
                            <Field
                              name="inner_per_gr"
                              className="form-control"
                            />
                          </div>
                        </div>
                      </fieldset>

                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <fieldset>
                            <legend>Kode Produksi</legend>
                            <table className="table table-bordered table-hovered table-striped table-hover">
                              <thead>
                                <tr>
                                  <th align="center" className="text-center">
                                    <Label name="kodeprod_karton" />
                                  </th>
                                  <th align="center" className="text-center">
                                    <Label name="kodeprod_inner" />
                                  </th>
                                  <th align="center" className="text-center">
                                    <Label name="kodeprod_toples" />
                                  </th>
                                  <th align="center" className="text-center">
                                    <Label name="kodeprod_film" />
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td align="center" className="col-sm-2 ">
                                    <Field
                                      name="kodeprod_karton"
                                      className="form-check-input "
                                    />
                                  </td>
                                  <td align="center" className="col-sm-2 ">
                                    <Field
                                      name="kodeprod_inner"
                                      className="form-check-input "
                                    />
                                  </td>
                                  <td align="center" className="col-sm-2 ">
                                    <Field
                                      name="kodeprod_toples"
                                      className="form-check-input  "
                                    />
                                  </td>
                                  <td align="center" className="col-sm-2">
                                    <Field
                                      name="kodeprod_film"
                                      className="form-check-input"
                                    />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </fieldset>
                        </div>
                      </div>
                      <div className="row mb-3">
                        <Label name="mesin" className="col-sm-2 form-label" />
                        <div className="col-sm-3">
                          <Field
                            name="mesin"
                            options={this.state.mesinOptions}
                          />
                        </div>
                        <Label
                          name="prod_exp"
                          className="col-sm-3 form-label"
                        />
                        <div className="col-sm-4">
                          <Field name="prod_exp" className="form-control" />
                        </div>
                      </div>
                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <fieldset>
                            <legend>Kasus Secara Visual</legend>
                            <table className="table table-bordered table-hovered table-striped table-hover">
                              <thead>
                                <tr>
                                  <th align="center" className="text-center">
                                    <Label name="sealing" />
                                  </th>
                                  <th align="center" className="text-center">
                                    <Label name="benda_asing" />
                                  </th>
                                  <th align="center" className="text-center">
                                    <Label name="kondisi_perekat" />
                                  </th>
                                  <th align="center" className="text-center">
                                    <Label name="kondisi_kemasan" />
                                  </th>
                                  <th align="center" className="text-center">
                                    <Label name="other" />
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td align="center" className="col-sm-2 ">
                                    <Field
                                      name="sealing"
                                      className="form-control"
                                    />
                                  </td>
                                  <td align="center" className="col-sm-2 ">
                                    <Field
                                      name="benda_asing"
                                      className="form-control  "
                                    />
                                  </td>
                                  <td align="center" className="col-sm-2 ">
                                    <Field
                                      name="kondisi_perekat"
                                      className="form-control  "
                                    />
                                  </td>
                                  <td align="center" className="col-sm-2 ">
                                    <Field
                                      name="kondisi_kemasan"
                                      className="form-control  "
                                    />
                                  </td>
                                  <td align="center" className="col-sm-2 ">
                                    <Field
                                      name="other"
                                      className="form-control  "
                                    />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </fieldset>
                        </div>
                      </div>
                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <Label name="keterangan" className="form-label" />
                          <br />
                          <Field name="keterangan" className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer">
                    {this.props.onClose ? (
                      <button
                        type="button"
                        className="btn btn-outline-secondary"
                        data-bs-dismiss="modal"
                        onClick={this.props.onClose}
                        disabled={this.state.loading}
                      >
                        Close
                      </button>
                    ) : undefined}
                    {this.props.data !== undefined ? (
                      this.props.data === null ? (
                        <>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}
                          >
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}
                          >
                            {this.state.loading === "insert" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      ) : (
                        <>
                          <button
                            className="btn btn-danger"
                            type="button"
                            disabled={this.state.loading}
                            onClick={(e) => {
                              e.preventDefault();
                              this.DeleteRecord();
                            }}
                          >
                            {this.state.loading === "delete" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Deleting...</span>
                              </>
                            ) : (
                              "Delete"
                            )}
                          </button>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}
                          >
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}
                          >
                            {this.state.loading === "edit" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      )
                    ) : undefined}
                  </div>
                </Form>
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default FGForm;
