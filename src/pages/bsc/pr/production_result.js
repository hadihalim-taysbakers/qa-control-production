import React from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import Select from "react-select";
import Approval from "../../../Approval";
import { formatTime } from "../../../utils/formatter";
import config from "../../../config";
import PRForm from "./prForm";
import PRGraph from "./GraphPR";
import Main from "../../../layout/main";
import Field from "../../../components/Field";
import Label from "../../../components/Label";
import Form, { FormContext } from "../../../components/Form";

class seasoning extends React.Component {
  refForm = React.createRef();
  fields = {
    min_weight_after_oil: {
      name: "min_weight_after_oil",
      id: "min_weight_after_oil",
      label: "Min",
      attributeName: "Berat Minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    max_weight_after_oil: {
      name: "max_weight_after_oil",
      id: "max_weight_after_oil",
      label: "Max",
      attributeName: "Berat Maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    min_weight_after_seasoning: {
      name: "min_weight_after_seasoning",
      id: "min_weight_after_seasoning",
      label: "Min",
      attributeName: "Berat Minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    max_weight_after_seasoning: {
      name: "max_weight_after_seasoning",
      id: "max_weight_after_seasoning",
      label: "Max",
      attributeName: "Berat Maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
  };
  state = {
    exists: [],
    FormData: undefined,
    graph: undefined,
    approveCreator: undefined,
    approveVerifier: undefined,
    approveChecker: undefined,
    approveKnower: undefined,
    creator_name: undefined,
    creator_title: undefined,
    creator_sign: undefined,
    created: undefined,
    verifier_name: undefined,
    verifier_title: undefined,
    verifier_sign: undefined,
    verified: undefined,
    checker_name: undefined,
    checker_title: undefined,
    checker_sign: undefined,
    checked: undefined,
    knower_name: undefined,
    knower_title: undefined,
    knower_sign: undefined,
    knowed: undefined,
    edit: undefined,
    loading: false,
    name: "",
    email: "",
    tanggal: "",
    typeprod: "",
    variantprod: "",
    mesin: "",
    shiftr: "",
    operator: "",
    headerData: undefined,
    no_doc: "FRM.QCRD.BSC.02.04; Rev.04",

    datas: [],
    searchString: [],
    // timeCheckOptions: [],
    shiftOptions: [],
    mesinOptions: [],
    productOptions: [],
    variantOptions: [],
  };
  getFields() {
    const fields = { ...this.fields };
    if (
      !(
        this.state.headerData &&
        this.state.headerData.min_weight_after_oil > 0 &&
        this.state.headerData.max_weight_after_oil > 0
      )
    ) {
      delete fields.min_weight_after_oil;
      delete fields.max_weight_after_oil;
    }

    if (
      !(
        this.state.headerData &&
        this.state.headerData.min_weight_after_seasoning > 0 &&
        this.state.headerData.max_weight_after_seasoning > 0
      )
    ) {
      delete fields.min_weight_after_seasoning;
      delete fields.max_weight_after_seasoning;
    }

    return fields;
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      (prevState.tanggal !== this.state.tanggal ||
        prevState.shiftr !== this.state.shiftr ||
        prevState.typeprod !== this.state.typeprod ||
        prevState.variantprod !== this.state.variantprod ||
        prevState.mesin !== this.state.mesin) &&
      this.state.tanggal &&
      this.state.shiftr &&
      this.state.typeprod &&
      this.state.variantprod &&
      this.state.mesin
    ) {
      const url = config.api + config.path.season;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            type_produksi: this.state.typeprod,
            variant_produksi: this.state.variantprod,
            mesin: this.state.mesin,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          const headerData = {};
          if (data.header) {
            headerData.min_weight_after_oil = data.header.min_weight_after_oil;
            headerData.max_weight_after_oil = data.header.max_weight_after_oil;
            headerData.min_weight_after_seasoning =
              data.header.min_weight_after_seasoning;
            headerData.max_weight_after_seasoning =
              data.header.max_weight_after_seasoning;
          } else {
            for (let i = 0; i < this.productTypes.length; i++) {
              if (
                parseInt(this.productTypes[i].id) ===
                parseInt(this.state.typeprod)
              ) {
                headerData.min_weight_after_oil =
                  this.productTypes[i].min_weight_after_oil;
                headerData.max_weight_after_oil =
                  this.productTypes[i].max_weight_after_oil;
                headerData.min_weight_after_seasoning =
                  this.productTypes[i].min_weight_after_seasoning;
                headerData.max_weight_after_seasoning =
                  this.productTypes[i].max_weight_after_seasoning;
              }
            }
          }
          this.setState({
            creator_name: data.header ? data.header.creator_name : undefined,
            creator_title: data.header ? data.header.creator_title : undefined,
            creator_sign: data.header ? data.header.creator_sign : undefined,
            created: data.header ? data.header.created : undefined,
            verifier_name: data.header ? data.header.verifier_name : undefined,
            verifier_title: data.header
              ? data.header.verifier_title
              : undefined,
            verifier_sign: data.header ? data.header.verifier_sign : undefined,
            verified: data.header ? data.header.verified : undefined,
            checker_name: data.header ? data.header.checker_name : undefined,
            checker_title: data.header ? data.header.checker_title : undefined,
            checker_sign: data.header ? data.header.checker_sign : undefined,
            checked: data.header ? data.header.checked : undefined,
            knower_name: data.header ? data.header.knower_name : undefined,
            knower_title: data.header ? data.header.knower_title : undefined,
            knower_sign: data.header ? data.header.knower_sign : undefined,
            knowed: data.header ? data.header.knowed : undefined,
            loading: false,
            datas: data.datas,
            operator: data.header ? data.header.operator : "",
            headerData: headerData,
          });
        });
    } else if (
      prevState.typeprod !== this.state.typeprod &&
      this.state.typeprod
    ) {
      for (let i = 0; i < this.productTypes.length; i++) {
        if (
          parseInt(this.productTypes[i].id) === parseInt(this.state.typeprod)
        ) {
          const min_weight_after_oil =
            this.productTypes[i].min_weight_after_oil;
          const max_weight_after_oil =
            this.productTypes[i].max_weight_after_oil;
          const min_weight_after_seasoning =
            this.productTypes[i].min_weight_seasoning;
          const max_weight_after_seasoning =
            this.productTypes[i].max_weight_seasoning;
          this.setState({
            min_weight_after_oil: min_weight_after_oil,
            max_weight_after_oil: max_weight_after_oil,
            min_weight_after_seasoning: min_weight_after_seasoning,
            max_weight_after_seasoning: max_weight_after_seasoning,
          });
        }
      }
    }
    if (prevState.tanggal !== this.state.tanggal && this.state.tanggal) {
      const url = config.api + config.path.season;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          this.setState({
            exists: data.datas,
          });
        });
    }
  }

  submitHeaderChange(datas) {
    if (
      this.state.tanggal &&
      this.state.shiftr &&
      this.state.typeprod &&
      this.state.variantprod &&
      this.state.mesin
    ) {
      this.setState({ loading: true }, () => {
        const formData = new FormData();
        for (const key in datas) {
          formData.append(key, datas[key]);
        }

        formData.append("no_doc", this.state.no_doc);
        axios
          .patch(config.api + config.path.season, formData, {
            "Content-Type": "application/x-www-form-urlencoded",
            params: {
              tanggal: this.state.tanggal,
              shiftr: this.state.shiftr,
              mesin: this.state.mesin,
              type_produksi: this.state.typeprod,
              variant_produksi: this.state.variantprod,
            },
          })
          .then((res) => {
            const headerData = { ...this.state.headerData };
            for (const key in datas) {
              headerData[key] = datas[key];
            }
            this.setState({
              loading: false,
              headerData: headerData,
            });
          })
          .catch(() => {
            this.setState({
              loading: false,
            });
            alert("Gagal insert data");
          });
      });
    }
  }
  componentDidMount() {
    const yourDate = new Date();
    this.setState({ tanggal: yourDate.toISOString().split("T")[0] });
  }

  async getOptions() {
    const resShift = await axios.get(config.api + config.path.master_shift, {
      params: { posisi: config.page.season.posisi },
    });
    const dataShift = resShift.data;
    const shiftOptions = dataShift.map((d) => ({
      value: d.id,
      label: d.judul,
    }));
    this.setState({ shiftOptions: shiftOptions });
    const resMesin = await axios.get(config.api + config.path.master_mesin);
    const dataMesin = resMesin.data;
    const mesinOptions = dataMesin.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    this.setState({ mesinOptions: mesinOptions });

    const resProduct = await axios.get(config.api + config.path.master_product);
    const dataProduct = resProduct.data;
    const productOptions = dataProduct.map((d) => ({
      value: d.id,
      label: d.product_name,
    }));
    this.productTypes = [...dataProduct];
    this.setState({ productOptions: productOptions });

    const resVariant = await axios.get(config.api + config.path.master_variant);
    const dataVariant = resVariant.data;
    const variantOptions = dataVariant.map((d) => ({
      value: d.id,
      label: d.variant_name,
      id_product: d.id_product,
    }));
    this.variantOptions = [...dataVariant];
    this.setState({ variantOptions: variantOptions });
  }

  componentWillMount() {
    this.getOptions();
  }

  setEdit(ind) {
    this.setState({
      formData: this.state.datas[ind],
    });
  }

  submitCreatorApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("creator_name", data.name);
      formData.append("creator_title", data.title);
      formData.append("creator_sign", data.sign);
      axios
        .patch(config.api + config.path.season, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
            variant_produksi: this.state.variantprod,
          },
        })
        .then((res) => {
          this.setState({
            approveCreator: undefined,
            creator_name: res.data.header.creator_name,
            creator_title: res.data.header.creator_title,
            creator_sign: res.data.header.creator_sign,
            created: res.data.header.created,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveCreator: undefined,
            creator_name: undefined,
            creator_title: undefined,
            creator_sign: undefined,
            created: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitVerifierApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("verifier_name", data.name);
      formData.append("verifier_title", data.title);
      formData.append("verifier_sign", data.sign);
      axios
        .patch(config.api + config.path.season, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
            variant_produksi: this.state.variantprod,
          },
        })
        .then((res) => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: res.data.header.verifier_name,
            verifier_title: res.data.header.verifier_title,
            verifier_sign: res.data.header.verifier_sign,
            verified: res.data.header.verified,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: undefined,
            verifier_title: undefined,
            verifier_sign: undefined,
            verified: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitCheckerApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("checker_name", data.name);
      formData.append("checker_title", data.title);
      formData.append("checker_sign", data.sign);
      axios
        .patch(config.api + config.path.season, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
            variant_produksi: this.state.variantprod
          },
        })
        .then((res) => {
          this.setState({
            approveChecker: undefined,
            checker_name: res.data.header.checker_name,
            checker_title: res.data.header.checker_title,
            checker_sign: res.data.header.checker_sign,
            checked: res.data.header.checked,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveCreator: undefined,
            checker_name: undefined,
            checker_title: undefined,
            checker_sign: undefined,
            checked: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitKnowerApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("knower_name", data.name);
      formData.append("knower_title", data.title);
      formData.append("knower_sign", data.sign);
      axios
        .patch(config.api + config.path.season, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
            variant_produksi: this.state.variantprod,
          },
        })
        .then((res) => {
          this.setState({
            approveKnower: undefined,
            knower_name: res.data.header.knower_name,
            knower_title: res.data.header.knower_title,
            knower_sign: res.data.header.knower_sign,
            knowed: res.data.header.knowed,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveKnower: undefined,
            knower_name: undefined,
            knower_title: undefined,
            knower_sign: undefined,
            knowed: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  getVariantOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.variantOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.variantprod) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
            parseInt(exists[j]["type_produksi"]) ===
              parseInt(this.state.typeprod) &&
            parseInt(exists[j]["variant_produksi"]) ===
              parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getVariantOptions() {
    const options = JSON.parse(JSON.stringify(this.state.variantOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    const newproduct = [];
    let flag = false;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.typeprod) === parseInt(options[i].id_product)) {
        flag = false;
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
            parseInt(exists[j]["type_produksi"]) ===
              parseInt(this.state.typeprod) &&
            parseInt(exists[j]["variant_produksi"]) ===
              parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            flag = true;
            
          }
        }
        if (flag) {
          newproduct.push({
            value: options[i].value,
            label: <b>{options[i].label}</b>,
          });
        } else {
          newproduct.push({
            value: options[i].value,
            label: options[i].label,
          });
        }
      }
    }
    return newproduct;
  }

  getProductOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.typeprod) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
            parseInt(exists[j]["type_produksi"]) ===
              parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getProductOptions() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
          parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
          parseInt(exists[j]["type_produksi"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getMesinOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.mesin) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getMesinOptions() {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
          parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getShiftOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.shiftr) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getShiftName(id) {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getShiftOptions() {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getProductName(id) {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return;
  }
  getVariantName(id) {
    const options = JSON.parse(JSON.stringify(this.state.variantOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return;
  }
  getMesinName(id) {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getGraphData(fields, min, max, title) {
    const labels = [];
    const dataMin = [];
    const dataMax = [];
    const datas = {};
    for (const key in fields) {
      datas[key] = [];
    }
    for (var i = 0; i < this.state.datas.length; i++) {
      for (const key in fields) {
        if (this.state.datas[i][key] > 0) {
          datas[key].push(this.state.datas[i][key]);
        } else {
          datas[key].push(null);
        }
      }

      labels.push(this.state.datas[i].time_check);
      dataMin.push(min);
      dataMax.push(max);
    }
    const datasets = [];
    for (const key in fields) {
      datasets.push({
        label: fields[key].label,
        data: datas[key],
        borderColor: fields[key].color,
      });
    }
    return {
      title: title,
      data: {
        labels: labels,
        datasets: [
          ...datasets,
          {
            label: "Over",
            fill: "end",
            data: dataMax,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
          },
          {
            label: "Under",
            data: dataMin,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
            fill: "start",
          },
        ],
      },
    };
  }
  removeExistData(typeprod, mesin, shift, variantprod) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (
        exists[i].type_produksi === typeprod &&
        exists[i].mesin === mesin &&
        exists[i].shift === shift &&
        exists[i].variant_produksi === variantprod
      ) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) - 1;
      }
    }
    if (isExist === false) {
      exists.push({
        type_produksi: typeprod,
        mesin: mesin,
        shiftr: shift,
        variant_produksi: variantprod,

        qty: "0",
      });
    }
    return exists;
  }
  getProductType() {
    if (this.productTypes) {
      for (let i = 0; i < this.productTypes.length; i++) {
        if (
          parseInt(this.productTypes[i].id) === parseInt(this.state.typeprod)
        ) {
          return this.productTypes[i];
        }
      }
    }
    return null;
  }
  addExistData(typeprod, mesin, shift, variantprod) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (
        exists[i].type_produksi === typeprod &&
        exists[i].mesin === mesin &&
        exists[i].shift === shift &&
        exists[i].variant_produksi === variantprod
      ) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) + 1;
      }
    }
    if (isExist === false) {
      exists.push({
        type_produksi: typeprod,
        mesin: mesin,
        shift: shift,
        variant_produksi: variantprod,
        qty: "1",
      });
    }
    return exists;
  }
  render() {
    const productType = this.getProductType();
    const min_weight_after_oil = productType
      ? parseFloat(productType.min_weight_after_oil)
      : 0;
    const max_weight_after_oil = productType
      ? parseFloat(productType.max_weight_after_oil)
      : 0;
    const min_weight_after_seasoning = productType
      ? parseFloat(productType.min_weight_after_seasoning)
      : 0;
    const max_weight_after_seasoning = productType
      ? parseFloat(productType.max_weight_after_seasoning)
      : 0;
    const hd_min_weight_after_oil = this.state.headerData
      ? parseFloat(this.state.headerData.min_weight_after_oil)
      : 0;
    const hd_max_weight_after_oil = this.state.headerData
      ? parseFloat(this.state.headerData.max_weight_after_oil)
      : 0;
    const hd_min_weight_after_seasoning = this.state.headerData
      ? parseFloat(this.state.headerData.min_weight_after_seasoning)
      : 0;
    const hd_max_weight_after_seasoning = this.state.headerData
      ? parseFloat(this.state.headerData.max_weight_after_seasoning)
      : 0;

    let colspannum = 30;
    if (min_weight_after_oil > 0 && max_weight_after_oil > 0) {
      colspannum = colspannum + 3;
    }

    if (min_weight_after_seasoning > 0 && max_weight_after_seasoning > 0)
      colspannum = colspannum + 3;

    return (
      <>
        <Main>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm">
                <h4 className="page-header text-center">
                  EXAMINATION OF SPRAY PRODUCTION RESULT FOR OIL & BISCUIT
                  SEASONING
                </h4>
                <h6 className="page-header text-center">
                  Pemeriksaan Hasil Produksi Spray Minyak & Bumbu Biskuit
                </h6>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-2 form-group">
                <label htmlFor="tanggal">Date :</label>
                <input
                  disabled={this.state.loading}
                  required
                  type="date"
                  name="tanggal"
                  id="tanggal"
                  className="form-control"
                  value={this.state.tanggal}
                  onChange={(e) => this.setState({ tanggal: e.target.value })}
                />
              </div>
              <div className="col-sm-2 form-group">
                <label htmlFor="shiftr">Shift :</label>
                <Select
                  isDisabled={this.state.loading}
                  name="shiftr"
                  id="shiftr"
                  options={this.getShiftOptions()}
                  value={this.getShiftOptionValue()}
                  onChange={(e) => {
                    this.setState({ shiftr: e.value });
                  }}
                />
              </div>
              <div className="col-sm-2 form-group">
                <label htmlFor="shiftr">Mesin :</label>
                <Select
                  isDisabled={this.state.loading}
                  name="mesin"
                  id="mesin"
                  options={this.getMesinOptions()}
                  value={this.getMesinOptionValue()}
                  onChange={(e) => {
                    this.setState({ mesin: e.value });
                  }}
                />
              </div>
              <div className="col-sm-3 form-group">
                <label htmlFor="typeprod">Type Production</label>
                <Select
                  isDisabled={this.state.loading}
                  name="typeprod"
                  id="typeprod"
                  options={this.getProductOptions()}
                  value={this.getProductOptionValue()}
                  onChange={(e) => {
                    this.setState({ typeprod: e.value });
                  }}
                />
              </div>
              <div className="col-sm-3 form-group">
                <label htmlFor="variantprod">Variant</label>
                <Select
                  isDisabled={this.state.loading}
                  name="variantprod"
                  id="variantprod"
                  options={this.getVariantOptions()}
                  value={this.getVariantOptionValue()}
                  onChange={(e) => {
                    this.setState({ variantprod: e.value });
                  }}
                />
              </div>
            </div>

            {this.state.tanggal &&
            this.state.typeprod &&
            this.state.variantprod &&
            this.state.mesin &&
            this.state.shiftr ? (
              <>
                <div className="row">
                  <div className="col-sm-4">
                    <font size="2">
                      Pengambilan sample setiap 30 Menit, sebanyak 10 pcs per
                      sisi
                    </font>
                  </div>
                  <div className="col-sm-4 text-center">
                    <font size="2">Note : L : Left, C : Center & R: Right</font>
                  </div>
                  <div className="col-sm-4 text-right">
                    <font size="2">No. Dok : {this.state.no_doc}</font>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12">
                    {this.fields ? (
                      <Form
                        fields={this.getFields()}
                        defaultValues={this.state.headerData}
                        loading={this.state.loading}
                        onSubmit={(data) => {
                          this.submitHeaderChange(data);
                        }}>
                        <FormContext.Consumer>
                          {({ submit, validate, errors }) => (
                            <table className="table data table-bordered table-striped table-hover seasoning">
                              <thead>
                                <tr>
                                  <th className="title">Target</th>
                                  <th
                                    className="title"
                                    rowSpan={3}
                                    justifyContent="center"
                                    align-text="center"
                                    align-vertical="middle">
                                    No.Lot Setelah Seasoning
                                  </th>
                                  {min_weight_after_oil > 0 &&
                                  max_weight_after_oil > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_weight_after_oil" />
                                          <Field
                                            name="min_weight_after_oil"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_weight_after_oil" />
                                          <Field
                                            name="max_weight_after_oil"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  {min_weight_after_seasoning > 0 &&
                                  max_weight_after_seasoning > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_weight_after_seasoning" />
                                          <Field
                                            name="min_weight_after_seasoning"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_weight_after_seasoning" />
                                          <Field
                                            name="max_weight_after_seasoning"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  <th rowSpan="3" className="vertical">
                                    <span>Physical condition</span>
                                  </th>
                                  <th rowSpan="2" colSpan="2">
                                    Evenness
                                  </th>
                                  <th colSpan="9">ORGANOLEPTIC</th>
                                  <th rowSpan="3">Status (√/x)</th>
                                  <th rowSpan="3">Corrective Actions</th>
                                  <th rowSpan="3">Notes</th>
                                </tr>
                                <tr>
                                  <th rowSpan="3"> Time Check</th>
                                  {min_weight_after_oil > 0 &&
                                  max_weight_after_oil > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{ fontWeight: "bold" }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                oil_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                oil_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                oil_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData
                                                .min_weight_after_oil,
                                              this.state.headerData
                                                .max_weight_after_oil,
                                              "Weight After Oiling Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201;Weight After Oiling (gr)
                                      </button>
                                    </th>
                                  ) : undefined}
                                  {min_weight_after_seasoning > 0 &&
                                  min_weight_after_seasoning > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{ fontWeight: "bold" }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                season_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                season_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                season_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData
                                                .min_weight_after_seasoning,
                                              this.state.headerData
                                                .max_weight_after_seasoning,
                                              "Weight After Seasoning Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Weight After Seasoning (gr)
                                      </button>
                                    </th>
                                  ) : undefined}

                                  <th colSpan={"6"} className="title">
                                    Visual / Kontaminasi (pcs)
                                  </th>
                                  <th rowSpan={"2"} className="title vertical">
                                    <span>Color (Warna)</span>
                                  </th>
                                  <th rowSpan={"2"} className="title vertical">
                                    <span>Taste (Rasa)</span>
                                  </th>
                                  <th rowSpan={"2"} className="title vertical">
                                    <span>Smell (Bau)</span>
                                  </th>
                                </tr>
                                <tr>
                                  {min_weight_after_oil > 0 &&
                                  max_weight_after_oil > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  {min_weight_after_seasoning > 0 &&
                                  max_weight_after_seasoning > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  <th scope="col" className="vertical">
                                    <span>Oil</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Seasoning</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Logam</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Plastik</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Rambut</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Oil</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Benang</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Other</span>
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.datas.map((season, index) => {
                                  const season_left = parseFloat(
                                    season.season_left
                                  );
                                  const season_center = parseFloat(
                                    season.season_center
                                  );
                                  const season_right = parseFloat(
                                    season.season_right
                                  );
                                  const oil_left = parseFloat(season.oil_left);
                                  const oil_center = parseFloat(
                                    season.oil_center
                                  );
                                  const oil_right = parseFloat(
                                    season.oil_right
                                  );
                                  return (
                                    <tr key={index}>
                                      <td className="action">
                                        {!this.state.verifier_name ? (
                                          <>
                                            <button
                                              type="button"
                                              className="btn btn-outline-info btn-block btn-sm"
                                              onClick={() => {
                                                this.setEdit(index);
                                              }}>
                                              &#128269;
                                            </button>

                                            {formatTime(season.time_check)}
                                          </>
                                        ) : (
                                          <>{formatTime(season.time_check)} </>
                                        )}
                                      </td>
                                      <td>{season.no_lot_batch}</td>
                                      {min_weight_after_oil > 0 &&
                                      max_weight_after_oil > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                oil_left <
                                                hd_min_weight_after_oil
                                                  ? "red"
                                                  : oil_left >
                                                    hd_max_weight_after_oil
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {oil_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                oil_center <
                                                hd_min_weight_after_oil
                                                  ? "red"
                                                  : oil_center >
                                                    hd_max_weight_after_oil
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {oil_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                oil_right <
                                                hd_min_weight_after_oil
                                                  ? "red"
                                                  : oil_right >
                                                    hd_max_weight_after_oil
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {oil_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_weight_after_seasoning > 0 &&
                                      max_weight_after_seasoning > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                season_left <
                                                hd_min_weight_after_seasoning
                                                  ? "red"
                                                  : season_left >
                                                    hd_max_weight_after_seasoning
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {season_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                season_center <
                                                hd_min_weight_after_seasoning
                                                  ? "red"
                                                  : season_center >
                                                    hd_max_weight_after_seasoning
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {season_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                season_right <
                                                hd_min_weight_after_seasoning
                                                  ? "red"
                                                  : season_right >
                                                    hd_max_weight_after_seasoning
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {season_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}

                                      <td align="center">
                                        {season.physical === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {season.even_oil === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {season.even_season === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td>{season.visual_logam}</td>
                                      <td>{season.visual_plastik}</td>
                                      <td>{season.visual_rambut}</td>
                                      <td>{season.visual_oil}</td>
                                      <td>{season.visual_benang}</td>
                                      <td>{season.visual_other}</td>
                                      <td align="center">
                                        {season.warna === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {season.rasa === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {season.bau === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>

                                      <td align="center">
                                        {season.status === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td>{season.corrective_action}</td>
                                      <td>{season.note}</td>
                                    </tr>
                                  );
                                })}
                              </tbody>

                              {!this.state.verifier_name ? (
                                <tfoot>
                                  <tr>
                                    <td colSpan={colspannum} align="center">
                                      <button
                                        type="button"
                                        className="btn btn-primary"
                                        onClick={(e) => {
                                          if (validate()) {
                                            this.setState({
                                              formData: null,
                                            });
                                          } else {
                                            alert(
                                              "Silahkan perbaiki data minimum dan maximum"
                                            );
                                          }
                                        }}
                                        value="Add">
                                        &#10010; Add
                                      </button>
                                    </td>
                                  </tr>
                                </tfoot>
                              ) : undefined}
                            </table>
                          )}
                        </FormContext.Consumer>
                      </Form>
                    ) : undefined}
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6 xs-12"></div>
                  <div className="col-md-6 xs-12">
                    <table className="sign" cellPadding={3}>
                      <tbody>
                        <tr>
                        <th width={"20%"}>&nbsp;</th>
                          <th width={"20%"}> Dibuat Oleh,</th>
                          <th width={"20%"}> Diverifikasi Oleh,</th>
                          <th width={"20%"}> Diperiksa Oleh,</th>
                          <th width={"20%"}>Mengetahui Oleh, </th>
                        </tr>
                        <tr>
                          <th>Tanda Tangan</th>
                          {this.state.creator_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.creator_sign}
                                alt="creator sign"
                                width={"100%"}
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 || this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveCreator: {
                                      name: "",
                                      title: "Operator",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.verifier_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.verifier_sign}
                                width={"100%"}
                                alt="verifier sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveVerifier: {
                                      name: "",
                                      title: "Quality Control",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.checker_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.checker_sign}
                                width={"100%"}
                                alt="checker sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  !this.state.verifier_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveChecker: {
                                      name: "",
                                      title: "Supervisor QC",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.knower_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.knower_sign}
                                width={"100%"}
                                alt="knower sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  !this.state.verifier_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveKnower: {
                                      name: "",
                                      title: "Manager QA",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                        </tr>
                        <tr>
                          <th>Nama</th>
                          {this.state.creator_name ? (
                            <td align="center" valign="middle">
                              {this.state.creator_name}
                            </td>
                          ) : undefined}
                          {this.state.verifier_name ? (
                            <td align="center" valign="middle">
                              {this.state.verifier_name}
                            </td>
                          ) : undefined}
                          {this.state.checker_name ? (
                            <td align="center" valign="middle">
                              {this.state.checker_name}
                            </td>
                          ) : undefined}
                          {this.state.knower_name ? (
                            <td align="center" valign="middle">
                              {this.state.knower_name}
                            </td>
                          ) : undefined}
                        </tr>
                        <tr>
                          <th>Jabatan</th>
                          {this.state.creator_title ? (
                            <td align="center" valign="middle">
                              {this.state.creator_title}
                            </td>
                          ) : undefined}
                          {this.state.verifier_title ? (
                            <td align="center" valign="middle">
                              {this.state.verifier_title}
                            </td>
                          ) : undefined}
                           {this.state.checker_title ? (
                            <td align="center" valign="middle">
                              {this.state.checker_title}
                            </td>
                          ) : undefined}
                          {this.state.knower_title ? (
                            <td align="center" valign="middle">
                              {this.state.knower_title}
                            </td>
                          ) : undefined}
                        </tr>
                        <tr>
                          <th>Tanggal</th>
                          {this.state.created ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.created).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.verified ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.verified).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                           {this.state.checked ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.verified).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.knowed ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.knowed).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </>
            ) : undefined}
          </div>
        </Main>
        <Approval
          loading={this.state.loading}
          title="Dibuat oleh,"
          show={this.state.approveCreator ? true : false}
          data={this.state.approveCreator}
          onSubmit={(data) => {
            this.submitCreatorApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveCreator: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Diverifikasi oleh,"
          show={this.state.approveVerifier ? true : false}
          data={this.state.approveVerifier}
          onSubmit={(data) => {
            this.submitVerifierApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveVerifier: undefined,
            });
          }}
        />
         <Approval
          loading={this.state.loading}
          title="Diperiksa oleh,"
          show={this.state.approveChecker ? true : false}
          data={this.state.approveChecker}
          onSubmit={(data) => {
            this.submitCheckerApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveChecker: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Mengetahui oleh,"
          show={this.state.approveKnower ? true : false}
          data={this.state.approveKnower}
          onSubmit={(data) => {
            this.submitKnowerApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveKnower: undefined,
            });
          }}
        />
        <PRGraph
          title="GRAPH EXAMINATION OF SPRAY PRODUCTION RESULT FOR OIL & BISCUIT SEASONING"
          tanggal={this.state.tanggal}
          shift={this.getShiftName(this.state.shiftr)}
          mesin={this.getMesinName(this.state.mesin)}
          product={this.getProductName(this.state.typeprod)}
          variant={this.getVariantName(this.state.variantprod)}
          show={this.state.graph ? true : false}
          data={this.state.graph ? this.state.graph.data : undefined}
          subtitle={this.state.graph ? this.state.graph.title : undefined}
          onClose={() => {
            this.setState({
              graph: undefined,
            });
          }}
        />
        <PRForm
          header={{
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            shiftLabel: this.getShiftName(this.state.shiftr),

            mesin: this.state.mesin,
            mesinLabel: this.getMesinName(this.state.mesin),
            typeprod: this.state.typeprod,
            typeprodLabel: this.getProductName(this.state.typeprod),
            variantprod: this.state.variantprod,
            variantprodLabel: this.getVariantName(this.state.variantprod),

            min_weight_after_oil: this.state.headerData
              ? this.state.headerData.min_weight_after_oil
              : undefined,
            max_weight_after_oil: this.state.headerData
              ? this.state.headerData.max_weight_after_oil
              : undefined,
            min_weight_after_seasoning: this.state.headerData
              ? this.state.headerData.min_weight_after_seasoning
              : undefined,
            max_weight_after_seasoning: this.state.headerData
              ? this.state.headerData.max_weight_after_seasoning
              : undefined,
          }}
          data={this.state.formData}
          show={this.state.formData !== undefined}
          onAfterDelete={(id_seasoning_detail) => {
            const currDatas = [...this.state.datas];
            const newDatas = [];
            for (let i = 0; i < currDatas.length; i++) {
              if (
                parseInt(currDatas[i].id_seasoning_detail) !==
                parseInt(id_seasoning_detail)
              ) {
                newDatas.push(currDatas[i]);
              }
            }
            this.setState({
              exists: this.removeExistData(
                this.state.typeprod,
                this.state.mesin,
                this.state.shiftr,
                this.state.variantprod
              ),
              datas: newDatas,
              formData: undefined,
            });
          }}
          onAfterEdit={(data) => {
            const currDatas = [...this.state.datas];
            for (let i = 0; i < currDatas.length; i++) {
              if (
                currDatas[i].id_seasoning_detail === data.id_seasoning_detail
              ) {
                currDatas[i] = data;
              }
            }
            this.setState({
              datas: currDatas,
              formData: undefined,
            });
          }}
          onAfterInsert={(datas) => {
            this.setState({
              exists: this.addExistData(
                this.state.typeprod,
                this.state.mesin,
                this.state.shiftr,
                this.state.variantprod
              ),
              datas: datas,
              formData: undefined,
            });
          }}
          onClose={() => {
            this.setState({
              formData: undefined,
            });
          }}
        />
      </>
    );
  }
}

export default seasoning;
