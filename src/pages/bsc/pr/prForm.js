import React from "react";
import moment from "moment";
import "moment/locale/id";
import axios from "axios";
import config from "../../../config";
import Field from "../../../components/Field";
import * as Validator from "validatorjs";
import Form from "./../../../components/Form";
import Label from "./../../../components/Label";

class prForm extends React.Component {
  constructor(props) {
    super(props);
    this.fields = {
      time_check: {
        name: "time_check",
        id: "time_check",
        label: "Waktu Pengecekan",
        attributeName: "Waktu Pengecekan",
        type: "time",
        rules: ["required"],
      },
      no_lot_batch: {
        name: "no_lot_batch",
        id: "no_lot_batch",
        label: "No. Lot Batch",
        attributeName: "No. Lot Batch",
        type: "text",
        rules: ["required"],
      },
      oil_left: {
        name: "oil_left",
        id: "oil_left",
        label: "Oiling Left",
        attributeName: "Oiling Left",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      oil_center: {
        name: "oil_center",
        id: "oil_center",
        label: "Oiling Center",
        attributeName: "Oiling Center",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      oil_right: {
        name: "oil_right",
        id: " oil_right",
        label: "Oiling Right",
        attributeName: "Oiling Right",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      season_left: {
        name: "season_left",
        id: "season_left",
        label: "Seasoning Left",
        attributeName: "Seasoning Left",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      season_center: {
        name: "season_center",
        id: "season_center",
        label: "Seasoning Center",
        attributeName: "Seasoning Center",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      season_right: {
        name: "season_right",
        id: "season_right",
        label: "Seasoning Right",
        attributeName: "Seasoning Right",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      physical: {
        name: "physical",
        id: "physical",
        label: "Physical",
        attributeName: "Physical Condition",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },

      even_oil: {
        name: "even_oil",
        id: "even_oil",
        label: "Eveness Oil",
        attributeName: "Eveness Oil",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      even_season: {
        name: "even_season",
        id: "even_season",
        label: "Eveness Seasoning",
        attributeName: "Eveness Seasoning",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      visual_logam: {
        name: "visual_logam",
        id: "visual_logam",
        label: "Logam",
        attributeName: "Visual Logam",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      visual_plastik: {
        name: "visual_plastik",
        id: "visual_plastik",
        label: "Plastik",
        attributeName: "Visual Plastik",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      visual_rambut: {
        name: "visual_rambut",
        id: "visual_rambut",
        label: "Rambut",
        attributeName: "Visual Rambut",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      visual_oil: {
        name: "visual_oil",
        id: "visual_oil",
        label: "Oil",
        attributeName: "Visual Oil",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      visual_benang: {
        name: "visual_benang",
        id: "visual_benang",
        label: "Benang",
        attributeName: "Visual Benang",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      visual_other: {
        name: "visual_other",
        id: "visual_other",
        label: "Other",
        attributeName: "Visual Lainnya",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      warna: {
        name: "warna",
        id: "warna",
        label: "Warna",
        attributeName: "Warna",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      rasa: {
        name: "rasa",
        id: "rasa",
        label: "rasa",
        attributeName: "Rasa",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      bau: {
        name: "bau",
        id: "bau",
        label: "Bau",
        attributeName: "Bau",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      status: {
        name: "status",
        id: "status",
        label: "Status",
        attributeName: "Status",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      corrective_action: {
        name: "corrective_action",
        id: "corrective_action",
        label: "Corrective Action",
        attributeName: "Corrective Action",
        type: "text",
      },
      note: {
        name: "note",
        id: "note",
        label: "Notes",
        attributeName: "Notes",
        type: "text",
      },
    };
    this.state = {
      loading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  getFields() {
    const fields = { ...this.fields };
    const header = { ...this.props.header };
    if (header.min_weight_after_oil <= 0 && header.max_weight_after_oil <= 0) {
      delete fields.oil_left;
      delete fields.oil_center;
      delete fields.oil_right;
    }
    if (
      header.min_weight_after_seasoning <= 0 &&
      header.max_weight_after_seasoning <= 0
    ) {
      delete fields.season_left;
      delete fields.season_center;
      delete fields.season_right;
    }
    return fields;
  }
  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState(
      {
        datas: { ...this.state.datas, [name]: value },
      },
      () => {
        const valid = this.validateField(name, value);
        if (valid.valid === true) {
          this.setState({
            errors: { ...this.state.errors, [name]: [] },
          });
        } else {
          this.setState({
            errors: { ...this.state.errors, [name]: valid.errors[name] },
          });
        }
      }
    );
  }
  validateField(name, value) {
    const messages = Validator.getMessages("id");
    Validator.setMessages("id", messages);
    Validator.setAttributeNames(this.attributeNames);
    const validation = new Validator(
      { [name]: value },
      { [name]: this.rules[name] },
      messages
    );
    if (validation.passes()) {
      return { valid: true, errors: null };
    } else {
      const errors = validation.errors.all();
      return {
        valid: false,
        errors: errors,
      };
    }
  }
  InsertRecord(datas) {
    let formData = new FormData();
    for (const key in this.props.header) {
      formData.append(key, this.props.header[key]);
    }
    for (const key in datas) {
      formData.append(key, datas[key]);
    }
    this.setState({ loading: "insert" }, () => {
      axios({
        method: "post",
        url: config.api + config.path.season,
        data: formData,
        params: {
          tanggal: this.props.header.tanggal,
          shiftr: this.props.header.shiftr,
          mesin: this.props.header.mesin,
          type_produksi: this.props.header.typeprod,
          variant_produksi: this.props.header.variantprod,
        },
        config: { headers: { "Content-Type": "multipart/form-data" } },
      }).then(
        function (response) {
          this.setState({ loading: false }, () => {
            this.props.onAfterInsert(response.data.datas);
            alert("Berhasil untuk menyimpan data");
          });
        }.bind(this)
      );
    });
  }
  DeleteRecord() {
    if (this.props.data && this.props.data.id_seasoning_detail) {
      this.setState({ loading: "delete" }, () => {
        axios
          .delete(config.api + config.path.season, {
            params: {
              id: this.props.data.id_seasoning_detail,
            },
          })
          .then((res) => {
            if (res.data.success) {
              this.setState({ loading: false }, () => {
                this.props.onAfterDelete(res.data.id_seasoning_detail);
                alert("Berhasil untuk menghapus data");
              });
            } else {
              alert("gagal delete");
            }
          });
      });
    }
  }
  EditRecord(datas) {
    if (this.props.data) {
      let formData = new FormData();
      for (const key in this.props.header) {
        formData.append(key, this.props.header[key]);
      }
      for (const key in datas) {
        formData.append(key, datas[key]);
      }
      this.setState({ loading: "edit" }, () => {
        axios
          .patch(config.api + config.path.season, formData, {
            params: {
              id: this.props.data.id_seasoning_detail,
            },
          })
          .then(
            function (response) {
              this.setState({ loading: false }, () => {
                this.props.onAfterEdit(response.data.data);
                alert("Berhasil untuk merubah data");
              });
            }.bind(this)
          )
          .catch((e) => {
            this.setState({ loading: false });
            alert("Gagal untuk menyimpan data");
          });
      });
    }
  }
  onSubmit(datas) {
    if (this.props.data) {
      this.EditRecord(datas);
    } else {
      this.InsertRecord(datas);
    }
  }
  render() {
    const fields = this.getFields();
    return (
      <div
        className={this.props.show ? "modal fade show" : "modal"}
        style={this.props.show ? { display: "block" } : undefined}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            {this.props.data === undefined ? undefined : (
              <>
                <Form
                  fields={fields}
                  onSubmit={this.onSubmit}
                  loading={this.state.loading}
                  defaultValues={this.props.data ? this.props.data : {}}
                >
                  <div className="modal-header">
                    <h5 className="modal-title">
                      {this.props.data !== undefined
                        ? this.props.data &&
                          this.props.data.id_seasoning_detail === null
                          ? "EDIT"
                          : "INSERT"
                        : ""}
                      EXAMINATION OF SPRAY PRODUCTION RESULT FOR OIL & BISCUIT
                      SEASONING
                    </h5>
                    {this.props.onClose ? (
                      <button
                        type="button"
                        disabled={this.props.loading}
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        onClick={this.props.onClose}
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    ) : undefined}
                  </div>
                  <div className="modal-body">
                    <div className="row mb-3">
                      <div className="col-sm-3">
                        <label htmlFor="tanggal" className="form-label">
                          Tanggal :
                        </label>
                        <br />
                        <b>
                          {moment(this.props.header.tanggal)
                            .locale("id")
                            .format("dddd, DD MMM YYYY")}
                        </b>
                      </div>

                      <div className="col-sm-2">
                        <label htmlFor="tanggal" className="form-label">
                          Shift :
                        </label>
                        <br />
                        <b>{this.props.header.shiftLabel}</b>
                      </div>
                      <div className="col-sm-2">
                        <label htmlFor="mesinLabel" className="form-label">
                          Mesin :
                        </label>
                        <br />
                        <b>{this.props.header.mesinLabel}</b>
                      </div>
                      <div className="col-sm-3">
                        <label htmlFor="typeprodLabel" className="form-label">
                          Type Production :
                        </label>
                        <br />
                        <b>{this.props.header.typeprodLabel}</b>
                      </div>
                      <div className="col-sm-2">
                        <label
                          htmlFor="variantprodLabel"
                          className="form-label"
                        >
                          Variant :
                        </label>
                        <br />
                        <b>{this.props.header.variantprodLabel}</b>
                      </div>
                    </div>
                    <hr />
                    <div className="row mb-3">
                      <Label
                        name="time_check"
                        className="col-sm-3 col-form-label"
                      />
                      <div className="col-sm-9">
                        <Field name="time_check" className="form-control" />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <Label
                        name="no_lot_batch"
                        className=" col-sm-3 form-label"
                      />
                      {""}
                      <div className="col-sm-9">
                        <Field name="no_lot_batch" className="form-control" />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col-sm-12">
                        <table className="table table-bordered table-hovered table-striped table-hover rm">
                          <thead>
                            <tr>
                              <th></th>
                              <th>L</th>
                              <th>C</th>
                              <th>R</th>
                            </tr>
                          </thead>
                          <tbody>
                            {fields.oil_left ||
                            fields.oil_center ||
                            fields.oil_right ? (
                              <tr>
                                <th>Weight After Corrective Action (gr)</th>
                                <td>
                                  <Field
                                    name="oil_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="oil_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="oil_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            <tr>
                              <th>Weight After Seasoning (gr)</th>
                              <td>
                                <Field
                                  name="season_left"
                                  className="form-control"
                                />
                              </td>
                              <td>
                                <Field
                                  name="season_center"
                                  className="form-control"
                                />
                              </td>

                              <td>
                                <Field
                                  name="season_right"
                                  className="form-control"
                                />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div className="row mb-3">
                      <div className="col-sm-4">
                        <Label name="physical" className="form-label" />
                        <br />
                        <Field name="physical" className="form-check-input" />
                      </div>
                      <div className="col-sm-4">
                        <Label name="even_oil" className="form-label" />
                        <br />
                        <Field name="even_oil" className="form-check-input" />
                      </div>
                      <div className="col-sm-4">
                        <Label name="even_season" className="form-label" />
                        <br />
                        <Field
                          name="even_season"
                          className="form-check-input"
                        />
                      </div>
                      <br />
                      <br />
                      <br />
                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <table className="table table-bordered table-hovered table-striped table-hover seasoning">
                            <thead>
                              <tr>
                                <th colSpan={12} align="center">
                                  Organoleptik
                                </th>
                              </tr>
                              <tr>
                                <th colSpan={6} align="center">
                                  Visual / Kontaminasi (pcs)
                                </th>
                                <th rowSpan={"2"} className="title vertical">
                                  <span>Color (Warna)</span>
                                </th>
                                <th rowSpan={"2"} className="title vertical">
                                  <span>Taste (Rasa)</span>
                                </th>
                                <th rowSpan={"2"} className="title vertical">
                                  <span>Smell (Bau)</span>
                                </th>
                              </tr>

                              <tr>
                                <th align="center">Logam</th>
                                <th align="center">Plastik</th>
                                <th align="center">Rambut</th>
                                <th align="center">Oil</th>
                                <th align="center">Benang</th>
                                <th align="center">Other</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td align="center">
                                  <Field
                                    name="visual_logam"
                                    className="form-control"
                                  />
                                </td>
                                <td align="center">
                                  <Field
                                    name="visual_plastik"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="visual_rambut"
                                    className="form-control"
                                  />
                                </td>
                                <td align="center">
                                  <Field
                                    name="visual_oil"
                                    className="form-control"
                                  />
                                </td>
                                <td align="center">
                                  <Field
                                    name="visual_benang"
                                    className="form-control"
                                  />
                                </td>
                                <td align="center">
                                  <Field
                                    name="visual_other"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="warna"
                                    className="form-check-input"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="rasa"
                                    className="form-check-input"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="bau"
                                    className="form-check-input"
                                  />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row mb-5">
                        <div className="col-sm-2">
                          <Label name="status" className="form-label" />
                          <br />

                          <Field name="status" className="form-check-input" />
                        </div>

                        <div className="col-sm-5">
                          <Label
                            name="corrective_action"
                            className="form-label"
                          />
                          <br />
                          <Field
                            name="corrective_action"
                            className="form-control"
                          />
                        </div>
                        <div className="col-sm-5">
                          <Label name="note" className="form-label" />
                          <br />
                          <Field name="note" className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="modal-footer">
                    {this.props.onClose ? (
                      <button
                        type="button"
                        className="btn btn-outline-secondary"
                        data-bs-dismiss="modal"
                        onClick={this.props.onClose}
                        disabled={this.state.loading}
                      >
                        Close
                      </button>
                    ) : undefined}
                    {this.props.data !== undefined ? (
                      this.props.data === null ? (
                        <>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}
                          >
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}
                          >
                            {this.state.loading === "insert" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      ) : (
                        <>
                          <button
                            className="btn btn-danger"
                            type="button"
                            disabled={this.state.loading}
                            onClick={(e) => {
                              e.preventDefault();
                              this.DeleteRecord();
                            }}
                          >
                            {this.state.loading === "delete" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Deleting...</span>
                              </>
                            ) : (
                              "Delete"
                            )}
                          </button>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}
                          >
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}
                          >
                            {this.state.loading === "edit" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      )
                    ) : undefined}
                  </div>
                </Form>
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default prForm;
