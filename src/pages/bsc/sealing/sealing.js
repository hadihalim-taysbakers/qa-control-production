import React from "react";
import axios from "axios";
import config from "../../../config";
import Form, { FormContext } from "../../../components/Form";
import Field from "../../../components/Field";
import Label from "../../../components/Label";
import Select from "react-select";
import Main from "../../../layout/main";
import Approval from "../../../Approval";
import { formatTime } from "../../../utils/formatter";
import GraphSealing from "./GraphSealing";
import SealingForm from "./sealingForm";

class Sealing extends React.Component {
  refForm = React.createRef();
  fields = {
    min_gw_per_pcs: {
      name: "min_gw_per_pcs",
      id: "min_gw_per_pcs",
      label: "Min 10 pcs",
      attributeName: "Berat minimum(pcs)",
      type: "label",
      min: 0,
      max: 2000,
      step: 0.1,
      rules: ["required", "numeric", "max:2000", "min:0"],
    },
    max_gw_per_pcs: {
      name: "max_gw_per_pcs",
      id: "max_gw_per_pcs",
      label: "Max 10 pcs",
      attributeName: "Berat maximum(pcs)",
      type: "label",
      min: 0,
      max: 2000,
      step: 0.1,
      rules: ["required", "numeric", "max:2000", "min:0"],
    },
    min_gw_per_box: {
      name: "min_gw_per_box",
      id: "min_gw_per_box",
      label: "Min",
      attributeName: "Berat minimum(box)",
      type: "label",
      min: 0,
      max: 2000,
      step: 0.1,
      rules: ["required", "numeric", "max:2000", "min:0"],
    },
    max_gw_per_box: {
      name: "max_gw_per_box",
      id: "max_gw_per_box",
      label: "Max",
      attributeName: "Berat maximum(box)",
      type: "label",
      min: 0,
      max: 2000,
      step: 0.1,
      rules: ["required", "numeric", "max:2000", "min:0"],
    },
    min_gw_per_bag: {
      name: "min_gw_per_bag",
      id: "min_gw_per_bag",
      label: "Min",
      attributeName: "Berat minimum(bag)",
      type: "label",
      min: 0,
      max: 2000,
      step: 0.1,
      rules: ["required", "numeric", "max:2000", "min:0"],
    },
    max_gw_per_bag: {
      name: "max_gw_per_bag",
      id: "max_gw_per_bag",
      label: "Max",
      attributeName: "Berat maximum(bag)",
      type: "label",
      min: 0,
      max: 2000,
      step: 0.1,
      rules: ["required", "numeric", "max:2000", "min:0"],
    },
    min_1pcs: {
      name: "min_1pcs",
      id: "min_1pcs",
      label: "Min 1 pcs",
    },
    max_1pcs: {
      name: "max_1pcs",
      id: "max_1pcs",
      label: "Max 1 pcs",
    },
    average_pcs: {
      name: "average_pcs",
      id: "average_pcs",
      label: "Average",
    },
  };
  state = {
    formData: undefined,
    exists: [],
    approveCreator: undefined,
    approveVerifier: undefined,
    approveKnower: undefined,
    creator_name: undefined,
    creator_title: undefined,
    creator_sign: undefined,
    created: undefined,
    verifier_name: undefined,
    verifier_title: undefined,
    verifier_sign: undefined,
    verified: undefined,
    knower_name: undefined,
    knower_title: undefined,
    knower_sign: undefined,
    knowed: undefined,
    edit: undefined,
    loading: false,
    headerData: undefined,
    graph: undefined,
    name: "",
    email: "",
    no_doc: "FRM.QCRD.CRP.02.02; Rev.05",
    tanggal: "",
    operator: "",
    typeprod: "",
    mesin: "",
    shiftr: "",
    datas: [],
    searchString: [],
    mesinOptions: [],
    shiftOptions: [],
    productOptions: [],
    data: [],
  };
  getFields() {
    const productType = this.getProductType();
    const fields = { ...this.fields };
    if (
      !(
        productType &&
        parseFloat(productType.min_gw_pcs_sealing) > 0 &&
        parseFloat(productType.max_gw_pcs_sealing) > 0
      )
    ) {
      delete fields.min_gw_per_pcs;
      delete fields.max_gw_per_pcs;
    }

    if (
      !(
        productType &&
        parseFloat(productType.min_gw_box_sealing) > 0 &&
        parseFloat(productType.min_gw_box_sealing) > 0
      )
    ) {
      delete fields.min_gw_per_box;
      delete fields.max_gw_per_box;
    }
    if (
      !(
        productType &&
        parseFloat(productType.min_gw_bag_sealing) > 0 &&
        parseFloat(productType.max_gw_bag_sealing) > 0
      )
    ) {
      delete fields.min_gw_per_bag;
      delete fields.max_gw_per_bag;
    }
    if (
      !(
        productType &&
        parseFloat(productType.min_1pcs_sealing) > 0 &&
        parseFloat(productType.max_1pcs_sealing) > 0
      )
    ) {
      delete fields.min_1pcs;
      delete fields.max_1pcs;
    }

    return fields;
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      (prevState.tanggal !== this.state.tanggal ||
        prevState.shiftr !== this.state.shiftr ||
        prevState.typeprod !== this.state.typeprod ||
        prevState.mesin !== this.state.mesin) &&
      this.state.tanggal &&
      this.state.shiftr &&
      this.state.typeprod &&
      this.state.mesin
    ) {
      const url = config.api + config.path.sealing;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            type_produksi: this.state.typeprod,
            mesin: this.state.mesin,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          const headerData = {};
          if (data.header) {
            headerData.min_gw_per_pcs = data.header.min_gw_per_pcs;
            headerData.max_gw_per_pcs = data.header.max_gw_per_pcs;
            headerData.min_gw_per_box = data.header.min_gw_per_box;
            headerData.max_gw_per_box = data.header.max_gw_per_box;
            headerData.min_gw_per_bag = data.header.min_gw_per_bag;
            headerData.max_gw_per_bag = data.header.max_gw_per_bag;
            headerData.min_1pcs = data.header.min_1pcs;
            headerData.max_1pcs = data.header.max_1pcs;
          } else {
            for (let i = 0; i < this.productTypes.length; i++) {
              if (
                parseInt(this.productTypes[i].id) ===
                parseInt(this.state.typeprod)
              ) {
                headerData.min_gw_per_pcs =
                  this.productTypes[i].min_gw_pcs_sealing;
                headerData.max_gw_per_pcs =
                  this.productTypes[i].max_gw_pcs_sealing;
                headerData.min_gw_per_box =
                  this.productTypes[i].min_gw_box_sealing;
                headerData.max_gw_per_box =
                  this.productTypes[i].max_gw_box_sealing;
                headerData.min_gw_per_bag =
                  this.productTypes[i].min_gw_bag_sealing;
                headerData.max_gw_per_bag =
                  this.productTypes[i].max_gw_bag_sealing;
                headerData.min_1pcs = this.productTypes[i].min_1pcs_sealing;
                headerData.max_1pcs = this.productTypes[i].max_1pcs_sealing;
              }
            }
          }
          this.setState({
            creator_name: data.header ? data.header.creator_name : undefined,
            creator_title: data.header ? data.header.creator_title : undefined,
            creator_sign: data.header ? data.header.creator_sign : undefined,
            created: data.header ? data.header.created : undefined,
            verifier_name: data.header ? data.header.verifier_name : undefined,
            verifier_title: data.header
              ? data.header.verifier_title
              : undefined,
            verifier_sign: data.header ? data.header.verifier_sign : undefined,
            verified: data.header ? data.header.verified : undefined,
            knower_name: data.header ? data.header.knower_name : undefined,
            knower_title: data.header ? data.header.knower_title : undefined,
            knower_sign: data.header ? data.header.knower_sign : undefined,
            knowed: data.header ? data.header.knowed : undefined,
            loading: false,
            datas: data.datas,
            operator: data.header ? data.header.operator : "",
            headerData: headerData,
          });
        });
    } else if (
      prevState.typeprod !== this.state.typeprod &&
      this.state.typeprod
    ) {
      for (let i = 0; i < this.productTypes.length; i++) {
        if (
          parseInt(this.productTypes[i].id) === parseInt(this.state.typeprod)
        ) {
          const min_gw_per_pcs = this.productTypes[i].min_gw_pcs_sealing;
          const max_gw_per_pcs = this.productTypes[i].max_gw_pcs_sealing;
          const min_gw_per_box = this.productTypes[i].min_gw_box_sealing;
          const max_gw_per_box = this.productTypes[i].max_gw_box_sealing;
          const min_gw_per_bag = this.productTypes[i].min_gw_bag_sealing;
          const max_gw_per_bag = this.productTypes[i].max_gw_bag_sealing;
          const min_1pcs = this.productTypes[i].min_1pcs_sealing;
          const max_1pcs = this.productTypes[i].max_1pcs_sealing;

          this.setState({
            min_gw_per_pcs: min_gw_per_pcs,
            max_gw_per_pcs: max_gw_per_pcs,
            min_gw_per_box: min_gw_per_box,
            max_gw_per_box: max_gw_per_box,
            min_gw_per_bag: min_gw_per_bag,
            max_gw_per_bag: max_gw_per_bag,
            min_1pcs: min_1pcs,
            max_1pcs: max_1pcs,
          });
        }
      }
    }

    if (prevState.tanggal !== this.state.tanggal && this.state.tanggal) {
      const url = config.api + config.path.sealing;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          this.setState({
            exists: data.datas,
          });
        });
    }
  }

  submitHeaderChange(datas) {
    if (
      this.state.tanggal &&
      this.state.shiftr &&
      this.state.typeprod &&
      this.state.mesin
    ) {
      this.setState({ loading: true }, () => {
        const formData = new FormData();
        for (const key in datas) {
          formData.append(key, datas[key]);
        }
        formData.append("no_doc", this.state.no_doc);
        axios
          .patch(config.api + config.path.sealing, formData, {
            "Content-Type": "application/x-www-form-urlencoded",
            params: {
              tanggal: this.state.tanggal,
              shiftr: this.state.shiftr,
              mesin: this.state.mesin,
              type_produksi: this.state.typeprod,
            },
          })
          .then((res) => {
            const headerData = { ...this.state.headerData };
            for (const key in datas) {
              headerData[key] = datas[key];
            }
            this.setState({
              loading: false,
              headerData: headerData,
            });
          })
          .catch(() => {
            this.setState({
              loading: false,
            });
            alert("Gagal insert data");
          });
      });
    }
  }
  componentDidMount() {
    const yourDate = new Date();
    this.setState({ tanggal: yourDate.toISOString().split("T")[0] });
  }

  async getOptions() {
    const resShift = await axios.get(config.api + config.path.master_shift);
    const dataShift = resShift.data;
    const shiftOptions = dataShift.map((d) => ({
      value: d.id,
      label: d.judul,
    }));
    this.setState({ shiftOptions: shiftOptions });
    const resMesin = await axios.get(config.api + config.path.master_mesin, {
      params: { posisi: config.page.sealing.posisi },
    });
    const dataMesin = resMesin.data;
    const mesinOptions = dataMesin.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    this.setState({ mesinOptions: mesinOptions });
    const resProduct = await axios.get(
      config.api + config.path.master_product2
    );
    const dataProduct = resProduct.data;
    const productOptions = dataProduct.map((d) => ({
      value: d.id,
      label: d.product_name,
    }));
    this.productTypes = [...dataProduct];
    this.setState({ productOptions: productOptions });
  }

  componentWillMount() {
    this.getOptions();
  }

  setEdit(ind) {
    this.setState({
      formData: this.state.datas[ind],
    });
  }

  submitCreatorApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("creator_name", data.name);
      formData.append("creator_title", data.title);
      formData.append("creator_sign", data.sign);
      axios
        .patch(config.api + config.path.sealing, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveCreator: undefined,
            creator_name: res.data.header.creator_name,
            creator_title: res.data.header.creator_title,
            creator_sign: res.data.header.creator_sign,
            created: res.data.header.created,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveCreator: undefined,
            creator_name: undefined,
            creator_title: undefined,
            creator_sign: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitVerifierApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("verifier_name", data.name);
      formData.append("verifier_title", data.title);
      formData.append("verifier_sign", data.sign);
      axios
        .patch(config.api + config.path.sealing, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: res.data.header.verifier_name,
            verifier_title: res.data.header.verifier_title,
            verifier_sign: res.data.header.verifier_sign,
            verified: res.data.header.verified,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: undefined,
            verifier_title: undefined,
            verifier_sign: undefined,
            verified: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitKnowerApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("knower_name", data.name);
      formData.append("knower_title", data.title);
      formData.append("knower_sign", data.sign);
      axios
        .patch(config.api + config.path.sealing, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveKnower: undefined,
            knower_name: res.data.header.knower_name,
            knower_title: res.data.header.knower_title,
            knower_sign: res.data.header.knower_sign,
            knowed: res.data.header.knowed,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveKnower: undefined,
            knower_name: undefined,
            knower_title: undefined,
            knower_sign: undefined,
            knowed: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }

  getProductOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.typeprod) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
            parseInt(exists[j]["type_produksi"]) ===
              parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getProductOptions() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
          parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
          parseInt(exists[j]["type_produksi"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getMesinOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.mesin) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getMesinOptions() {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
          parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getShiftOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.shiftr) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getShiftName(id) {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }

  getMesinName(id) {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getProductName(id) {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return;
  }

  getShiftOptions() {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }

  getGraphData(fields, min, max, title) {
    const labels = [];
    const dataMin = [];
    const dataMax = [];
    const datas = {};

    for (const key in fields) {
      datas[key] = [];
    }
    for (var i = 0; i < this.state.datas.length; i++) {
      for (const key in fields) {
        if (this.state.datas[i][key] > 0) {
          datas[key].push(this.state.datas[i][key]);
        } else {
          datas[key].push(null);
        }
      }

      labels.push(this.state.datas[i].time_check);
      dataMin.push(min);
      dataMax.push(max);
    }
    const datasets = [];
    for (const key in fields) {
      datasets.push({
        label: fields[key].label,
        data: datas[key],
        borderColor: fields[key].color,
      });
    }
    return {
      title: title,
      data: {
        labels: labels,
        datasets: [
          ...datasets,
          {
            label: "Over",
            fill: "end",
            data: dataMax,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
          },

          {
            label: "Under",
            data: dataMin,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
            fill: "start",
          },
        ],
      },
    };
  }

  removeExistData(typeprod, mesin, shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (
        exists[i].type_produksi === typeprod &&
        exists[i].mesin === mesin &&
        exists[i].shift === shift
      ) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) - 1;
      }
    }
    if (isExist === false) {
      exists.push({
        type_produksi: typeprod,
        mesin: mesin,
        shift: shift,

        qty: "0",
      });
    }
    return exists;
  }
  addExistData(typeprod, mesin, shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (
        exists[i].type_produksi === typeprod &&
        exists[i].mesin === mesin &&
        exists[i].shift === shift
      ) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) + 1;
      }
    }
    if (isExist === false) {
      exists.push({
        type_produksi: typeprod,
        mesin: mesin,
        shift: shift,

        qty: "1",
      });
    }
    return exists;
  }
  getProductType() {
    if (this.productTypes) {
      for (let i = 0; i < this.productTypes.length; i++) {
        if (
          parseInt(this.productTypes[i].id) === parseInt(this.state.typeprod)
        ) {
          return this.productTypes[i];
        }
      }
    }
    return null;
  }
  render() {
    const productType = this.getProductType();
    const min_gw_per_pcs = productType
      ? parseFloat(productType.min_gw_pcs_sealing)
      : 0;
    const max_gw_per_pcs = productType
      ? parseFloat(productType.max_gw_pcs_sealing)
      : 0;
    const min_gw_per_box = productType
      ? parseFloat(productType.min_gw_box_sealing)
      : 0;
    const max_gw_per_box = productType
      ? parseFloat(productType.max_gw_box_sealing)
      : 0;
    const min_gw_per_bag = productType
      ? parseFloat(productType.min_gw_bag_sealing)
      : 0;
    const max_gw_per_bag = productType
      ? parseFloat(productType.max_gw_bag_sealing)
      : 0;
    const min_1pcs = productType ? parseFloat(productType.min_1pcs_sealing) : 0;
    const max_1pcs = productType ? parseFloat(productType.max_1pcs_sealing) : 0;

    const hd_min_pcs = this.state.headerData
      ? parseFloat(this.state.headerData.min_gw_per_pcs)
      : 0;
    const hd_max_pcs = this.state.headerData
      ? parseFloat(this.state.headerData.max_gw_per_pcs)
      : 0;
    const hd_min_box = this.state.headerData
      ? parseFloat(this.state.headerData.min_gw_per_box)
      : 0;
    const hd_max_box = this.state.headerData
      ? parseFloat(this.state.headerData.max_gw_per_box)
      : 0;
    const hd_min_bag = this.state.headerData
      ? parseFloat(this.state.headerData.min_gw_per_bag)
      : 0;
    const hd_max_bag = this.state.headerData
      ? parseFloat(this.state.headerData.max_gw_per_bag)
      : 0;
    const hd_min_1pcs = this.state.headerData
      ? parseFloat(this.state.headerData.min_1pcs)
      : 0;
    const hd_max_1pcs = this.state.headerData
      ? parseFloat(this.state.headerData.max_1pcs)
      : 0;

    let colspannum = 36;
    if (min_gw_per_pcs > 0 && max_gw_per_pcs > 0) colspannum = colspannum + 10;
    if (min_gw_per_box > 0 && max_gw_per_box > 0) colspannum = colspannum + 2;
    if (min_gw_per_bag > 0 && max_gw_per_bag > 0) colspannum = colspannum + 2;

    return (
      <>
        <Main>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm">
                <h4 className="page-header text-center">
                  PEMERIKSAAN HASIL PRODUKSI SETIAP MESIN SEALING
                </h4>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-3 form-group">
                <label htmlFor="tanggal">Date :</label>
                <input
                  disabled={this.state.loading}
                  required
                  type="date"
                  name="tanggal"
                  id="tanggal"
                  className="form-control"
                  value={this.state.tanggal}
                  onChange={(e) => this.setState({ tanggal: e.target.value })}
                />
              </div>
              <div className="col-sm-3 form-group">
                <label htmlFor="shiftr">Shift :</label>
                <Select
                  isDisabled={this.state.loading}
                  name="shiftr"
                  id="shiftr"
                  options={this.getShiftOptions()}
                  value={this.getShiftOptionValue()}
                  onChange={(e) => {
                    this.setState({ shiftr: e.value });
                  }}
                />
              </div>
              <div className="col-sm-3 form-group">
                <label htmlFor="shiftr">Mesin :</label>
                <Select
                  isDisabled={this.state.loading}
                  name="mesin"
                  id="mesin"
                  options={this.getMesinOptions()}
                  value={this.getMesinOptionValue()}
                  onChange={(e) => {
                    this.setState({ mesin: e.value });
                  }}
                />
              </div>
              <div className="col-sm-3 form-group">
                <label htmlFor="typeprod">Type Production</label>
                <Select
                  isDisabled={this.state.loading}
                  name="typeprod"
                  id="typeprod"
                  options={this.getProductOptions()}
                  value={this.getProductOptionValue()}
                  onChange={(e) => {
                    this.setState({ typeprod: e.value });
                  }}
                />
              </div>
            </div>
            {this.state.tanggal &&
            this.state.typeprod &&
            this.state.mesin &&
            this.state.shiftr ? (
              <>
                <div className="row">
                  <div className="col-sm-6">
                    <font size="2">
                      Pengambilan sample setiap 30 Menit, sebanyak 10 pcs per
                      sisi
                    </font>
                  </div>

                  <div className="col-sm-6 text-right">
                    <font size="2">No. Dok : {this.state.no_doc}</font>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12">
                    {this.fields ? (
                      <Form
                        fields={this.getFields()}
                        defaultValues={this.state.headerData}
                        onSubmit={(data) => {
                          this.submitHeaderChange(data);
                        }}>
                        <FormContext.Consumer>
                          {({ submit, validate, errors }) => (
                            <table className="table data table-bordered table-hovered table-striped table-hover sealing">
                              <thead>
                                <tr>
                                  <th rowSpan="4">Time Check</th>
                                  <th colSpan="4">Kualitas Sealing (pcs)</th>
                                  <th colSpan="3">Potongan / Cutting (pcs)</th>
                                  <th rowSpan="4" className="vertical">
                                    <span>
                                      Kesesuaian Packaging (&radic;/x)
                                    </span>
                                  </th>
                                  <th colSpan="12">
                                    Gross Weight (Berat dengan kemasan)
                                  </th>
                                  <th colSpan="4">Organoleptic (&radic;/x)</th>
                                  <th colSpan="5">Print Coding</th>
                                  <th rowSpan="4">Nomor Barcode</th>
                                  <th rowSpan="4" className="vertical">
                                    <span>Test Sensor Kosong (&radic;/x)</span>
                                  </th>
                                  <th colSpan="2">Vakum</th>
                                  <th rowSpan="4">Customer&PO/SPK</th>
                                  <th rowSpan="4">Keterangan</th>
                                </tr>
                                <tr>
                                  <th rowSpan="3" className="vertical">
                                    <span>Bolong</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Melipat</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Pecah</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Kotor / Lecet</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Printing Terpotong</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Pot. Tdk Simetris/ Acak</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Serabut</span>
                                  </th>

                                  {min_gw_per_pcs > 0 && max_gw_per_pcs > 0 ? (
                                    <th colSpan="10">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_gw_per_pcs" />
                                          <Field
                                            name="min_gw_per_pcs"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_gw_per_pcs" />
                                          <Field
                                            name="max_gw_per_pcs"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  <th rowSpan="3" className="vertical">
                                    <span>Per 10 pcs (gr)</span>
                                  </th>
                                  {min_gw_per_box > 0 && max_gw_per_box > 0 ? (
                                    <th>
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_gw_per_box" />
                                          <Field
                                            name="min_gw_per_box"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_gw_per_box" />
                                          <Field
                                            name="max_gw_per_box"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}

                                  {min_gw_per_bag > 0 && max_gw_per_bag > 0 ? (
                                    <th>
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_gw_per_bag" />
                                          <Field
                                            name="min_gw_per_bag"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_gw_per_bag" />
                                          <Field
                                            name="max_gw_per_bag"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}

                                  <th rowSpan="3" className="vertical">
                                    <span>Warna </span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Rasa </span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Bau </span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Jenis Temuan Kontaminasi</span>
                                  </th>
                                  <th rowSpan="3">Prod / MFG</th>
                                  <th rowSpan="3">Exp</th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Code</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Jam</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Mesin</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Ke-1 (&radic;/x)</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Ke-2 (&radic;/x)</span>
                                  </th>
                                </tr>
                                <tr>
                                  {min_1pcs > 0 && max_1pcs > 0 ? (
                                    <th colSpan={10}>
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                average_pcs: {
                                                  label: "Rata-Rata",
                                                  color: "#28a745",
                                                },
                                                tertinggi: {
                                                  label: "Data Tertinggi",
                                                  color: "#007bff",
                                                },
                                                terendah: {
                                                  label: "Data Terendah",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData.min_1pcs,
                                              this.state.headerData.max_1pcs,
                                              "Gross Weight Graph "
                                            ),
                                          });
                                        }}>
                                        &#128201; Berat per pcs (gr)
                                        <br />
                                        Check Ke
                                      </button>
                                    </th>
                                  ) : undefined}

                                  {min_gw_per_box > 0 && max_gw_per_box > 0 ? (
                                    <th rowSpan={2}>
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                berat_per_innerbox: {
                                                  label: "Berat per Innerbox",
                                                  color: "#28a745",
                                                },
                                              },
                                              this.state.headerData
                                                .min_gw_per_box,
                                              this.state.headerData
                                                .max_gw_per_box,
                                              "Gross Weight Graph (Innerbox)"
                                            ),
                                          });
                                        }}>
                                        &#128201; Berat per
                                        <br />
                                        Innerbox(gr)
                                      </button>
                                    </th>
                                  ) : undefined}
                                  {min_gw_per_bag > 0 && max_gw_per_bag > 0 ? (
                                    <th rowSpan={2}>
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                berat_per_bag: {
                                                  label: "Berat Per Bag",
                                                  color: "#28a745",
                                                },
                                              },
                                              this.state.headerData
                                                .min_gw_per_bag,
                                              this.state.headerData
                                                .max_gw_per_bag,
                                              "Gross Weight Graph (box)"
                                            ),
                                          });
                                        }}>
                                        &#128201; Berat Per Bag <br /> (gr)
                                      </button>
                                    </th>
                                  ) : undefined}
                                </tr>
                                <tr>
                                  <th>1</th>
                                  <th>2</th>
                                  <th>3</th>
                                  <th>4</th>
                                  <th>5</th>
                                  <th>6</th>
                                  <th>7</th>
                                  <th>8</th>
                                  <th>9</th>
                                  <th>10</th>
                                </tr>
                              </thead>

                              <tbody>
                                {this.state.datas.map((sealing, index) => {
                                  const cek_berat1 = parseFloat(
                                    sealing.cek_berat1
                                  );
                                  const cek_berat2 = parseFloat(
                                    sealing.cek_berat2
                                  );
                                  const cek_berat3 = parseFloat(
                                    sealing.cek_berat3
                                  );
                                  const cek_berat4 = parseFloat(
                                    sealing.cek_berat4
                                  );
                                  const cek_berat5 = parseFloat(
                                    sealing.cek_berat5
                                  );
                                  const cek_berat6 = parseFloat(
                                    sealing.cek_berat6
                                  );
                                  const cek_berat7 = parseFloat(
                                    sealing.cek_berat7
                                  );
                                  const cek_berat8 = parseFloat(
                                    sealing.cek_berat8
                                  );
                                  const cek_berat9 = parseFloat(
                                    sealing.cek_berat9
                                  );
                                  const cek_berat10 = parseFloat(
                                    sealing.cek_berat10
                                  );
                                  const berat_per_innerbox = parseFloat(
                                    sealing.berat_per_innerbox
                                  );
                                  const berat_per_bag = parseFloat(
                                    sealing.berat_per_bag
                                  );
                                  const cek_berat_all =
                                    cek_berat1 +
                                    cek_berat2 +
                                    cek_berat3 +
                                    cek_berat4 +
                                    cek_berat5 +
                                    cek_berat6 +
                                    cek_berat7 +
                                    cek_berat8 +
                                    cek_berat9 +
                                    cek_berat10;
                                  parseFloat(sealing.cek_berat_all);

                                  return (
                                    <tr key={index}>
                                      <td className="action">
                                        {!this.state.verifier_name ? (
                                          <>
                                            <button
                                              type="button"
                                              className="btn btn-outline-info btn-block btn-sm"
                                              onClick={() => {
                                                this.setEdit(index);
                                              }}>
                                              &#128269;
                                            </button>

                                            {formatTime(sealing.time_check)}
                                          </>
                                        ) : (
                                          <>{formatTime(sealing.time_check)} </>
                                        )}
                                      </td>

                                      <td align="center">
                                        {sealing.sealing_bolong}
                                      </td>
                                      <td align="center">
                                        {sealing.sealing_melipat}
                                      </td>
                                      <td align="center">
                                        {sealing.sealing_pecah}
                                      </td>
                                      <td align="center">
                                        {sealing.sealing_kotor}
                                      </td>
                                      <td align="center">
                                        {sealing.printing_terpotong}
                                      </td>
                                      <td align="center">
                                        {sealing.tidak_simetris}
                                      </td>
                                      <td align="center">{sealing.serabut}</td>
                                      <td align="center">
                                        {sealing.kesesuaian_packaging ===
                                        "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat1 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat1 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat1.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat2 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat2 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat2.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat3 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat3 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat3.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat4 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat4 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat4.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat5 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat5 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat5.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat6 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat6 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat6.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat7 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat7 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat7.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat8 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat8 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat8.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat9 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat9 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat9.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_1pcs > 0 && max_1pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat10 < hd_min_1pcs
                                                  ? "red"
                                                  : cek_berat10 > hd_max_1pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat10.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}

                                      {min_gw_per_pcs > 0 &&
                                      max_gw_per_pcs > 0 ? (
                                        <>
                                          <td>
                                            <span
                                              className={
                                                cek_berat_all < hd_min_pcs
                                                  ? "red"
                                                  : cek_berat_all > hd_max_pcs
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {cek_berat_all.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_gw_per_box > 0 &&
                                      max_gw_per_box > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                berat_per_innerbox < hd_min_box
                                                  ? "red"
                                                  : berat_per_innerbox >
                                                    hd_max_box
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {berat_per_innerbox.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_gw_per_bag > 0 &&
                                      max_gw_per_bag > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                berat_per_bag < hd_min_bag
                                                  ? "red"
                                                  : berat_per_bag > hd_max_bag
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {berat_per_bag.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      <td align="center">
                                        {sealing.organ_warna === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {sealing.organ_rasa === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {sealing.organ_bau === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {sealing.organ_kontaminasi}
                                      </td>
                                      <td>{sealing.prod_mfg}</td>
                                      <td>{sealing.coding_exp}</td>
                                      <td>{sealing.print_code}</td>
                                      <td>{sealing.print_time}</td>
                                      <td>{sealing.print_mesin}</td>
                                      <td>{sealing.nomor_barcode}</td>
                                      <td align="center">
                                        {sealing.test_sensor === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {sealing.vakum1 === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {sealing.vakum2 === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td>{sealing.customer}</td>
                                      <td>{sealing.keterangan}</td>
                                    </tr>
                                  );
                                })}
                              </tbody>
                              {!this.state.verifier_name ? (
                                <tfoot>
                                  <tr>
                                    <td colSpan={colspannum} align="center">
                                      <button
                                        type="button"
                                        className="btn btn-primary"
                                        onClick={(e) => {
                                          if (validate()) {
                                            this.setState({
                                              formData: null,
                                            });
                                          } else {
                                            alert(
                                              "Silahkan perbaiki data minimum dan maximum"
                                            );
                                          }
                                        }}
                                        value="Add">
                                        &#10010; Add
                                      </button>
                                    </td>
                                  </tr>
                                </tfoot>
                              ) : undefined}
                            </table>
                          )}
                        </FormContext.Consumer>
                      </Form>
                    ) : undefined}
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6 xs-12"></div>
                  <div className="col-md-6 xs-12">
                    <table className="sign" cellPadding={3}>
                      <tbody>
                        <tr>
                          <th width={"25%"}></th>
                          <th width={"25%"}> Dibuat Oleh,</th>
                          <th width={"25%"}> Diverifikasi Oleh,</th>
                          <th width={"25%"}>Mengetahui Oleh, </th>
                        </tr>
                        <tr>
                          <th>Tanda Tangan</th>
                          {this.state.creator_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.creator_sign}
                                alt="creator sign"
                                width={"100%"}
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 || this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveCreator: {
                                      name: "",
                                      title: "Quality Control",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.verifier_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.verifier_sign}
                                width={"100%"}
                                alt="verifier sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveVerifier: {
                                      name: "",
                                      title: "Supervisor QC",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.knower_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.knower_sign}
                                width={"100%"}
                                alt="knower sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  !this.state.verifier_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveKnower: {
                                      name: "",
                                      title: "Manager QA",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                        </tr>
                        <tr>
                          <th>Nama</th>
                          {this.state.creator_name ? (
                            <td align="center" valign="middle">
                              {this.state.creator_name}
                            </td>
                          ) : undefined}
                          {this.state.verifier_name ? (
                            <td align="center" valign="middle">
                              {this.state.verifier_name}
                            </td>
                          ) : undefined}
                          {this.state.knower_name ? (
                            <td align="center" valign="middle">
                              {this.state.knower_name}
                            </td>
                          ) : undefined}
                        </tr>
                        <tr>
                          <th>Jabatan</th>
                          {this.state.creator_title ? (
                            <td align="center" valign="middle">
                              {this.state.creator_title}
                            </td>
                          ) : undefined}
                          {this.state.verifier_title ? (
                            <td align="center" valign="middle">
                              {this.state.verifier_title}
                            </td>
                          ) : undefined}
                          {this.state.knower_title ? (
                            <td align="center" valign="middle">
                              {this.state.knower_title}
                            </td>
                          ) : undefined}
                        </tr>
                        <tr>
                          <th>Tanggal</th>
                          {this.state.created ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.created).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.verified ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.verified).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.knowed ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.knowed).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </>
            ) : undefined}
          </div>
        </Main>

        <Approval
          loading={this.state.loading}
          title="Dibuat oleh,"
          show={this.state.approveCreator ? true : false}
          data={this.state.approveCreator}
          onSubmit={(data) => {
            this.submitCreatorApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveCreator: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Diverifikasi oleh,"
          show={this.state.approveVerifier ? true : false}
          data={this.state.approveVerifier}
          onSubmit={(data) => {
            this.submitVerifierApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveVerifier: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Mengetahui oleh,"
          show={this.state.approveKnower ? true : false}
          data={this.state.approveKnower}
          onSubmit={(data) => {
            this.submitKnowerApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveKnower: undefined,
            });
          }}
        />
        <GraphSealing
          title="GRAPH PEMERIKSAAN HASIL PRODUKSI SETIAP MESIN SEALING"
          tanggal={this.state.tanggal}
          shift={this.getShiftName(this.state.shiftr)}
          mesin={this.getMesinName(this.state.mesin)}
          product={this.getProductName(this.state.typeprod)}
          show={this.state.graph ? true : false}
          data={this.state.graph ? this.state.graph.data : undefined}
          subtitle={this.state.graph ? this.state.graph.title : undefined}
          onClose={() => {
            this.setState({
              graph: undefined,
            });
          }}
        />
        <SealingForm
          header={{
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            shiftLabel: this.getShiftName(this.state.shiftr),
            typeprod: this.state.typeprod,
            typeprodLabel: this.getProductName(this.state.typeprod),
            mesin: this.state.mesin,
            mesinLabel: this.getMesinName(this.state.mesin),

            min_gw_per_pcs: min_gw_per_pcs,
            max_gw_per_pcs: max_gw_per_pcs,
            min_gw_per_box: min_gw_per_box,
            max_gw_per_box: max_gw_per_box,
            min_gw_per_bag: min_gw_per_bag,
            max_gw_per_bag: max_gw_per_bag,
            min_1pcs: min_1pcs,
            max_1pcs: max_1pcs,
          }}
          data={this.state.formData}
          show={this.state.formData !== undefined}
          onAfterDelete={(id_sealing_detail) => {
            const currDatas = [...this.state.datas];
            const newDatas = [];
            for (let i = 0; i < currDatas.length; i++) {
              if (
                parseInt(currDatas[i].id_sealing_detail) !==
                parseInt(id_sealing_detail)
              ) {
                newDatas.push(currDatas[i]);
              }
            }
            this.setState({
              exists: this.removeExistData(
                this.state.typeprod,
                this.state.mesin,
                this.state.shiftr
              ),
              datas: newDatas,
              formData: undefined,
            });
          }}
          onAfterEdit={(data) => {
            const currDatas = [...this.state.datas];
            for (let i = 0; i < currDatas.length; i++) {
              if (currDatas[i].id_sealing_detail === data.id_sealing_detail) {
                currDatas[i] = data;
              }
            }
            this.setState({
              datas: currDatas,
              formData: undefined,
            });
          }}
          onAfterInsert={(datas) => {
            this.setState({
              exists: this.addExistData(
                this.state.typeprod,
                this.state.mesin,
                this.state.shiftr
              ),
              datas: datas,
              formData: undefined,
            });
          }}
          onClose={() => {
            this.setState({
              formData: undefined,
            });
          }}
        />
      </>
    );
  }
}
export default Sealing;
