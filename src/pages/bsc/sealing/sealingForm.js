import React from "react";
import moment from "moment";
import Label from "../../../components/Label";
import Field from "../../../components/Field";
import axios from "axios";
import config from "../../../config";
import "moment/locale/id";
import * as Validator from "validatorjs";
import Form from "../../../components/Form";

class SealingForm extends React.Component {
  constructor(props) {
    super(props);
    this.fields = {
      time_check: {
        name: "time_check",
        id: "time_check",
        label: "Waktu Pengecekan",
        attributeName: "Waktu Pengecekan",
        type: "time",
        rules: ["required"],
      },
      sealing_bolong: {
        name: "sealing_bolong",
        id: "sealing_bolong",
        label: "Sealing Bolong",
        attributeName: "Bolong",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      sealing_melipat: {
        name: "sealing_melipat",
        id: "sealing_melipat",
        label: "Sealing Melipat",
        attributeName: "Melipat",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      sealing_pecah: {
        name: "sealing_pecah",
        id: "sealing_pecah",
        label: "Sealing Pecah",
        attributeName: "Pecah",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      sealing_kotor: {
        name: "sealing_kotor",
        id: "sealing_kotor",
        label: "Kotor / Lecet",
        attributeName: "Kotor / Lecet",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      printing_terpotong: {
        name: "printing_terpotong",
        id: "printing_terpotong",
        label: "Printing Terpotong",
        attributeName: "Printing Terpotong",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      tidak_simetris: {
        name: "tidak_simetris",
        id: "tidak_simetris",
        label: "Tidak Simetris / Acak",
        attributeName: "Tidak Simetris / Acak",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      serabut: {
        name: "serabut",
        id: "serabut",
        label: "Serabut",
        attributeName: "Serabut",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      kesesuaian_packaging: {
        name: "kesesuaian_packaging",
        id: "kesesuaian_packaging",
        label: "Kesesuaian Packaging",
        attributeName: "Kesesuaian Packaging",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      cek_berat1: {
        name: "cek_berat1",
        id: "cek_berat1",
        label: "1",
        attributeName: "Berat pcs ke 1",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      cek_berat2: {
        name: "cek_berat2",
        id: "cek_berat2",
        label: "2",
        attributeName: "Berat pcs ke 2",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      cek_berat3: {
        name: "cek_berat3",
        id: "cek_berat3",
        label: "3",
        attributeName: "Berat pcs ke 3",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      cek_berat4: {
        name: "cek_berat4",
        id: "cek_berat4",
        label: "4",
        attributeName: "Berat pcs ke 4",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      cek_berat5: {
        name: "cek_berat5",
        id: "cek_berat5",
        label: "5",
        attributeName: "Berat pcs ke 5",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      cek_berat6: {
        name: "cek_berat6",
        id: "cek_berat6",
        label: "6",
        attributeName: "Berat pcs ke 6",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      cek_berat7: {
        name: "cek_berat7",
        id: "cek_berat7",
        label: "7",
        attributeName: "Berat pcs ke 7",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      cek_berat8: {
        name: "cek_berat8",
        id: "cek_berat8",
        label: "8",
        attributeName: "Berat pcs ke 8",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      cek_berat9: {
        name: "cek_berat9",
        id: "cek_berat9",
        label: "9",
        attributeName: "Berat pcs ke 9",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      cek_berat10: {
        name: "cek_berat10",
        id: "cek_berat10",
        label: "10",
        attributeName: "Berat pcs ke 10",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },

      berat_per_innerbox: {
        name: "berat_per_innerbox",
        id: "berat_per_innerbox",
        label: "Berat per Innerbox (gr)",
        attributeName: "Berat per Innerbox (gr)",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      berat_per_bag: {
        name: "berat_per_bag",
        id: "berat_per_bag",
        label: "Berat per Bag (gr)",
        attributeName: "Berat per Bag (gr)",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      organ_warna: {
        name: "organ_warna",
        id: "organ_warna",
        label: "Warna",
        attributeName: "Warna",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      organ_rasa: {
        name: "organ_rasa",
        id: "organ_rasa",
        label: "Rasa",
        attributeName: "Rasa",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      organ_bau: {
        name: "organ_bau",
        id: "organ_bau",
        label: "Bau",
        attributeName: "Bau",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      organ_kontaminasi: {
        name: "organ_kontaminasi",
        id: "organ_kontaminasi",
        label: "Jenis Temuan Kontaminasi",
        attributeName: "Jenis Temuan Kontaminasi",
        type: "text",
        rules: ["required"],
      },
      prod_mfg: {
        name: "prod_mfg",
        id: "prod_mfg",
        label: "Prod / MFG",
        attributeName: "Prod / MFG",
        type: "text",
        rules: ["required"],
      },
      coding_exp: {
        name: "coding_exp",
        id: "coding_exp",
        label: "Expired",
        attributeName: "Expired",
        type: "text",
        rules: ["required"],
      },
      print_code: {
        name: "print_code",
        id: "print_code",
        label: "Code",
        attributeName: "Code",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      print_time: {
        name: "print_time",
        id: "print_time",
        label: "Jam",
        attributeName: "Jam",
        type: "text",
        min: 0,
        rules: ["required"],
      },
      print_mesin: {
        name: "print_mesin",
        id: "print_mesin",
        label: "Mesin",
        attributeName: "Mesin",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      nomor_barcode: {
        name: "nomor_barcode",
        id: "nomor_barcode",
        label: "Nomor Barcode",

        attributeName: "Nomor Barcode",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      test_sensor: {
        name: "test_sensor",
        id: "test_sensor",
        label: "Test Sensor ",
        attributeName: "Test Sensor",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      vakum1: {
        name: "vakum1",
        id: "vakum1",
        label: "Vakum ke-1",
        attributeName: "Vakum ke-1",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      vakum2: {
        name: "vakum2",
        id: "vakum2",
        label: "Vakum ke-2",
        attributeName: "Vakum ke-2",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      customer: {
        name: "customer",
        id: "customer",
        label: "Customer & PO/SPK",
        attributeName: "Customer & PO/SPK",
        type: "text",
        values: "required",
      },
      keterangan: {
        name: "keterangan",
        id: "keterangan",
        label: "Keterangan",
        attributeName: "Keterangan",
        type: "text",
      },
    };
    this.state = {
      loading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  getFields() {
    const fields = { ...this.fields };
    const header = { ...this.props.header };
    if (header.min_gw_per_pcs <= 0 && header.max_gw_per_pcs <= 0) {
      delete fields.cek_berat1;
      delete fields.cek_berat2;
      delete fields.cek_berat3;
      delete fields.cek_berat4;
      delete fields.cek_berat5;
      delete fields.cek_berat6;
      delete fields.cek_berat7;
      delete fields.cek_berat8;
      delete fields.cek_berat9;
      delete fields.cek_berat10;
    }
    if (header.min_gw_per_box <= 0 && header.max_gw_per_box <= 0) {
      delete fields.berat_per_innerbox;
    }
    if (header.min_gw_per_bag <= 0 && header.max_gw_per_bag <= 0) {
      delete fields.berat_per_bag;
    }

    return fields;
  }
  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState(
      {
        datas: { ...this.state.datas, [name]: value },
      },
      () => {
        const valid = this.validateField(name, value);
        if (valid.valid === true) {
          this.setState({
            errors: { ...this.state.errors, [name]: valid.errors[name] },
          });
        } else {
          this.setState({
            errors: { ...this.state.errors, [name]: valid.errors[name] },
          });
        }
      }
    );
  }
  validateField(name, value) {
    const messages = Validator.getMessages("id");
    Validator.setMessages("id", messages);
    Validator.setAttributeNames(this.attributeNames);
    const validation = new Validator(
      { [name]: value },
      { [name]: this.rules[name] },
      messages
    );
    if (validation.passes()) {
      return { valid: true, errors: null };
    } else {
      const errors = validation.errors.all();
      return {
        valid: false,
        errors: errors,
      };
    }
  }
  InsertRecord(datas) {
    let formData = new FormData();
    for (const key in this.props.header) {
      formData.append(key, this.props.header[key]);
    }
    for (const key in datas) {
      formData.append(key, datas[key]);
    }
    this.setState({ loading: "insert" }, () => {
      axios({
        method: "post",
        responseType: "json",
        url: config.api + config.path.sealing,
        data: formData,
        params: {
          tanggal: this.props.header.tanggal,
          shiftr: this.props.header.shiftr,
          mesin: this.props.header.mesin,
          type_produksi: this.props.header.typeprod,
        },
        config: { headers: { "Content-Type": "multipart/form-data" } },
      })
        .then(
          function (response) {
            if (response.data.success) {
              this.setState({ loading: false }, () => {
                this.props.onAfterInsert(response.data.datas);
                alert("Berhasil untuk menyimpan data");
              });
            } else {
              this.setState({ loading: false }, () => {
                alert(
                  "Gagal untuk menyimpan data : " + response.data.error.messages
                );
              });
            }
          }.bind(this)
        )
        .catch((e) => {
          this.setState({ loading: false });
          alert("Gagal untuk menyimpan data");
        });
    });
  }
  DeleteRecord() {
    if (this.props.data && this.props.data.id_sealing_detail) {
      this.setState({ loading: "delete" }, () => {
        axios
          .delete(config.api + config.path.sealing, {
            params: { id_sealing_detail: this.props.data.id_sealing_detail },
          })
          .then((res) => {
            if (res.data.success) {
              this.setState({ loading: false }, () => {
                this.props.onAfterDelete(res.data.id_sealing_detail);
                alert("Berhasil untuk menghapus data");
              });
            } else {
              alert("gagal delete");
            }
          });
      });
    }
  }
  EditRecord(datas) {
    if (this.props.data) {
      let formData = new FormData();
      for (const key in this.props.header) {
        formData.append(key, this.props.header[key]);
      }
      for (const key in datas) {
        formData.append(key, datas[key]);
      }
      this.setState({ loading: "edit" }, () => {
        axios
          .patch(config.api + config.path.sealing, formData, {
            params: {
              id_sealing_detail: this.props.data.id_sealing_detail,
            },
          })
          .then(
            function (response) {
              this.setState({ loading: false }, () => {
                this.props.onAfterEdit(response.data.data);
                alert("Berhasil untuk merubah data");
              });
            }.bind(this)
          )
          .catch((e) => {
            this.setState({ loading: false });
            alert("Gagal untuk menyimpan data");
          });
      });
    }
  }
  onSubmit(datas) {
    if (this.props.data) {
      this.EditRecord(datas);
    } else {
      this.InsertRecord(datas);
    }
  }
  getProductType() {
    if (this.productTypes) {
      for (let i = 0; i < this.productTypes.length; i++) {
        if (
          parseInt(this.productTypes[i].id) === parseInt(this.state.typeprod)
        ) {
          return this.productTypes[i];
        }
      }
    }
    return null;
  }
  render() {
    const fields = this.getFields();
    return (
      <div
        className={this.props.show ? "modal fade show" : "modal"}
        style={this.props.show ? { display: "block" } : undefined}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            {this.props.data === undefined ? undefined : (
              <>
                <Form
                  fields={fields}
                  onSubmit={this.onSubmit}
                  loading={this.state.loading}
                  defaultValues={this.props.data ? this.props.data : {}}
                >
                  <div className="modal-header">
                    <h5 className="modal-title">
                      {this.props.data !== undefined
                        ? this.props.data &&
                          this.props.data.id_sealing_detail === null
                          ? "EDIT"
                          : "INSERT"
                        : ""}
                      PEMERIKSAAN HASIL PRODUKSI SETIAP MESIN SEALING
                    </h5>
                    {this.props.onClose ? (
                      <button
                        type="button"
                        disabled={this.state.loading}
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        onClick={this.props.onClose}
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    ) : undefined}
                  </div>
                  <div className="modal-body">
                    <div className="row mb-3">
                      <div className="col-sm-3">
                        <label htmlFor="tanggal" className="form-label">
                          Tanggal :
                        </label>
                        <br />
                        <b>
                          {moment(this.props.header.tanggal)
                            .locale("id")
                            .format("ddd, DD MMM YYYY")}
                        </b>
                      </div>
                      <div className="col-sm-3">
                        <label htmlFor="shift" className="form-label">
                          Shift :
                        </label>
                        <br />
                        <b>{this.props.header.shiftLabel}</b>
                      </div>
                      <div className="col-sm-3">
                        <label htmlFor="mesinLabel" className="form-label">
                          Mesin :
                        </label>
                        <br />
                        <b>{this.props.header.mesinLabel}</b>
                      </div>
                      <div className="col-sm-3">
                        <label htmlFor="typeprodLabel" className="form-label">
                          Type Production :
                        </label>
                        <br />
                        <b>{this.props.header.typeprodLabel}</b>
                      </div>

                      <hr />
                      <div className="row mb-3">
                        <Label
                          name="time_check"
                          className="col-sm-3 col-form-label"
                        />
                        <div className="col-sm-9">
                          <Field name="time_check" className="form-control" />
                        </div>
                      </div>
                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <fieldset>
                            <legend>Kualitas Sealing (pcs) </legend>
                            <table className="table table-bordered table-hovered table-striped table-hover sealing">
                              <thead>
                                <tr>
                                  <th className="text-center">
                                    Sealing Bolong{" "}
                                  </th>
                                  <th className="text-center">
                                    Sealing Melipat
                                  </th>
                                  <th className="text-center">Sealing Pecah</th>
                                  <th className="text-center">Kotor / Lecet</th>
                                </tr>
                              </thead>

                              <tbody>
                                <tr>
                                  <td align="center">
                                    <Field
                                      name="sealing_bolong"
                                      className="form-control"
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="sealing_melipat"
                                      className="form-control"
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="sealing_pecah"
                                      className="form-control"
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="sealing_kotor"
                                      className="form-control"
                                    />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </fieldset>
                        </div>
                      </div>
                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <fieldset>
                            <legend>Potongan / Cutting</legend>
                            <table className="table table-bordered table-hovered table-striped table-hover sealing">
                              <thead>
                                <tr>
                                  <th className="text-center">
                                    Printing Terpotong
                                  </th>
                                  <th className="text-center">
                                    Tdk Simetris/Acak
                                  </th>
                                  <th className="text-center"> Serabut</th>
                                  <th className="text-center">
                                    Kesesuaian Packaging (&radic;/x)
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <Field
                                      name="printing_terpotong"
                                      className="form-control"
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="tidak_simetris"
                                      className="form-control"
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="serabut"
                                      className="form-control"
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="kesesuaian_packaging"
                                      className="form-check-input"
                                    />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </fieldset>
                        </div>
                      </div>

                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <fieldset>
                            <legend>Gross Weight (Berat dengan kemasan)</legend>
                            <table className="table table-bordered table-hovered table-striped table-hover sealing">
                              <thead>
                                <tr>
                                  <th className="text-center" colSpan={10}>
                                    Berat per pcs (gr)
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                {fields.cek_berat1 ||
                                fields.cek_berat2 ||
                                fields.cek_berat3 ||
                                fields.cek_berat4 ||
                                fields.cek_berat5 ||
                                fields.cek_berat6 ||
                                fields.cek_berat7 ||
                                fields.cek_berat8 ||
                                fields.cek_berat9 ||
                                fields.cek_berat10 ? (
                                  <>
                                    <tr>
                                      <td align="center">
                                        <Label
                                          name="cek_berat1"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat1"
                                          className="form-control"
                                        />
                                      </td>
                                      <td align="center">
                                        <Label
                                          name="cek_berat2"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat2"
                                          className="form-control"
                                        />
                                      </td>
                                      <td align="center">
                                        <Label
                                          name="cek_berat3"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat3"
                                          className="form-control"
                                        />
                                      </td>
                                      <td align="center">
                                        <Label
                                          name="cek_berat4"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat4"
                                          className="form-control"
                                        />
                                      </td>
                                      <td align="center">
                                        <Label
                                          name="cek_berat5"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat5"
                                          className="form-control"
                                        />
                                      </td>
                                    </tr>
                                    <tr>
                                      <td align="center">
                                        <Label
                                          name="cek_berat6"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat6"
                                          className="form-control"
                                        />
                                      </td>
                                      <td align="center">
                                        <Label
                                          name="cek_berat7"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat7"
                                          className="form-control"
                                        />
                                      </td>
                                      <td align="center">
                                        <Label
                                          name="cek_berat8"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat8"
                                          className="form-control"
                                        />
                                      </td>
                                      <td align="center">
                                        <Label
                                          name="cek_berat9"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat9"
                                          className="form-control"
                                        />
                                      </td>
                                      <td align="center">
                                        <Label
                                          name="cek_berat10"
                                          className="form-label"
                                        />
                                        <Field
                                          name="cek_berat10"
                                          className="form-control"
                                        />
                                      </td>
                                    </tr>
                                  </>
                                ) : undefined}
                              </tbody>
                            </table>
                          </fieldset>
                        </div>
                      </div>
                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <table className="table table-bordered table-hovered table-striped table-hover sealing">
                            <thead>
                              <tr>
                                {fields.berat_per_innerbox ? (
                                  <th className="text-center">
                                    Berat Per Innerbox (gr)
                                  </th>
                                ) : undefined}
                                {fields.berat_per_bag ? (
                                  <th className="text-center">
                                    Berat Per Bag (gr)
                                  </th>
                                ) : undefined}
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                {fields.berat_per_innerbox ? (
                                  <td>
                                    <Field
                                      name="berat_per_innerbox"
                                      className="form-control"
                                    />
                                  </td>
                                ) : undefined}
                                {fields.berat_per_bag ? (
                                  <td>
                                    <Field
                                      name="berat_per_bag"
                                      className="form-control"
                                    />
                                  </td>
                                ) : undefined}
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div className="row mb-3">
                        <div className="col-sm-12">
                          <fieldset>
                            <legend> Organoleptic (√/x) </legend>
                            <table className="table table-bordered table-hovered table-striped table-hover sealing">
                              <thead>
                                <tr>
                                  <th className="text-center">Warna </th>
                                  <th className="text-center">Rasa</th>
                                  <th className="text-center">Bau</th>
                                  <th className="text-center">
                                    Jenis Temuan Kontaminasi
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <Field
                                      name="organ_warna"
                                      className="form-check-input"
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="organ_rasa"
                                      className="form-check-input"
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="organ_bau"
                                      className="form-check-input"
                                    />
                                  </td>
                                  <td align="center">
                                    <Field
                                      name="organ_kontaminasi"
                                      className="form-control"
                                    />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </fieldset>
                        </div>
                      </div>
                      <fieldset>
                        <legend>Print Coding </legend>
                        <div className="row mb-3">
                          <Label
                            name="prod_mfg"
                            className="col-sm-2 col-form-label"
                          />
                          <div className="col-sm-4">
                            <Field name="prod_mfg" className="form-control" />
                          </div>
                          <Label
                            name="coding_exp"
                            className="col-sm-2 col-form-label "
                          />
                          <div className="col-sm-4">
                            <Field
                              name="coding_exp"
                              className="form-control "
                            />
                          </div>
                        </div>
                        <div className="row mb-3">
                          <div className="col-sm-2">
                            <Label name="print_code" className="form-label" />
                            <br />
                            <Field name="print_code" className="form-control" />
                          </div>
                          <div className="col-sm-2">
                            <Label name="print_time" className="form-label" />
                            <br />
                            <Field name="print_time" className="form-control" />
                          </div>
                          <div className="col-sm-2">
                            <Label name="print_mesin" className="form-label" />
                            <br />
                            <Field
                              name="print_mesin"
                              className="form-control"
                            />
                          </div>
                          <div className="col-sm-4">
                            <Label
                              name="nomor_barcode"
                              className="form-label"
                            />
                            <br />
                            <Field
                              name="nomor_barcode"
                              className="form-control"
                            />
                          </div>
                          <div className="col-sm-2">
                            <Label name="test_sensor" className="form-label" />
                            <br />
                            <Field
                              name="test_sensor"
                              className="form-check-input"
                            />
                          </div>
                        </div>
                      </fieldset>

                      <fieldset>
                        <legend>Vakum</legend>
                        <div className="row mb-3">
                          <div className="col-sm-2">
                            <Label name="vakum1" className="form-label" />
                            <br />
                            <Field name="vakum1" className="form-check-input" />
                          </div>
                          <div className="col-sm-2">
                            <Label name="vakum2" className="form-label" />
                            <br />
                            <Field name="vakum2" className="form-check-input" />
                          </div>
                          <div className="col-sm-8">
                            <Label name="customer" className="form-label" />
                            <br />
                            <Field name="customer" className="form-control" />
                          </div>
                        </div>
                      </fieldset>
                      <div className="row mb-3">
                        <div>
                          <Label name="keterangan" className="form-label" />
                          <br />
                          <Field name="keterangan" className="form-control" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer">
                    {this.props.onClose ? (
                      <button
                        type="button"
                        className="btn btn-outline-secondary"
                        data-bs-dismiss="modal"
                        onClick={this.props.onClose}
                        disabled={this.state.loading}
                      >
                        Close
                      </button>
                    ) : undefined}
                    {this.props.data !== undefined ? (
                      this.props.data === null ? (
                        <>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}
                          >
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}
                          >
                            {this.state.loading === "insert" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      ) : (
                        <>
                          <button
                            className="btn btn-danger"
                            type="button"
                            disabled={this.state.loading}
                            onClick={(e) => {
                              e.preventDefault();
                              this.DeleteRecord();
                            }}
                          >
                            {this.state.loading === "delete" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Deleting...</span>
                              </>
                            ) : (
                              "Delete"
                            )}
                          </button>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}
                          >
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}
                          >
                            {this.state.loading === "edit" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      )
                    ) : undefined}
                  </div>
                </Form>
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default SealingForm;
