import React from "react";
import Main from "../../../layout/main";
import axios from "axios";
import { Carousel } from "react-responsive-carousel";
import config from "../../../config";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Line } from "react-chartjs-2";
class BscDashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      position: null,
      selects: {
        moulding: false,
        baking: false,
        oiling: false,
        seasoning: false,
      },
      datas: {
        moulding: undefined,
        baking: undefined,
        oiling: undefined,
        seasoning: undefined,
      },
    };

    this.setSelects = this.setSelects.bind(this);
  }
  configGraph = {
    moulding: {
      dataset: {
        weight_per_pcs_left: {
          label: "Left",
          color: "#28a745",
        },
        weight_per_pcs_center: {
          label: "Center",
          color: "#007bff",
        },
        weight_per_pcs_right: {
          label: "Right",
          color: "#ffc107",
        },
      },
      fieldmin: "weight_min",
      fieldmax: "weight_max",
      title: "Weight Graph",
    },
    baking: {
      dataset: {
        weight_left: {
          label: "Left",
          color: "#28a745",
        },
        weight_center: {
          label: "Center",
          color: "#007bff",
        },
        weight_right: {
          label: "Right",
          color: "#ffc107",
        },
      },
      fieldmin: "min_weight_baking",
      fieldmax: "max_weight_baking",
      title: "Weight Graph",
    },
    seasoning: {
      dataset: {
        season_left: {
          label: "Left",
          color: "#28a745",
        },
        season_center: {
          label: "Center",
          color: "#007bff",
        },
        season_right: {
          label: "Right",
          color: "#ffc107",
        },
      },

      fieldmin: "min_weight_after_seasoning",
      fieldmax: "max_weight_after_seasoning",
      title: "Weight After Seasoning Graph",
    },
    oiling: {
      dataset: {
        oil_left: {
          label: "Left",
          color: "#28a745",
        },
        oil_center: {
          label: "Center",
          color: "#007bff",
        },
        oil_right: {
          label: "Right",
          color: "#ffc107",
        },
      },
      fieldmin: "min_weight_after_oil",
      fieldmax: "max_weight_after_oil",
      title: "Weight After Oiling Graph",
    },
  };

  timeout = null;
  setSelects(e) {
    const selects = { ...this.state.selects };
    if (e.target.checked) {
      selects[e.target.name] = true;
    } else {
      selects[e.target.name] = false;
    }
    this.setState(
      {
        selects: {
          moulding: false,
          baking: false,
          oiling: false,
          seasoning: false,
        },
      },
      () => {
        this.setState({
          selects: selects,
        });
      }
    );
  }
  getPosition() {
    const selects = this.getSelects({ ...this.state.selects });
    return selects[this.state.position]
      ? ("Report " + selects[this.state.position]).toUpperCase()
      : "";
  }
  getSelects(selects) {
    const selecttrue = [];
    for (const position in selects) {
      if (selects[position]) {
        selecttrue.push(position);
      }
    }
    return selecttrue;
  }
  getGraphData(rawdatas, fields, fieldmin, fieldmax, title) {
    const labels = [];
    const dataMin = [];
    const dataMax = [];
    const datas = {};
    for (const key in fields) {
      datas[key] = [];
    }
    for (var i = 0; i < rawdatas.length; i++) {
      for (const key in fields) {
        if (rawdatas[i][key] > 0) {
          datas[key].push(rawdatas[i][key]);
        } else {
          datas[key].push(null);
        }
      }

      labels.push(rawdatas[i].time_check);
      dataMin.push(rawdatas[i][fieldmin]);
      dataMax.push(rawdatas[i][fieldmax]);
    }
    const datasets = [];
    for (const key in fields) {
      datasets.push({
        label: fields[key].label,
        data: datas[key],
        borderColor: fields[key].color,
      });
    }
    return {
      title: title,
      data: {
        labels: labels,
        datasets: [
          ...datasets,
          {
            label: "Over",
            fill: "end",
            data: dataMax,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
          },
          {
            label: "Under",
            data: dataMin,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
            fill: "start",
          },
        ],
      },
    };
  }
  checkData(datas, datasets, fieldmin, fieldmax) {
    const data = datas[datas.length - 1];
    for (const field in datasets) {
      //for (let i = 0; i < fields.length; i++) {
      if (data[field] < data[fieldmin]) return true;
      else if (data[field] > data[fieldmax]) return true;
    }
    return false;
  }
  renderItems(position) {
    const datas = this.state.datas[position];

    const components = [];
    if (datas) {
      let index = 0;
      for (let mesin in datas) {
        const isinvalid = this.checkData(
          datas[mesin],
          this.configGraph[position].dataset,
          this.configGraph[position].fieldmin,
          this.configGraph[position].fieldmax
        );

        components.push(
          <div
            className={
              index === 0
                ? isinvalid
                  ? "borderBlink"
                  : ""
                : isinvalid
                ? "mt-2 borderBlink"
                : "mt-2"
            }
          >
            <div className="card" key={"items" + mesin}>
              <div className="card-head">
                <h4>{mesin === "1" ? " Mesin A" : "Mesin B"}</h4>
              </div>
              <div className="card-body">
                <Line
                  data={
                    this.getGraphData(
                      datas[mesin],
                      this.configGraph[position].dataset,
                      this.configGraph[position].fieldmin,
                      this.configGraph[position].fieldmax,
                      this.configGraph[position].title
                    ).data
                  }
                  options={{
                    responsive: true,
                    maintainAspectRatio: false,
                    plugins: {
                      legend: {
                        position: "top",
                      },
                      title: {
                        display: false,
                        //text: this.configGraph[position].title,
                      },
                    },
                  }}
                />
              </div>
            </div>
          </div>
        );
        index++;
      }
    }
    return components;
  }
  renderCarouselItems() {
    const selects = this.getSelects({ ...this.state.selects });
    const components = [];
    for (let i = 0; i < selects.length; i++) {
      components.push(
        <div className="carouselItem" key={"carouselItem" + selects[i]}>
          {this.renderItems(selects[i])}
        </div>
      );
    }
    return components;
  }
  getData() {
    const selects = this.getSelects({ ...this.state.selects });
    const position = selects[this.state.position];
    if (selects.length > 0) {
      if (this.timeout) clearTimeout(this.timeout);
      const url = config.api + config.path.dashboard_bsc;
      axios
        .get(url, {
          params: {
            position: position,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          this.setState(
            {
              datas: { ...this.state.datas, [position]: data.datas },
            },
            () => {
              this.timeout = setTimeout(this.getData.bind(this), 11000);
            }
          );
        });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      JSON.stringify(this.state.selects) !== JSON.stringify(prevState.selects)
    ) {
      this.setState({
        position:
          this.getSelects({ ...this.state.selects }).length > 0 ? 0 : null,
      });
    } else if (
      prevState.position !== this.state.position &&
      this.state.position !== null
    ) {
      this.getData();
    }
  }
  render() {
    const selects = this.getSelects({ ...this.state.selects });
    return (
      <Main>
        <div className="container-fluid dashboard mt-3">
          <div className="row">
            <div className="col-sm text-start">
              <h1>{this.getPosition()}</h1>
            </div>
            <div className="col-sm text-end">
              <div
                className="btn-group btn-group-sm"
                role="group"
                aria-label="Basic checkbox toggle button group"
              >
                <input
                  type="checkbox"
                  className="btn-check"
                  id="moulding"
                  autoComplete="off"
                  name="moulding"
                  checked={this.state.selects.moulding}
                  onChange={this.setSelects}
                />
                <label className="btn btn-outline-primary" htmlFor="moulding">
                  Moulding
                </label>
                <input
                  type="checkbox"
                  className="btn-check"
                  id="baking"
                  name="baking"
                  checked={this.state.selects.baking}
                  onChange={this.setSelects}
                  autoComplete="off"
                />
                <label className="btn btn-outline-primary" htmlFor="baking">
                  Baking
                </label>
                <input
                  type="checkbox"
                  className="btn-check"
                  id="oiling"
                  name="oiling"
                  autoComplete="off"
                  checked={this.state.selects.oiling}
                  onChange={this.setSelects}
                />
                <label className="btn btn-outline-primary" htmlFor="oiling">
                  Oiling
                </label>
                <input
                  type="checkbox"
                  className="btn-check"
                  id="seasoning"
                  name="seasoning"
                  autoComplete="off"
                  checked={this.state.selects.seasoning}
                  onChange={this.setSelects}
                />
                <label className="btn btn-outline-primary" htmlFor="seasoning">
                  Seasoning
                </label>
              </div>
            </div>
          </div>
          <div className="row mt-2 mb-2 item">
            {selects.length > 0 ? (
              <Carousel
                showArrows={true}
                interval={5000}
                autoPlay
                stopOnHover={false}
                showThumbs={false}
                showStatus={false}
                showIndicators={false}
                infiniteLoop
                onChange={(index) => {
                  this.setState({ position: index });
                }}
              >
                {this.renderCarouselItems()}
              </Carousel>
            ) : null}
          </div>
        </div>
      </Main>
    );
  }
}
export default BscDashboard;
