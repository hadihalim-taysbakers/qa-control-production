import React from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import Select from "react-select";
import Main from "../../../layout/main";
import Approval from "../../../Approval";
import { formatTime } from "../../../utils/formatter";
import GraphQC from "./GraphQC";
import config from "../../../config";
import QCForm from "./QCForm";
import Form, { FormContext } from "../../../components/Form";
import Field from "../../../components/Field";
import Label from "../../../components/Label";
class qc extends React.Component {
  refForm = React.createRef();
  fields = {
    weight_min: {
      name: "weight_min",
      id: "weight_min",
      label: "Min",
      attributeName: "Berat minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    weight_max: {
      name: "weight_max",
      id: "weight_max",
      label: "Max",
      attributeName: "Berat maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    diameter_min: {
      name: "diameter_min",
      id: "diameter_min",
      label: "Min",
      attributeName: "Diameter minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    diameter_max: {
      name: "diameter_max",
      id: "diameter_max",
      label: "Max",
      attributeName: "Diameter maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    length_min: {
      name: "length_min",
      id: "length_min",
      label: "Min",
      attributeName: "Panjang minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    length_max: {
      name: "length_max",
      id: "length_max",
      label: "Max",
      attributeName: "Panjang maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    wide_min: {
      name: "wide_min",
      id: "wide_min",
      label: "Min",
      attributeName: "Lebar minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    wide_max: {
      name: "wide_max",
      id: "wide_max",
      label: "Max",
      attributeName: "Lebar maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    height_min: {
      name: "height_min",
      id: "height_min",
      label: "Min",
      attributeName: "Tinggi minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    height_max: {
      name: "height_max",
      id: "height_max",
      label: "Max",
      attributeName: "Tinggi maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
  };
  state = {
    formData: undefined,
    exists: [],
    graph: undefined,
    approveCreator: undefined,
    approveVerifier: undefined,
    approveChecker: undefined,
    approveKnower: undefined,
    creator_name: undefined,
    creator_title: undefined,
    creator_sign: undefined,
    created: undefined,
    checker_name: undefined,
    checker_title: undefined,
    checker_sign: undefined,
    checked: undefined,
    verifier_name: undefined,
    verifier_title: undefined,
    verifier_sign: undefined,
    verified: undefined,
    knower_name: undefined,
    knower_title: undefined,
    knower_sign: undefined,
    knowed: undefined,
    edit: undefined,
    loading: false,
    name: "",
    email: "",
    no_doc: "FRM.QCRD.BSC.02.02; Rev.02",
    tanggal: "",
    operator: "",
    typeprod: "",
    headerData: undefined,
    mesin: "",
    shiftr: "",
    datas: [],
    searchString: [],
    mesinOptions: [],
    shiftOptions: [],
    productOptions: [],
  };
  getFields() {
    const productType = this.getProductType();
    const fields = { ...this.fields };
    if (
      !(
        productType &&
        parseFloat(productType.min_weight_moulding) > 0 &&
        parseFloat(productType.max_weight_moulding) > 0
      )
    ) {
      delete fields.weight_min;
      delete fields.weight_max;
    }

    if (
      !(
        productType &&
        parseFloat(productType.min_diameter_moulding) > 0 &&
        parseFloat(productType.max_diameter_moulding) > 0
      )
    ) {
      delete fields.diameter_min;
      delete fields.diameter_max;
    }
    if (
      !(
        productType &&
        parseFloat(productType.min_length_moulding) > 0 &&
        parseFloat(productType.max_length_moulding) > 0
      )
    ) {
      delete fields.length_max;
      delete fields.length_min;
    }
    if (
      !(
        productType &&
        parseFloat(productType.min_wide_moulding) > 0 &&
        parseFloat(productType.max_wide_moulding) > 0
      )
    ) {
      delete fields.wide_min;
      delete fields.wide_max;
    }
    if (
      !(
        productType &&
        parseFloat(productType.max_height_moulding) > 0 &&
        parseFloat(productType.min_height_moulding) > 0
      )
    ) {
      delete fields.height_max;
      delete fields.height_min;
    }

    return fields;
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      (prevState.tanggal !== this.state.tanggal ||
        prevState.shiftr !== this.state.shiftr ||
        prevState.typeprod !== this.state.typeprod ||
        prevState.mesin !== this.state.mesin) &&
      this.state.tanggal &&
      this.state.shiftr &&
      this.state.typeprod &&
      this.state.mesin
    ) {
      const url = config.api + config.path.moulding;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            type_produksi: this.state.typeprod,
            mesin: this.state.mesin,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          const headerData = {};
          if (data.header) {
            headerData.weight_min = data.header.weight_min;
            headerData.weight_max = data.header.weight_max;
            headerData.diameter_min = data.header.diameter_min;
            headerData.diameter_max = data.header.diameter_max;
            headerData.length_min = data.header.length_min;
            headerData.length_max = data.header.length_max;
            headerData.wide_min = data.header.wide_min;
            headerData.wide_max = data.header.wide_max;
            headerData.height_min = data.header.height_min;
            headerData.height_max = data.header.height_max;
          } else {
            for (let i = 0; i < this.productTypes.length; i++) {
              if (
                parseInt(this.productTypes[i].id) ===
                parseInt(this.state.typeprod)
              ) {
                headerData.weight_min =
                  this.productTypes[i].min_weight_moulding;
                headerData.weight_max =
                  this.productTypes[i].max_weight_moulding;
                headerData.diameter_min =
                  this.productTypes[i].min_diameter_moulding;
                headerData.diameter_max =
                  this.productTypes[i].max_diameter_moulding;
                headerData.length_min =
                  this.productTypes[i].min_length_moulding;
                headerData.length_max =
                  this.productTypes[i].max_length_moulding;
                headerData.wide_min = this.productTypes[i].min_wide_moulding;
                headerData.wide_max = this.productTypes[i].max_wide_moulding;
                headerData.height_min =
                  this.productTypes[i].min_height_moulding;
                headerData.height_max =
                  this.productTypes[i].max_height_moulding;
              }
            }
          }
          this.setState({
            creator_name: data.header ? data.header.creator_name : undefined,
            creator_title: data.header ? data.header.creator_title : undefined,
            creator_sign: data.header ? data.header.creator_sign : undefined,
            created: data.header ? data.header.created : undefined,
            verifier_name: data.header ? data.header.verifier_name : undefined,
            verifier_title: data.header
              ? data.header.verifier_title
              : undefined,
            verifier_sign: data.header ? data.header.verifier_sign : undefined,
            verified: data.header ? data.header.verified : undefined,
            checker_name: data.header ? data.header.checker_name : undefined,
            checker_title: data.header ? data.header.checker_title : undefined,
            checker_sign: data.header ? data.header.checker_sign : undefined,
            checked: data.header ? data.header.checked : undefined,
            knower_name: data.header ? data.header.knower_name : undefined,
            knower_title: data.header ? data.header.knower_title : undefined,
            knower_sign: data.header ? data.header.knower_sign : undefined,
            knowed: data.header ? data.header.knowed : undefined,
            loading: false,
            datas: data.datas,
            operator: data.header ? data.header.operator : "",
            headerData: headerData,
          });
        });
    } else if (
      prevState.typeprod !== this.state.typeprod &&
      this.state.typeprod
    ) {
      for (let i = 0; i < this.productTypes.length; i++) {
        if (
          parseInt(this.productTypes[i].id) === parseInt(this.state.typeprod)
        ) {
          const weight_min = this.productTypes[i].min_weight_moulding;
          const weight_max = this.productTypes[i].max_weight_moulding;
          const diameter_min = this.productTypes[i].min_diameter_moulding;
          const diameter_max = this.productTypes[i].max_diameter_moulding;
          const length_min = this.productTypes[i].min_length_moulding;
          const length_max = this.productTypes[i].max_length_moulding;
          const wide_min = this.productTypes[i].min_wide_moulding;
          const wide_max = this.productTypes[i].max_wide_moulding;
          const height_min = this.productTypes[i].min_height_moulding;
          const height_max = this.productTypes[i].max_height_moulding;
          this.setState({
            weight_min: weight_min,
            weight_max: weight_max,
            diameter_min: diameter_min,
            diameter_max: diameter_max,
            length_min: length_min,
            length_max: length_max,
            wide_min: wide_min,
            wide_max: wide_max,
            height_min: height_min,
            height_max: height_max,
          });
        }
      }
    }

    if (prevState.tanggal !== this.state.tanggal && this.state.tanggal) {
      const url = config.api + config.path.moulding;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          this.setState({
            exists: data.datas,
          });
        });
    }
  }

  submitHeaderChange(datas) {
    if (
      this.state.tanggal &&
      this.state.shiftr &&
      this.state.typeprod &&
      this.state.mesin
    ) {
      this.setState({ loading: true }, () => {
        const formData = new FormData();
        for (const key in datas) {
          formData.append(key, datas[key]);
        }
        formData.append("no_doc", this.state.no_doc);
        axios
          .patch(config.api + config.path.moulding, formData, {
            "Content-Type": "application/x-www-form-urlencoded",
            params: {
              tanggal: this.state.tanggal,
              shiftr: this.state.shiftr,
              mesin: this.state.mesin,
              type_produksi: this.state.typeprod,
            },
          })
          .then((res) => {
            const headerData = { ...this.state.headerData };
            for (const key in datas) {
              headerData[key] = datas[key];
            }
            this.setState({
              loading: false,
              headerData: headerData,
            });
          })
          .catch(() => {
            this.setState({
              loading: false,
            });
            alert("Gagal insert data");
          });
      });
    }
  }
  componentDidMount() {
    const yourDate = new Date();
    this.setState({ tanggal: yourDate.toISOString().split("T")[0] });
  }

  async getOptions() {
    const resShift = await axios.get(config.api + config.path.master_shift);
    const dataShift = resShift.data;
    const shiftOptions = dataShift.map((d) => ({
      value: d.id,
      label: d.judul,
    }));
    this.setState({ shiftOptions: shiftOptions });
    const resMesin = await axios.get(config.api + config.path.master_mesin, {
      params: { posisi: config.page.moulding.posisi },
    });
    const dataMesin = resMesin.data;
    const mesinOptions = dataMesin.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    this.setState({ mesinOptions: mesinOptions });
    const resProduct = await axios.get(config.api + config.path.master_product);
    const dataProduct = resProduct.data;
    const productOptions = dataProduct.map((d) => ({
      value: d.id,
      label: d.product_name,
    }));
    this.productTypes = [...dataProduct];
    this.setState({ productOptions: productOptions });
  }

  componentWillMount() {
    this.getOptions();
  }

  setEdit(ind) {
    this.setState({
      formData: this.state.datas[ind],
    });
  }

  submitCreatorApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("creator_name", data.name);
      formData.append("creator_title", data.title);
      formData.append("creator_sign", data.sign);
      axios
        .patch(config.api + config.path.moulding, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveCreator: undefined,
            creator_name: res.data.header.creator_name,
            creator_title: res.data.header.creator_title,
            creator_sign: res.data.header.creator_sign,
            created: res.data.header.created,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveCreator: undefined,
            creator_name: undefined,
            creator_title: undefined,
            creator_sign: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitCheckerApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("checker_name", data.name);
      formData.append("checker_title", data.title);
      formData.append("checker_sign", data.sign);
      axios
        .patch(config.api + config.path.moulding, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveChecker: undefined,
            checker_name: res.data.header.checker_name,
            checker_title: res.data.header.checker_title,
            checker_sign: res.data.header.checker_sign,
            checked: res.data.header.checked,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveCreator: undefined,
            checker_name: undefined,
            checker_title: undefined,
            checker_sign: undefined,
            checked: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitVerifierApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("verifier_name", data.name);
      formData.append("verifier_title", data.title);
      formData.append("verifier_sign", data.sign);
      axios
        .patch(config.api + config.path.moulding, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: res.data.header.verifier_name,
            verifier_title: res.data.header.verifier_title,
            verifier_sign: res.data.header.verifier_sign,
            verified: res.data.header.verified,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: undefined,
            verifier_title: undefined,
            verifier_sign: undefined,
            verified: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  getProductOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.typeprod) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
            parseInt(exists[j]["type_produksi"]) ===
              parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getProductOptions() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
          parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
          parseInt(exists[j]["type_produksi"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getMesinOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.mesin) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getMesinOptions() {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
          parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getShiftOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.shiftr) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getShiftName(id) {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }

  getMesinName(id) {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getProductName(id) {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getShiftOptions() {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  submitKnowerApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("knower_name", data.name);
      formData.append("knower_title", data.title);
      formData.append("knower_sign", data.sign);
      axios
        .patch(config.api + config.path.moulding, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveKnower: undefined,
            knower_name: res.data.header.knower_name,
            knower_title: res.data.header.knower_title,
            knower_sign: res.data.header.knower_sign,
            knowed: res.data.header.knowed,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveKnower: undefined,
            knower_name: undefined,
            knower_title: undefined,
            knower_sign: undefined,
            knowed: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  getGraphData(fields, min, max, title) {
    const labels = [];
    const dataMin = [];
    const dataMax = [];
    const datas = {};
    for (const key in fields) {
      datas[key] = [];
    }
    for (var i = 0; i < this.state.datas.length; i++) {
      for (const key in fields) {
        if (this.state.datas[i][key] > 0) {
          datas[key].push(this.state.datas[i][key]);
        } else {
          datas[key].push(null);
        }
      }

      labels.push(this.state.datas[i].time_check);
      dataMin.push(min);
      dataMax.push(max);
    }
    const datasets = [];
    for (const key in fields) {
      datasets.push({
        label: fields[key].label,
        data: datas[key],
        borderColor: fields[key].color,
      });
    }
    return {
      title: title,
      data: {
        labels: labels,
        datasets: [
          ...datasets,
          {
            label: "Over",
            fill: "end",
            data: dataMax,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
          },
          {
            label: "Under",
            data: dataMin,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
            fill: "start",
          },
        ],
      },
    };
  }
  removeExistData(typeprod, mesin, shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (
        exists[i].type_produksi === typeprod &&
        exists[i].mesin === mesin &&
        exists[i].shift === shift
      ) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) - 1;
      }
    }
    if (isExist === false) {
      exists.push({
        type_produksi: typeprod,
        mesin: mesin,
        shift: shift,
        qty: "0",
      });
    }
    return exists;
  }
  addExistData(typeprod, mesin, shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (
        exists[i].type_produksi === typeprod &&
        exists[i].mesin === mesin &&
        exists[i].shift === shift
      ) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) + 1;
      }
    }
    if (isExist === false) {
      exists.push({
        type_produksi: typeprod,
        mesin: mesin,
        shift: shift,
        qty: "1",
      });
    }
    return exists;
  }
  getProductType() {
    if (this.productTypes) {
      for (let i = 0; i < this.productTypes.length; i++) {
        if (
          parseInt(this.productTypes[i].id) === parseInt(this.state.typeprod)
        ) {
          return this.productTypes[i];
        }
      }
    }
    return null;
  }
  render() {
    const productType = this.getProductType();
    const weight_min = productType
      ? parseFloat(productType.min_weight_moulding)
      : 0;
    const weight_max = productType
      ? parseFloat(productType.max_weight_moulding)
      : 0;
    const diameter_min = productType
      ? parseFloat(productType.min_diameter_moulding)
      : 0;
    const diameter_max = productType
      ? parseFloat(productType.max_diameter_moulding)
      : 0;
    const length_min = productType
      ? parseFloat(productType.min_length_moulding)
      : 0;
    const length_max = productType
      ? parseFloat(productType.max_length_moulding)
      : 0;
    const wide_min = productType
      ? parseFloat(productType.min_wide_moulding)
      : 0;
    const wide_max = productType
      ? parseFloat(productType.max_wide_moulding)
      : 0;
    const height_min = productType
      ? parseFloat(productType.min_height_moulding)
      : 0;
    const height_max = productType
      ? parseFloat(productType.max_height_moulding)
      : 0;

    const hd_weight_min = this.state.headerData
      ? parseFloat(this.state.headerData.weight_min)
      : 0;
    const hd_weight_max = this.state.headerData
      ? parseFloat(this.state.headerData.weight_max)
      : 0;
    const hd_diameter_min = this.state.headerData
      ? parseFloat(this.state.headerData.diameter_min)
      : 0;
    const hd_diameter_max = this.state.headerData
      ? parseFloat(this.state.headerData.diameter_max)
      : 0;
    const hd_length_min = this.state.headerData
      ? parseFloat(this.state.headerData.length_min)
      : 0;
    const hd_length_max = this.state.headerData
      ? parseFloat(this.state.headerData.length_max)
      : 0;
    const hd_wide_max = this.state.headerData
      ? parseFloat(this.state.headerData.wide_max)
      : 0;
    const hd_wide_min = this.state.headerData
      ? parseFloat(this.state.headerData.wide_min)
      : 0;
    const hd_height_max = this.state.headerData
      ? parseFloat(this.state.headerData.height_max)
      : 0;
    const hd_height_min = this.state.headerData
      ? parseFloat(this.state.headerData.height_min)
      : 0;

    let colspannum = 30;
    if (weight_min > 0 && weight_max > 0) colspannum = colspannum + 3;
    if (diameter_min > 0 && diameter_max > 0) colspannum = colspannum + 3;
    if (length_min > 0 && length_max > 0) colspannum = colspannum + 3;
    if (wide_min > 0 && wide_max > 0) colspannum = colspannum + 3;
    if (height_max > 0 && height_min > 0) colspannum = colspannum + 3;

    return (
      <>
        <Main>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm">
                <h4 className="page-header text-center">
                  DOUGH CHECK AFTER MOULDING
                </h4>
                <h6 className="page-header text-center">
                  PEMERIKSAAN HASIL ADONAN SETELAH CETAK
                </h6>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-3 form-group">
                <label htmlFor="tanggal">Date :</label>
                <input
                  disabled={this.state.loading}
                  required
                  type="date"
                  name="tanggal"
                  id="tanggal"
                  className="form-control"
                  value={this.state.tanggal}
                  onChange={(e) => this.setState({ tanggal: e.target.value })}
                />
              </div>
              <div className="col-sm-3 form-group">
                <label htmlFor="shiftr">Shift :</label>
                <Select
                  isDisabled={this.state.loading}
                  name="shiftr"
                  id="shiftr"
                  options={this.getShiftOptions()}
                  value={this.getShiftOptionValue()}
                  onChange={(e) => {
                    this.setState({ shiftr: e.value });
                  }}
                />
              </div>
              <div className="col-sm-3 form-group">
                <label htmlFor="shiftr">Mesin :</label>
                <Select
                  isDisabled={this.state.loading}
                  name="mesin"
                  id="mesin"
                  options={this.getMesinOptions()}
                  value={this.getMesinOptionValue()}
                  onChange={(e) => {
                    this.setState({ mesin: e.value });
                  }}
                />
              </div>
              <div className="col-sm-3 form-group">
                <label htmlFor="typeprod">Type Production</label>
                <Select
                  isDisabled={this.state.loading}
                  name="typeprod"
                  id="typeprod"
                  options={this.getProductOptions()}
                  value={this.getProductOptionValue()}
                  onChange={(e) => {
                    this.setState({ typeprod: e.value });
                  }}
                />
              </div>
            </div>
            {this.state.tanggal &&
            this.state.typeprod &&
            this.state.mesin &&
            this.state.shiftr ? (
              <>
                <div className="row">
                  <div className="col-sm-4">
                    <font size="2">
                      Pengambilan sample setiap 30 Menit, sebanyak 10 pcs per
                      sisi
                    </font>
                  </div>
                  <div className="col-sm-4 text-center">
                    <font size="2">Note : L : Left, C : Center & R: Right</font>
                  </div>
                  <div className="col-sm-4 text-right">
                    <font size="2">No. Dok : {this.state.no_doc}</font>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12">
                    {this.fields ? (
                      <Form
                        fields={this.getFields()}
                        defaultValues={this.state.headerData}
                        loading={this.state.loading}
                        onSubmit={(data) => {
                          this.submitHeaderChange(data);
                        }}>
                        <FormContext.Consumer>
                          {({ submit, validate, errors }) => (
                            <table className="table data table-bordered table-hovered  table-striped table-hover qc">
                              <thead>
                                <tr>
                                  <th rowSpan="3">Time Check</th>
                                  {weight_min > 0 && weight_max > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="weight_min" />
                                          <Field
                                            name="weight_min"
                                            className="form-control-plaintext"
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="weight_max" />
                                          <Field
                                            name="weight_max"
                                            className="form-control-plaintext"
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  {diameter_min > 0 && diameter_max > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="diameter_min" />
                                          <Field
                                            name="diameter_min"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="diameter_max" />
                                          <Field
                                            name="diameter_max"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  {length_min > 0 && length_max > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="length_min" />
                                          <Field
                                            name="length_min"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="length_max" />
                                          <Field
                                            name="length_max"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  {wide_min > 0 && wide_max > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="wide_min" />
                                          <Field
                                            name="wide_min"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="wide_max" />
                                          <Field
                                            name="wide_max"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  {height_min > 0 && height_max > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="height_min" />
                                          <Field
                                            name="height_min"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="height_max" />
                                          <Field
                                            name="height_max"
                                            className="form-control-plaintext"
                                            readOnly={
                                              true || this.state.loading
                                            }
                                            onChange={() => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}

                                  <th colSpan="3">Shape (&radic;/x)</th>
                                  <th colSpan="3">Texture (&radic;/x)</th>
                                  <th colSpan="3">Color (&radic;/x)</th>
                                  <th colSpan="9">Organoleptik</th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Status Prod (&radic;/x)</span>
                                  </th>
                                  <th rowSpan="3">No.Lot Batch No.Lot Set</th>
                                  <th rowSpan="3">
                                    Corrective Action Tindakan Perbaikan
                                  </th>
                                </tr>
                                <tr>
                                  {weight_min > 0 && weight_max > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{ fontWeight: "bold" }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                weight_per_pcs_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                weight_per_pcs_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                weight_per_pcs_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData.weight_min,
                                              this.state.headerData.weight_max,
                                              "Weight Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Weight
                                        <br />
                                        (gr)
                                      </button>
                                    </th>
                                  ) : undefined}
                                  {diameter_min > 0 && diameter_max > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                diameter_per_pcs_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                diameter_per_pcs_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                diameter_per_pcs_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData
                                                .diameter_min,
                                              this.state.headerData
                                                .diameter_max,
                                              "Diameter Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Diameter
                                        <br />
                                        (mm)
                                      </button>
                                    </th>
                                  ) : undefined}
                                  {length_min > 0 && length_max > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                length_per_pcs_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                length_per_pcs_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                length_per_pcs_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData.length_min,
                                              this.state.headerData.length_max,
                                              "Length Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Length <br /> (mm)
                                      </button>
                                    </th>
                                  ) : undefined}
                                  {wide_min > 0 && wide_max > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                wide_per_pcs_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                wide_per_pcs_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                wide_per_pcs_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData.wide_min,
                                              this.state.headerData.wide_max,
                                              "Wide Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Wide <br /> (mm)
                                      </button>
                                    </th>
                                  ) : undefined}
                                  {height_min > 0 && height_max > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                stack_per_pcs_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                stack_per_pcs_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                stack_per_pcs_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData.height_min,
                                              this.state.headerData.height_max,
                                              "Stack Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Stack <br /> (mm)
                                      </button>
                                    </th>
                                  ) : undefined}

                                  <th colSpan="3">Bentuk</th>
                                  <th colSpan="3">Tekstur</th>
                                  <th colSpan="3">Warna</th>
                                  <th colSpan="3">(Smell/Bau)</th>
                                  <th colSpan="6">
                                    Visual / Kontaminasi (pcs)
                                  </th>
                                </tr>
                                <tr>
                                  {weight_min > 0 && weight_max > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  {diameter_min > 0 && diameter_max > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  {length_min > 0 && length_max > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  {wide_min > 0 && wide_max > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  {height_min > 0 && height_max > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}

                                  <th scope="col">L</th>
                                  <th scope="col">C</th>
                                  <th scope="col">R</th>
                                  <th scope="col">L</th>
                                  <th scope="col">C</th>
                                  <th scope="col">R</th>
                                  <th scope="col">L</th>
                                  <th scope="col">C</th>
                                  <th scope="col">R</th>
                                  <th scope="col">L</th>
                                  <th scope="col">C</th>
                                  <th scope="col">R</th>
                                  <th scope="col" className="vertical">
                                    <span>Logam</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Plastik</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Rambut</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Oli</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Benang</span>
                                  </th>
                                  <th scope="col" className="vertical">
                                    <span>Other</span>
                                  </th>
                                </tr>
                              </thead>

                              <tbody>
                                {this.state.datas.map((qc, index) => {
                                  const weight_per_pcs_left = parseFloat(
                                    qc.weight_per_pcs_left
                                  );
                                  const weight_per_pcs_center = parseFloat(
                                    qc.weight_per_pcs_center
                                  );
                                  const weight_per_pcs_right = parseFloat(
                                    qc.weight_per_pcs_right
                                  );
                                  const diameter_per_pcs_left = parseFloat(
                                    qc.diameter_per_pcs_left
                                  );
                                  const diameter_per_pcs_center = parseFloat(
                                    qc.diameter_per_pcs_center
                                  );
                                  const diameter_per_pcs_right = parseFloat(
                                    qc.diameter_per_pcs_right
                                  );
                                  const length_per_pcs_left = parseFloat(
                                    qc.length_per_pcs_left
                                  );
                                  const length_per_pcs_center = parseFloat(
                                    qc.length_per_pcs_center
                                  );
                                  const length_per_pcs_right = parseFloat(
                                    qc.length_per_pcs_right
                                  );
                                  const wide_per_pcs_left = parseFloat(
                                    qc.wide_per_pcs_left
                                  );
                                  const wide_per_pcs_center = parseFloat(
                                    qc.wide_per_pcs_center
                                  );
                                  const wide_per_pcs_right = parseFloat(
                                    qc.wide_per_pcs_right
                                  );

                                  const stack_per_pcs_left = parseFloat(
                                    qc.stack_per_pcs_left
                                  );
                                  const stack_per_pcs_center = parseFloat(
                                    qc.stack_per_pcs_center
                                  );
                                  const stack_per_pcs_right = parseFloat(
                                    qc.stack_per_pcs_right
                                  );

                                  return (
                                    <tr key={index}>
                                      <td className="action">
                                        {!this.state.verifier_name ? (
                                          <>
                                            <button
                                              type="button"
                                              className="btn btn-outline-info btn-block btn-sm"
                                              onClick={() => {
                                                this.setEdit(index);
                                              }}>
                                              &#128269;
                                            </button>

                                            {formatTime(qc.time_check)}
                                          </>
                                        ) : (
                                          <>{formatTime(qc.time_check)} </>
                                        )}
                                      </td>
                                      {weight_min > 0 && weight_max > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                weight_per_pcs_left <
                                                hd_weight_min
                                                  ? "red"
                                                  : weight_per_pcs_left >
                                                    hd_weight_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {weight_per_pcs_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                weight_per_pcs_center <
                                                hd_weight_min
                                                  ? "red"
                                                  : weight_per_pcs_center >
                                                    hd_weight_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {weight_per_pcs_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                weight_per_pcs_right <
                                                hd_weight_min
                                                  ? "red"
                                                  : weight_per_pcs_right >
                                                    hd_weight_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {weight_per_pcs_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {diameter_min > 0 && diameter_max > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                diameter_per_pcs_left <
                                                hd_diameter_min
                                                  ? "red"
                                                  : diameter_per_pcs_left >
                                                    hd_diameter_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {diameter_per_pcs_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                diameter_per_pcs_center <
                                                hd_diameter_min
                                                  ? "red"
                                                  : diameter_per_pcs_center >
                                                    hd_diameter_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {diameter_per_pcs_center.toFixed(
                                                1
                                              )}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                diameter_per_pcs_right <
                                                hd_diameter_min
                                                  ? "red"
                                                  : diameter_per_pcs_right >
                                                    hd_diameter_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {diameter_per_pcs_right.toFixed(
                                                1
                                              )}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {length_min > 0 && length_max > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                length_per_pcs_left <
                                                hd_length_min
                                                  ? "red"
                                                  : length_per_pcs_left >
                                                    length_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {length_per_pcs_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                length_per_pcs_center <
                                                hd_length_min
                                                  ? "red"
                                                  : length_per_pcs_center >
                                                    hd_length_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {length_per_pcs_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                length_per_pcs_right <
                                                hd_length_min
                                                  ? "red"
                                                  : length_per_pcs_right >
                                                    hd_length_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {length_per_pcs_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {wide_min > 0 && wide_max > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                wide_per_pcs_left < hd_wide_min
                                                  ? "red"
                                                  : wide_per_pcs_left >
                                                    hd_wide_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {wide_per_pcs_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                wide_per_pcs_center <
                                                hd_wide_min
                                                  ? "red"
                                                  : wide_per_pcs_center >
                                                    hd_wide_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {wide_per_pcs_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                wide_per_pcs_right < wide_min
                                                  ? "red"
                                                  : wide_per_pcs_right >
                                                    hd_wide_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {wide_per_pcs_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {height_max > 0 && height_min > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                stack_per_pcs_left <
                                                hd_height_min
                                                  ? "red"
                                                  : stack_per_pcs_left >
                                                    hd_height_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {stack_per_pcs_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                stack_per_pcs_center <
                                                hd_height_min
                                                  ? "red"
                                                  : stack_per_pcs_center >
                                                    hd_height_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {stack_per_pcs_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                stack_per_pcs_right <
                                                hd_height_min
                                                  ? "red"
                                                  : stack_per_pcs_right >
                                                    hd_height_max
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {stack_per_pcs_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      <td align="center">
                                        {qc.shape_left === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.shape_center === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.shape_right === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.texture_left === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.texture_center === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.texture_right === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.color_left === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.color_center === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.color_right === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.organoleptik_left === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.organoleptik_center === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {qc.organoleptik_right === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td>{qc.organoleptik_logam}</td>
                                      <td>{qc.organoleptik_plastik}</td>
                                      <td>{qc.organoleptik_rambut}</td>
                                      <td>{qc.organoleptik_oli}</td>
                                      <td>{qc.organoleptik_benang}</td>
                                      <td>{qc.organoleptik_other}</td>
                                      <td align="center">
                                        {qc.status_prod === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td>{qc.no_lot_batch}</td>
                                      <td>{qc.corrective_action}</td>
                                    </tr>
                                  );
                                })}
                              </tbody>
                              {!this.state.verifier_name ? (
                                <tfoot>
                                  <tr>
                                    <td colSpan={colspannum} align="center">
                                      <button
                                        type="button"
                                        className="btn btn-primary"
                                        onClick={(e) => {
                                          if (validate()) {
                                            this.setState({
                                              formData: null, 
                                            });
                                          } else {
                                            alert(
                                              "Silahkan perbaiki data minimum dan maximum"
                                            );
                                          }
                                        }}
                                        value="Add">
                                        &#10010; Add
                                      </button>
                                    </td>
                                  </tr>
                                </tfoot>
                              ) : undefined}
                            </table>
                          )}
                        </FormContext.Consumer>
                      </Form>
                    ) : undefined}
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6 xs-12"></div>
                  <div className="col-md-6 xs-12">
                    <table className="sign" cellPadding={3}>
                      <tbody>
                        <tr>
                        <th width={"20%"}>&nbsp;</th>
                          <th width={"20%"}> Dibuat Oleh,</th>
                          <th width={"20%"}> Diverifikasi Oleh,</th>
                          <th width={"20%"}> Diperiksa Oleh,</th>
                          <th width={"20%"}>Mengetahui Oleh, </th>
                        </tr>
                        <tr>
                          <th>Tanda Tangan</th>
                          {this.state.creator_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.creator_sign}
                                alt="creator sign"
                                width={"100%"}
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 || this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveCreator: {
                                      name: "",
                                      title: "Operator",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.verifier_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.verifier_sign}
                                width={"100%"}
                                alt="verifier sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveVerifier: {
                                      name: "",
                                      title: "Quality Control",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.checker_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.checker_sign}
                                width={"100%"}
                                alt="checker sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  !this.state.verifier_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveChecker: {
                                      name: "",
                                      title: "Supervisor QC",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.knower_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.knower_sign}
                                width={"100%"}
                                alt="knower sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  !this.state.verifier_name ||
                                  !this.state.checker_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveKnower: {
                                      name: "",
                                      title: "Manager QA",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                        </tr>
                        <tr>
                          <th>Nama</th>
                          {this.state.creator_name ? (
                            <td align="center" valign="middle">
                              {this.state.creator_name}
                            </td>
                          ) : undefined}
                          {this.state.verifier_name ? (
                            <td align="center" valign="middle">
                              {this.state.verifier_name}
                            </td>
                          ) : undefined}
                          {this.state.checker_name ? (
                            <td align="center" valign="middle">
                              {this.state.checker_name}
                            </td>
                          ) : undefined}
                          {this.state.knower_name ? (
                            <td align="center" valign="middle">
                              {this.state.knower_name}
                            </td>
                          ) : undefined}
                        </tr>
                        <tr>
                          <th>Jabatan</th>
                          {this.state.creator_title ? (
                            <td align="center" valign="middle">
                              {this.state.creator_title}
                            </td>
                          ) : undefined}
                          {this.state.verifier_title ? (
                            <td align="center" valign="middle">
                              {this.state.verifier_title}
                            </td>
                          ) : undefined}
                           {this.state.checker_title ? (
                            <td align="center" valign="middle">
                              {this.state.checker_title}
                            </td>
                          ) : undefined}
                          {this.state.knower_title ? (
                            <td align="center" valign="middle">
                              {this.state.knower_title}
                            </td>
                          ) : undefined}
                        </tr>
                        <tr>
                          <th>Tanggal</th>
                          {this.state.created ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.created).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.verified ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.verified).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.checked ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.verified).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.knowed ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.knowed).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </>
            ) : undefined}
          </div>
        </Main>
        {/* </div> */}
        <Approval
          loading={this.state.loading}
          title="Dibuat oleh,"
          show={this.state.approveCreator ? true : false}
          data={this.state.approveCreator}
          onSubmit={(data) => {
            this.submitCreatorApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveCreator: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Diverifikasi oleh,"
          show={this.state.approveVerifier ? true : false}
          data={this.state.approveVerifier}
          onSubmit={(data) => {
            this.submitVerifierApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveVerifier: undefined,
            });
          }}
        />
         <Approval
          loading={this.state.loading}
          title="Diperiksa oleh,"
          show={this.state.approveChecker ? true : false}
          data={this.state.approveChecker}
          onSubmit={(data) => {
            this.submitCheckerApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveChecker: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Mengetahui oleh,"
          show={this.state.approveKnower ? true : false}
          data={this.state.approveKnower}
          onSubmit={(data) => {
            this.submitKnowerApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveKnower: undefined,
            });
          }}
        />
        <GraphQC
          title="GRAPH DOUGH CHECK AFTER MOULDING"
          tanggal={this.state.tanggal}
          shift={this.getShiftName(this.state.shiftr)}
          mesin={this.getMesinName(this.state.mesin)}
          product={this.getProductName(this.state.typeprod)}
          show={this.state.graph ? true : false}
          data={this.state.graph ? this.state.graph.data : undefined}
          subtitle={this.state.graph ? this.state.graph.title : undefined}
          onClose={() => {
            this.setState({
              graph: undefined,
            });
          }}
        />

        <QCForm
          header={{
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            shiftLabel: this.getShiftName(this.state.shiftr),
            typeprod: this.state.typeprod,
            typeprodLabel: this.getProductName(this.state.typeprod),
            mesin: this.state.mesin,
            mesinLabel: this.getMesinName(this.state.mesin),
            weight_min: weight_min,
            weight_max: weight_max,
            diameter_min: diameter_min,
            diameter_max: diameter_max,
            length_min: length_min,
            length_max: length_max,
            wide_min: wide_min,
            wide_max: wide_max,
            height_min: height_min,
            height_max: height_max,
          }}
          data={this.state.formData}
          show={this.state.formData !== undefined}
          onAfterDelete={(id) => {
            const currDatas = [...this.state.datas];
            const newDatas = [];
            for (let i = 0; i < currDatas.length; i++) {
              if (parseInt(currDatas[i].id) !== parseInt(id)) {
                newDatas.push(currDatas[i]);
              }
            }
            this.setState({
              exists: this.removeExistData(
                this.state.typeprod,
                this.state.mesin,
                this.state.shiftr
              ),
              datas: newDatas,
              formData: undefined,
            });
          }}
          onAfterEdit={(data) => {
            const currDatas = [...this.state.datas];
            for (let i = 0; i < currDatas.length; i++) {
              if (currDatas[i].id === data.id) {
                currDatas[i] = data;
              }
            }
            this.setState({
              datas: currDatas,
              formData: undefined,
            });
          }}
          onAfterInsert={(datas) => {
            this.setState({
              exists: this.addExistData(
                this.state.typeprod,
                this.state.mesin,
                this.state.shiftr
              ),
              datas: datas,
              formData: undefined,
            });
          }}
          onClose={() => {
            this.setState({
              formData: undefined,
            });
          }}
        />
      </>
    );
  }
}
export default qc;