import React from "react";
import moment from "moment";
import "moment/locale/id";
import axios from "axios";
import config from "./../../../config";
import Field from "./../../../components/Field";
import * as Validator from "validatorjs";
import Form from "./../../../components/Form";
import Label from "./../../../components/Label";

class QCForm extends React.Component {
  constructor(props) {
    super(props);
    this.fields = {
      time_check: {
        name: "time_check",
        id: "time_check",
        label: "Waktu Pengecekan",
        attributeName: "Waktu Pengecekan",
        type: "time",
        rules: ["required"],
      },
      weight_per_pcs_left: {
        name: "weight_per_pcs_left",
        id: "weight_per_pcs_left",
        label: "Berat kiri",
        attributeName: "Berat kiri",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      weight_per_pcs_center: {
        name: "weight_per_pcs_center",
        id: "weight_per_pcs_center",
        label: "Berat tengah",
        attributeName: "Berat tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      weight_per_pcs_right: {
        name: "weight_per_pcs_right",
        id: "weight_per_pcs_right",
        label: "Berat kanan",
        attributeName: "Berat kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      diameter_per_pcs_left: {
        name: "diameter_per_pcs_left",
        id: "diameter_per_pcs_left",
        label: "Diameter kiri",
        attributeName: "Diameter kiri",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      diameter_per_pcs_center: {
        name: "diameter_per_pcs_center",
        id: "diameter_per_pcs_center",
        label: "Diameter tengah",
        attributeName: "Diameter tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      diameter_per_pcs_right: {
        name: "diameter_per_pcs_right",
        id: "diameter_per_pcs_right",
        label: "Diameter kanan",
        attributeName: "Diameter kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      length_per_pcs_left: {
        name: "length_per_pcs_left",
        id: "length_per_pcs_left",
        label: "Panjang kanan",
        attributeName: "Panjang kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      length_per_pcs_center: {
        name: "length_per_pcs_center",
        id: "length_per_pcs_center",
        label: "Panjang tengah",
        attributeName: "Panjang tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      length_per_pcs_right: {
        name: "length_per_pcs_right",
        id: "length_per_pcs_right",
        label: "Panjang kanan",
        attributeName: "Panjang kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      wide_per_pcs_left: {
        name: "wide_per_pcs_left",
        id: "wide_per_pcs_left",
        label: "Lebar kiri",
        attributeName: "Lebar kiri",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      wide_per_pcs_center: {
        name: "wide_per_pcs_center",
        id: "wide_per_pcs_center",
        label: "Lebar tengah",
        attributeName: "Lebar tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      wide_per_pcs_right: {
        name: "wide_per_pcs_right",
        id: "wide_per_pcs_right",
        label: "Lebar kanan",
        attributeName: "Lebar kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      stack_per_pcs_left: {
        name: "stack_per_pcs_left",
        id: "stack_per_pcs_left",
        label: "Tinggi kiri",
        attributeName: "Tinggi kiri",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      stack_per_pcs_center: {
        name: "stack_per_pcs_center",
        id: "stack_per_pcs_center",
        label: "Tinggi tengah",
        attributeName: "Tinggi tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      stack_per_pcs_right: {
        name: "stack_per_pcs_right",
        id: "stack_per_pcs_right",
        label: "Tinggi kanan",
        attributeName: "Tinggi kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      shape_left: {
        name: "shape_left",
        id: "shape_left",
        label: "Bentuk kiri",
        attributeName: "Bentuk kiri",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      shape_center: {
        name: "shape_center",
        id: "shape_center",
        label: "Bentuk tengah",
        attributeName: "Bentuk tengah",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      shape_right: {
        name: "shape_right",
        id: "shape_right",
        label: "Bentuk kanan",
        attributeName: "Bentuk kanan",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      texture_left: {
        name: "texture_left",
        id: "texture_left",
        label: "Texture kiri",
        attributeName: "Texture kiri",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      texture_center: {
        name: "texture_center",
        id: "texture_center",
        label: "Texture tengah",
        attributeName: "Texture tengah",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      texture_right: {
        name: "texture_right",
        id: "texture_right",
        label: "Texture kanan",
        attributeName: "Texture kanan",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      color_left: {
        name: "color_left",
        id: "color_left",
        label: "Warna kiri",
        attributeName: "Warna kiri",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      color_center: {
        name: "color_center",
        id: "color_center",
        label: "Warna tengah",
        attributeName: "Warna tengah",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      color_right: {
        name: "color_right",
        id: "color_right",
        label: "Warna kanan",
        attributeName: "Warna kanan",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      organoleptik_left: {
        name: "organoleptik_left",
        id: "organoleptik_left",
        label: "Organoleptik kiri",
        attributeName: "Organoleptik kiri",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      organoleptik_center: {
        name: "organoleptik_center",
        id: "organoleptik_center",
        label: "Organoleptik tengah",
        attributeName: "Organoleptik tengah",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      organoleptik_right: {
        name: "organoleptik_right",
        id: "organoleptik_right",
        label: "Organoleptik kanan",
        attributeName: "Organoleptik kanan",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      organoleptik_logam: {
        name: "organoleptik_logam",
        id: "organoleptik_logam",
        label: "Organoleptik logam",
        attributeName: "Organoleptik logam",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      organoleptik_plastik: {
        name: "organoleptik_plastik",
        id: "organoleptik_plastik",
        label: "Organoleptik plastik",
        attributeName: "Organoleptik plastik",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      organoleptik_rambut: {
        name: "organoleptik_rambut",
        id: "organoleptik_rambut",
        label: "Organoleptik rambut",
        attributeName: "Organoleptik rambut",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      organoleptik_oli: {
        name: "organoleptik_oli",
        id: "organoleptik_oli",
        label: "Organoleptik oli",
        attributeName: "Organoleptik oli",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      organoleptik_benang: {
        name: "organoleptik_benang",
        id: "organoleptik_benang",
        label: "Organoleptik benang",
        attributeName: "Organoleptik benang",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      organoleptik_other: {
        name: "organoleptik_other",
        id: "organoleptik_other",
        label: "Organoleptik lainnya",
        attributeName: "Organoleptik lainnya",
        type: "number",
        min: 0,
        rules: ["required", "numeric", "min:0"],
      },
      status_prod: {
        name: "status_prod",
        id: "status_prod",
        label: "Status",
        attributeName: "Status",
        type: "checkbox",
        //rules: ["accepted"],
        values: { 0: "0", 1: "1" },
      },
      no_lot_batch: {
        name: "no_lot_batch",
        id: "no_lot_batch",
        label: "No. Lot Batch",
        attributeName: "No. Lot Batch",
        type: "text",
        rules: ["required"],
      },
      corrective_action: {
        name: "corrective_action",
        id: "corrective_action",
        label: "Corrective action",
        attributeName: "Corrective action",
        type: "text",
      },
    };
    this.state = {
      loading: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  getFields() {
    const fields = { ...this.fields };
    const header = { ...this.props.header };
    if (header.weight_min <= 0 && header.weight_max <= 0) {
      delete fields.weight_per_pcs_left;
      delete fields.weight_per_pcs_center;
      delete fields.weight_per_pcs_right;
    }
    if (header.height_max <= 0 && header.height_max <= 0) {
      delete fields.stack_per_pcs_left;
      delete fields.stack_per_pcs_center;
      delete fields.stack_per_pcs_right;
    }

    if (header.wide_min <= 0 && header.wide_max <= 0) {
      delete fields.wide_per_pcs_left;
      delete fields.wide_per_pcs_center;
      delete fields.wide_per_pcs_right;
    }
    if (header.length_min <= 0 && header.length_max <= 0) {
      delete fields.length_per_pcs_left;
      delete fields.length_per_pcs_center;
      delete fields.length_per_pcs_right;
    }
    if (header.diameter_min <= 0 && header.diameter_max <= 0) {
      delete fields.diameter_per_pcs_left;
      delete fields.diameter_per_pcs_center;
      delete fields.diameter_per_pcs_right;
    }

    return fields;
  }
  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState(
      {
        datas: { ...this.state.datas, [name]: value },
      },
      () => {
        const valid = this.validateField(name, value);
        if (valid.valid === true) {
          this.setState({
            errors: { ...this.state.errors, [name]: [] },
          });
        } else {
          this.setState({
            errors: { ...this.state.errors, [name]: valid.errors[name] },
          });
        }
      }
    );
  }
  validateField(name, value) {
    const messages = Validator.getMessages("id");
    Validator.setMessages("id", messages);
    Validator.setAttributeNames(this.attributeNames);
    const validation = new Validator(
      { [name]: value },
      { [name]: this.rules[name] },
      messages
    );
    if (validation.passes()) {
      return { valid: true, errors: null };
    } else {
      const errors = validation.errors.all();
      return {
        valid: false,
        errors: errors,
      };
    }
  }
  InsertRecord(datas) {
    //event.preventDefault();
    let formData = new FormData();
    for (const key in this.props.header) {
      formData.append(key, this.props.header[key]);
    }
    for (const key in datas) {
      formData.append(key, datas[key]);
    }
    this.setState({ loading: "insert" }, () => {
      axios({
        method: "post",
        url: config.api + config.path.moulding,
        data: formData,
        params: {
          tanggal: this.props.header.tanggal,
          shiftr: this.props.header.shiftr,
          mesin: this.props.header.mesin,
          type_produksi: this.props.header.typeprod,
        },
        config: { headers: { "Content-Type": "multipart/form-data" } },
      })
        .then(
          function (response) {
            this.setState({ loading: false }, () => {
              this.props.onAfterInsert(response.data.datas);
              alert("Berhasil untuk menyimpan data");
            });
          }.bind(this)
        )
        .catch((e) => {
          this.setState({ loading: false });
          alert("Gagal untuk menyimpan data");
        });
    });
  }

  DeleteRecord() {
    if (this.props.data && this.props.data.id) {
      this.setState({ loading: "delete" }, () => {
        axios
          .delete(config.api + config.path.moulding, {
            params: {
              id: this.props.data.id,
            },
          })
          .then((res) => {
            if (res.data.success) {
              this.setState({ loading: false }, () => {
                this.props.onAfterDelete(res.data.id);
                alert("Berhasil untuk menghapus data");
              });
            } else {
              alert("gagal delete");
            }
          });
      });
    }
  }
  EditRecord(datas) {
    if (this.props.data) {
      let formData = new FormData();
      for (const key in this.props.header) {
        formData.append(key, this.props.header[key]);
      }
      for (const key in datas) {
        formData.append(key, datas[key]);
      }
      this.setState({ loading: "edit" }, () => {
        axios
          .patch(config.api + config.path.moulding, formData, {
            params: {
              id: this.props.data.id,
            },
          })
          .then(
            function (response) {
              this.setState({ loading: false }, () => {
                this.props.onAfterEdit(response.data.data);
                alert("Berhasil untuk merubah data");
              });
            }.bind(this)
          )
          .catch((e) => {
            this.setState({ loading: false });
            alert("Gagal untuk menyimpan data");
          });
      });
    }
  }

  onSubmit(datas) {
    if (this.props.data) {
      this.EditRecord(datas);
    } else {
      this.InsertRecord(datas);
    }
  }
  render() {
    const fields = this.getFields();
    return (
      <div
        className={this.props.show ? "modal fade show" : "modal"}
        style={this.props.show ? { display: "block" } : undefined}
        tabIndex="-1"
        role="dialog">
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            {this.props.data === undefined ? undefined : (
              <>
                <Form
                  fields={fields}
                  onSubmit={this.onSubmit}
                  loading={this.state.loading}
                  defaultValues={this.props.data ? this.props.data : {}}>
                  <div className="modal-header">
                    <h5 className="modal-title">
                      {this.props.data !== undefined
                        ? this.props.data && this.props.data.id === null
                          ? "EDIT"
                          : "INSERT"
                        : ""}
                      DOUGH CHECK AFTER MOULDING
                    </h5>
                    {this.props.onClose ? (
                      <button
                        type="button"
                        disabled={this.state.loading}
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        //disabled={this.props.loading}
                        onClick={this.props.onClose}>
                        <span aria-hidden="true">&times;</span>
                      </button>
                    ) : undefined}
                  </div>

                  <div className="modal-body">
                    <div className="row mb-3">
                      <div className="col-sm-3">
                        <label htmlFor="tanggal" className="form-label">
                          Tanggal :
                        </label>
                        <br />
                        <b>
                          {moment(this.props.header.tanggal)
                            .locale("id")
                            .format("dddd, DD MMM YYYY")}
                        </b>
                      </div>

                      <div className="col-sm-3">
                        <label htmlFor="tanggal" className="form-label">
                          Shift :
                        </label>
                        <br />
                        <b>{this.props.header.shiftLabel}</b>
                      </div>
                      <div className="col-sm-3">
                        <label htmlFor="mesinLabel" className="form-label">
                          Mesin :
                        </label>
                        <br />
                        <b>{this.props.header.mesinLabel}</b>
                      </div>
                      <div className="col-sm-3">
                        <label htmlFor="typeprodLabel" className="form-label">
                          Type Production :
                        </label>
                        <br />
                        <b>{this.props.header.typeprodLabel}</b>
                      </div>
                    </div>
                    <hr />
                    <div className="row mb-3">
                      <Label
                        name="time_check"
                        className="col-sm-3 col-form-label"
                      />
                      <div className="col-sm-9">
                        <Field name="time_check" className="form-control" />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col-sm-12">
                        <table className="table table-bordered table-hovered table-striped table-hover qc">
                          <thead>
                            <tr>
                              <th></th>
                              <th>L</th>
                              <th>C</th>
                              <th>R</th>
                            </tr>
                          </thead>
                          <tbody>
                            {fields.weight_per_pcs_left ||
                            fields.weight_per_pcs_center ||
                            fields.weight_per_pcs_right ? (
                              <tr>
                                <th>Weight (gr)</th>
                                <td>
                                  <Field
                                    name="weight_per_pcs_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="weight_per_pcs_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="weight_per_pcs_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            {fields.diameter_per_pcs_left ||
                            fields.diameter_per_pcs_center ||
                            fields.diameter_per_pcs_right ? (
                              <tr>
                                <th>Diameter (mm)</th>
                                <td>
                                  <Field
                                    name="diameter_per_pcs_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="diameter_per_pcs_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="diameter_per_pcs_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            {fields.length_per_pcs_left ||
                            fields.length_per_pcs_center ||
                            fields.length_per_pcs_right ? (
                              <tr>
                                <th>Length (mm)</th>
                                <td>
                                  <Field
                                    name="length_per_pcs_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="length_per_pcs_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="length_per_pcs_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            {fields.wide_per_pcs_left ||
                            fields.wide_per_pcs_center ||
                            fields.wide_per_pcs_center ? (
                              <tr>
                                <th>Wide (mm)</th>
                                <td>
                                  <Field
                                    name="wide_per_pcs_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="wide_per_pcs_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="wide_per_pcs_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            {fields.stack_per_pcs_left ||
                            fields.stack_per_pcs_center ||
                            fields.stack_per_pcs_right ? (
                              <tr>
                                <th>Stack Height (mm)</th>
                                <td>
                                  <Field
                                    name="stack_per_pcs_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="stack_per_pcs_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="stack_per_pcs_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            <tr>
                              <th>Shape (&radic;/x)</th>
                              <td align="center">
                                <Field name="shape_left" />
                              </td>
                              <td align="center">
                                <Field name="shape_center" />
                              </td>
                              <td align="center">
                                <Field name="shape_right" />
                              </td>
                            </tr>
                            <tr>
                              <th>Texture (&radic;/x)</th>
                              <td align="center">
                                <Field name="texture_left" />
                              </td>
                              <td align="center">
                                <Field name="texture_center" />
                              </td>
                              <td align="center">
                                <Field name="texture_right" />
                              </td>
                            </tr>
                            <tr>
                              <th>Color (&radic;/x)</th>
                              <td align="center">
                                <Field name="color_left" />
                              </td>
                              <td align="center">
                                <Field name="color_center" />
                              </td>
                              <td align="center">
                                <Field name="color_right" />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col-sm-12">
                        <table className="table table-bordered table-hovered table-striped table-hover qc">
                          <thead>
                            <tr>
                              <th colSpan={9} align="center">
                                Organoleptik
                              </th>
                            </tr>
                            <tr>
                              <th colSpan={3} align="center">
                                (Smell/Bau)
                              </th>
                              <th colSpan={6} align="center">
                                Visual / Kontaminasi (pcs)
                              </th>
                            </tr>
                            <tr>
                              <th align="center">L</th>
                              <th align="center">C</th>
                              <th align="center">R</th>
                              <th align="center">Logam</th>
                              <th align="center">Plastik</th>
                              <th align="center">Rambut</th>
                              <th align="center">Oli</th>
                              <th align="center">Benang</th>
                              <th align="center">Other</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td align="center">
                                <Field name="organoleptik_left" />
                              </td>
                              <td align="center">
                                <Field name="organoleptik_center" />
                              </td>
                              <td align="center">
                                <Field name="organoleptik_right" />
                              </td>
                              <td>
                                <Field
                                  name="organoleptik_logam"
                                  className="form-control"
                                />
                              </td>
                              <td>
                                <Field
                                  name="organoleptik_plastik"
                                  className="form-control"
                                />
                              </td>
                              <td>
                                <Field
                                  name="organoleptik_rambut"
                                  className="form-control"
                                />
                              </td>
                              <td>
                                <Field
                                  name="organoleptik_oli"
                                  className="form-control"
                                />
                              </td>
                              <td>
                                <Field
                                  name="organoleptik_benang"
                                  className="form-control"
                                />
                              </td>
                              <td>
                                <Field
                                  name="organoleptik_other"
                                  className="form-control"
                                />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col-sm-2">
                        <Label name="status_prod" className="form-label" />
                        <br />
                        <Field
                          name="status_prod"
                          className="form-check-input"
                        />
                      </div>
                      <div className="col-sm-5">
                        <Label name="no_lot_batch" className="form-label" />
                        <br />
                        <Field name="no_lot_batch" className="form-control" />
                      </div>
                      <div className="col-sm-5">
                        <Label
                          name="corrective_action"
                          className="form-label"
                        />
                        <br />
                        <Field
                          name="corrective_action"
                          className="form-control"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer">
                    {this.props.onClose ? (
                      <button
                        type="button"
                        className="btn btn-outline-secondary"
                        data-bs-dismiss="modal"
                        onClick={this.props.onClose}
                        disabled={this.state.loading}>
                        Close
                      </button>
                    ) : undefined}
                    {this.props.data !== undefined ? (
                      this.props.data === null ? (
                        <>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}>
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}>
                            {this.state.loading === "insert" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      ) : (
                        <>
                          <button
                            className="btn btn-danger"
                            type="button"
                            disabled={this.state.loading}
                            onClick={(e) => {
                              e.preventDefault();
                              this.DeleteRecord();
                            }}>
                            {this.state.loading === "delete" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"></span>
                                <span className="sr-only">Deleting...</span>
                              </>
                            ) : (
                              "Delete"
                            )}
                          </button>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}>
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}>
                            {this.state.loading === "edit" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      )
                    ) : undefined}
                  </div>
                </Form>
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default QCForm;
