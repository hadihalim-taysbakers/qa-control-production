import React from "react";
import moment from "moment";
import "moment/locale/id";
import axios from "axios";
import config from "../../../config";
import Field from "../../../components/Field";
import * as Validator from "validatorjs";
import Form from "../../../components/Form";
import Label from "../../../components/Label";

class bakingForm extends React.Component {
  constructor(props) {
    super(props);
    this.fields = {
      time_check: {
        name: "time_check",
        id: "time_check",
        label: "Waktu Pengecekan",
        attributeName: "Waktu Pengecekan",
        type: "time",
        rules: ["required"],
      },
      no_lot: {
        name: "no_lot",
        id: "no_lot",
        label: "No.Lot",
        attributeName: "No.Lot",
        type: "text",
        rules: ["required"],
      },
      mouisture: {
        name: "mouisture",
        id: "mouisture",
        label: "Mouisture",
        attributeName: "Mouisture",
        type: "number",
        min: 0,
        max: 100,
        step: 1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      weight_left: {
        name: "weight_left",
        id: "weight_left",
        label: "Berat kiri",
        attributeName: "Berat kiri",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      weight_center: {
        name: "weight_center",
        id: "weight_center",
        label: "Berat Tengah",
        attributeName: "Berat Tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      weight_right: {
        name: "weight_right",
        id: "weight_right",
        label: "Berat Kanan",
        attributeName: "Berat Kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      diameter_left: {
        name: "diameter_left",
        id: "diameter_left",
        label: "Diameter Kiri",
        attributeName: "Diameter Kiri",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      diameter_center: {
        name: "diameter_center",
        id: "diameter_center",
        label: "Diameter Tengah",
        attributeName: "Diameter Tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      diameter_right: {
        name: "diameter_right",
        id: "diameter_right",
        label: "Diameter Kanan",
        attributeName: "Diameter Kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      length_left: {
        name: "length_left",
        id: "length_left",
        label: "Panjang Kiri",
        attributeName: "Panjang Kiri",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      length_center: {
        name: "length_center",
        id: "length_center",
        label: "Panjang Tengah",
        attributeName: "Panjang Tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      length_right: {
        name: "length_right",
        id: "length_right",
        label: "Panjang Kanan",
        attributeName: "Panjang Kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      wide_left: {
        name: "wide_left",
        id: "wide_left",
        label: "Lebar Kiri",
        attributeName: "Lebar Kiri",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      wide_center: {
        name: "wide_center",
        id: "wide_center",
        label: "Lebar Tengah",
        attributeName: "Lebar Tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      wide_right: {
        name: "wide_right",
        id: "wide_right",
        label: "Lebar Kanan",
        attributeName: "Lebar Kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      stack_left: {
        name: "stack_left",
        id: "stack_left",
        label: "Tinggi Kiri",
        attributeName: "Tinggi Kiri",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      stack_center: {
        name: "stack_center",
        id: "stack_center",
        label: "Tinggi Tengah",
        attributeName: "Tinggi Tengah",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      stack_right: {
        name: "stack_right",
        id: "stack_right",
        label: "Tinggi Kanan",
        attributeName: "Tinggi Kanan",
        type: "number",
        min: 0,
        max: 100,
        step: 0.1,
        rules: ["required", "numeric", "max:100", "min:0"],
      },
      bentuk_potongan: {
        name: "bentuk_potongan",
        id: "bentuk_potongan",
        label: "Bentuk Potongan",
        attributeName: "Bentuk Potongan",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      color_vs_qas: {
        name: "color_vs_qas",
        id: "color_vs_qas",
        label: " Color VS QAS",
        attributeName: "Color VS QAS",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      tampilan_seluruh: {
        name: "tampilan_seluruh",
        id: "tampilan_seluruh",
        label: "Tampilan Keseluruhan",
        attributeName: "Tampilan Keseluruhan",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      status: {
        name: "status",
        id: "status",
        label: "Status",
        attributeName: "Status",
        type: "checkbox",
        values: { 0: "0", 1: "1" },
      },
      tindakan: {
        name: "tindakan",
        id: "tindakan",
        label: "Corrective Actions",
        attributeName: "Corrective Actions",
        type: "text",
        rules: ["required"],
      },
    };
    this.state = {
      loading: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  getFields() {
    const fields = { ...this.fields };
    const header = { ...this.props.header };
    if (header.min_weight_baking <= 0 && header.max_weight_baking <= 0) {
      delete fields.weight_left;
      delete fields.weight_center;
      delete fields.weight_right;
    }
    if (header.min_diameter_baking <= 0 && header.min_diameter_baking <= 0) {
      delete fields.diameter_left;
      delete fields.diameter_center;
      delete fields.diameter_right;
    }
    if (header.min_length_baking <= 0 && header.max_length_baking <= 0) {
      delete fields.length_left;
      delete fields.length_center;
      delete fields.length_right;
    }

    if (header.min_wide_baking <= 0 && header.max_wide_baking <= 0) {
      delete fields.wide_left;
      delete fields.wide_center;
      delete fields.wide_right;
    }

    if (header.min_height_baking <= 0 && header.max_height_baking <= 0) {
      delete fields.stack_left;
      delete fields.stack_center;
      delete fields.stack_right;
    }

    return fields;
  }
  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState(
      {
        datas: { ...this.state.datas, [name]: value },
      },
      () => {
        const valid = this.validateField(name, value);
        if (valid.valid === true) {
          this.setState({
            errors: { ...this.state.errors, [name]: [] },
          });
        } else {
          this.setState({
            errors: { ...this.state.errors, [name]: valid.errors[name] },
          });
        }
      }
    );
  }
  validateField(name, value) {
    const messages = Validator.getMessages("id");
    Validator.setMessages("id", messages);
    Validator.setAttributeNames(this.attributeNames);
    const validation = new Validator(
      { [name]: value },
      { [name]: this.rules[name] },
      messages
    );
    if (validation.passes()) {
      return { valid: true, errors: null };
    } else {
      const errors = validation.errors.all();
      return {
        valid: false,
        errors: errors,
      };
    }
  }
  InsertRecord(datas) {
    let formData = new FormData();
    for (const key in this.props.header) {
      formData.append(key, this.props.header[key]);
    }
    for (const key in datas) {
      formData.append(key, datas[key]);
    }
    this.setState({ loading: "insert" }, () => {
      axios({
        method: "post",
        url: config.api + config.path.baking,
        data: formData,
        params: {
          tanggal: this.props.header.tanggal,
          shiftr: this.props.header.shiftr,
          mesin: this.props.header.mesin,
          type_produksi: this.props.header.typeprod,
        },
        config: { headers: { "Content-Type": "multipart/form-data" } },
      })
        .then(
          function (response) {
            this.setState({ loading: false }, () => {
              this.props.onAfterInsert(response.data.datas);
              alert("Berhasil untuk menyimpan data");
            });
          }.bind(this)
        )
        .catch((e) => {
          this.setState({ loading: false });

          alert("Gagal untuk menyimpan data");
        });
    });
  }
  DeleteRecord() {
    if (this.props.data && this.props.data.id_baking_detail) {
      this.setState({ loading: "delete" }, () => {
        axios
          .delete(config.api + config.path.baking, {
            params: {
              id: this.props.data.id_baking_detail,
            },
          })
          .then((res) => {
            if (res.data.success) {
              this.setState({ loading: false }, () => {
                this.props.onAfterDelete(res.data.id_baking_detail);
                alert("Berhasil untuk menghapus data");
              });
            } else {
              alert("gagal delete");
            }
          });
      });
    }
  }
  EditRecord(datas) {
    if (this.props.data) {
      let formData = new FormData();
      for (const key in this.props.header) {
        formData.append(key, this.props.header[key]);
      }
      for (const key in datas) {
        formData.append(key, datas[key]);
      }

      this.setState({ loading: "edit" }, () => {
        axios
          .patch(config.api + config.path.baking, formData, {
            params: {
              id: this.props.data.id_baking_detail,
            },
          })
          .then(
            function (response) {
              this.setState({ loading: false }, () => {
                this.props.onAfterEdit(response.data.data);
                alert("Berhasil untuk merubah data");
              });
            }.bind(this)
          )
          .catch((e) => {
            this.setState({ loading: false });
            alert("Gagal untuk menyimpan data");
          });
      });
    }
  }
  onSubmit(datas) {
    if (this.props.data) {
      this.EditRecord(datas);
    } else {
      this.InsertRecord(datas);
    }
  }
  render() {
    const fields = this.getFields();
    return (
      <div
        className={this.props.show ? "modal fade show" : "modal"}
        style={this.props.show ? { display: "block" } : undefined}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            {this.props.data === undefined ? undefined : (
              <>
                <Form
                  fields={fields}
                  onSubmit={this.onSubmit}
                  loading={this.state.loading}
                  defaultValues={this.props.data ? this.props.data : {}}
                >
                  <div className="modal-header">
                    <h5 className="modal-title">
                      {this.props.data !== undefined
                        ? this.props.data &&
                          this.props.data.id_baking_detail === null
                          ? "EDIT"
                          : "INSERT"
                        : ""}
                      POST BAKING PRODUCT CHECK
                    </h5>
                    {this.props.onClose ? (
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        onClick={this.props.onClose}
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    ) : undefined}
                  </div>
                  <div className="modal-body">
                    <div className="row mb-3">
                      <div className="col-sm-3">
                        <label htmlFor="tanggal" className="form-label">
                          Tanggal :
                        </label>
                        <br />
                        <b>
                          {moment(this.props.header.tanggal)
                            .locale("id")
                            .format("dddd, DD MMM YYYY")}
                        </b>
                      </div>

                      <div className="col-sm-3">
                        <label htmlFor="tanggal" className="form-label">
                          Shift :
                        </label>
                        <br />
                        <b>{this.props.header.shiftLabel}</b>
                      </div>
                      <div className="col-sm-3">
                        <label htmlFor="mesinLabel" className="form-label">
                          Mesin :
                        </label>
                        <br />
                        <b>{this.props.header.mesinLabel}</b>
                      </div>
                      <div className="col-sm-3">
                        <label htmlFor="typeprodLabel" className="form-label">
                          Type Production :
                        </label>
                        <br />
                        <b>{this.props.header.typeprodLabel}</b>
                      </div>
                    </div>
                    <hr />
                    <div className="row mb-3">
                      <Label
                        name="time_check"
                        className="col-sm-3 col-form-label"
                      />
                      <div className="col-sm-9">
                        <Field name="time_check" className="form-control" />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <Label
                        name="no_lot"
                        className="col-sm-3 col-form-label"
                      />
                      <div className="col-sm-9">
                        <Field name="no_lot" className="form-control" />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <Label
                        name="mouisture"
                        className="col-sm-3 col-form-label"
                      />
                      <div className="col-sm-9">
                        <Field name="mouisture" className="form-control" />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col-sm-12">
                        <table className="table table-bordered table-hovered table-striped table-hover baking">
                          <thead>
                            <tr>
                              <th></th>
                              <th>L</th>
                              <th>C</th>
                              <th>R</th>
                            </tr>
                          </thead>
                          <tbody>
                            {fields.weight_left ||
                            fields.weight_center ||
                            fields.weight_right ? (
                              <tr>
                                <th>Weight After Oven (gr)</th>
                                <td>
                                  <Field
                                    name="weight_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="weight_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="weight_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            {fields.diameter_left ||
                            fields.diameter_center ||
                            fields.diameter_right ? (
                              <tr>
                                <th>Diameter (mm)</th>
                                <td>
                                  <Field
                                    name="diameter_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="diameter_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="diameter_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            {fields.length_left ||
                            fields.length_center ||
                            fields.length_right ? (
                              <tr>
                                <th>Length (mm)</th>
                                <td>
                                  <Field
                                    name="length_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="length_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="length_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            {fields.wide_left ||
                            fields.wide_center ||
                            fields.length_right ? (
                              <tr>
                                <th>Wide (mm)</th>
                                <td>
                                  <Field
                                    name="wide_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="wide_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="wide_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                            {fields.stack_left ||
                            fields.stack_center ||
                            fields.stack_right ? (
                              <tr>
                                <th>Stack Height (mm)</th>
                                <td>
                                  <Field
                                    name="stack_left"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="stack_center"
                                    className="form-control"
                                  />
                                </td>
                                <td>
                                  <Field
                                    name="stack_right"
                                    className="form-control"
                                  />
                                </td>
                              </tr>
                            ) : undefined}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <div className="col-sm-3">
                        <Label
                          name="bentuk_potongan"
                          className="col-form-label"
                        />
                        <br />
                        <Field
                          name="bentuk_potongan"
                          className="form-check-input"
                        />
                      </div>
                      <div className="col-sm-3">
                        <Label name="color_vs_qas" className="col-form-label" />
                        <br />
                        <Field
                          name="color_vs_qas"
                          className="form-check-input"
                        />
                      </div>
                      <div className="col-sm-3">
                        <Label
                          name="tampilan_seluruh"
                          className="col-form-label"
                        />
                        <br />
                        <Field
                          name="tampilan_seluruh"
                          className="form-check-input"
                        />
                      </div>
                      <div className="col-sm-3">
                        <Label name="status" className="col-form-label" />
                        <br />
                        <Field name="status" className="form-check-input" />
                      </div>
                    </div>

                    <div className="row mb-3">
                      <Label
                        name="tindakan"
                        className="col-sm-3 col-form-label"
                      />
                      <div className="col-sm-9">
                        <Field name="tindakan" className="form-control" />
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer">
                    {this.props.onClose ? (
                      <button
                        type="button"
                        className="btn btn-outline-secondary"
                        data-bs-dismiss="modal"
                        onClick={this.props.onClose}
                        disabled={this.state.loading}
                      >
                        Close
                      </button>
                    ) : undefined}
                    {this.props.data !== undefined ? (
                      this.props.data === null ? (
                        <>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}
                          >
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}
                          >
                            {this.state.loading === "insert" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      ) : (
                        <>
                          <button
                            className="btn btn-danger"
                            type="button"
                            disabled={this.state.loading}
                            onClick={(e) => {
                              e.preventDefault();
                              this.DeleteRecord();
                            }}
                          >
                            {this.state.loading === "delete" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Deleting...</span>
                              </>
                            ) : (
                              "Delete"
                            )}
                          </button>
                          <button
                            type="reset"
                            className="btn btn-outline-warning"
                            disabled={this.state.loading}
                          >
                            Reset
                          </button>
                          <button
                            className="btn btn-primary"
                            type="submit"
                            disabled={this.state.loading}
                          >
                            {this.state.loading === "edit" ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Saving...</span>
                              </>
                            ) : (
                              "Save"
                            )}
                          </button>
                        </>
                      )
                    ) : undefined}
                  </div>
                </Form>
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default bakingForm;