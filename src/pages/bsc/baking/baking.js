import React from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import Select from "react-select";
import Approval from "../../../Approval";
import { formatTime } from "../../../utils/formatter";
import config from "../../../config";
import BakingForm from "./bakingForm";
import Form, { FormContext } from "../../../components/Form";
import Main from "../../../layout/main";
import Field from "../../../components/Field";
import Label from "../../../components/Label";
import BakingGraph from "./GraphBaking";

class baking extends React.Component {
  refForm = React.createRef();
  fields = {
    min_weight_baking: {
      name: "min_weight_baking",
      id: "min_weight_baking",
      label: "Min",
      attributeName: "Berat minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    max_weight_baking: {
      name: "max_weight_baking",
      id: "max_weight_baking",
      label: "Max",
      attributeName: "Berat maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    min_diameter_baking: {
      name: " min_diameter_baking",
      id: " min_diameter_baking",
      label: "Min",
      attributeName: "Diameter minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    max_diameter_baking: {
      name: " max_diameter_baking",
      id: " max_diameter_baking",
      label: "Max",
      attributeName: "Diameter maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    min_length_baking: {
      name: " min_length_baking",
      id: " min_length_baking",
      label: "Min",
      attributeName: "Panjang minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    max_length_baking: {
      name: "max_length_baking",
      id: "max_length_baking",
      label: "Max",
      attributeName: "Panjang maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    min_wide_baking: {
      name: "min_wide_baking",
      id: "min_wide_baking",
      label: "Min",
      attributeName: "Lebar minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    max_wide_baking: {
      name: "max_wide_baking",
      id: "max_wide_baking",
      label: "Max",
      attributeName: "Lebar maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    min_height_baking: {
      name: "min_height_baking",
      id: "min_height_baking",
      label: "Min",
      attributeName: "Tinggi minimum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
    max_height_baking: {
      name: "max_height_baking",
      id: "max_height_baking",
      label: "Max",
      attributeName: "Tinggi maximum",
      type: "label",
      min: 0,
      max: 100,
      step: 0.1,
      rules: ["required", "numeric", "max:100", "min:0"],
    },
  };
  state = {
    exists: [],
    graph: undefined,
    FormData: undefined,
    approveCreator: undefined,
    approveVerifier: undefined,
    approveChecker: undefined,
    approveKnower: undefined,
    creator_name: undefined,
    creator_title: undefined,
    creator_sign: undefined,
    created: undefined,
    verifier_name: undefined,
    verifier_title: undefined,
    verifier_sign: undefined,
    verified: undefined,
    checker_name: undefined,
    checker_title: undefined,
    checker_sign: undefined,
    checked: undefined,
    knower_name: undefined,
    knower_title: undefined,
    knower_sign: undefined,
    knowed: undefined,
    name: undefined,
    email: undefined,
    edit: undefined,
    loading: false,
    tanggal: "",
    typeprod: "",
    mesin: "",
    shiftr: "",
    no_doc: "FRM.QCRD.BSC.02.03; Rev.02",
    datas: [],
    query: "",
    data: [],
    searchString: [],
    timeCheckOptions: [],
    shiftOptions: [],
    mesinOptions: [],
    productOptions: [],
    aquaticCreatures: [],
  };
  getFields() {
    const fields = { ...this.fields };
    if (
      !(
        this.state.headerData &&
        this.state.headerData.min_weight_baking > 0 &&
        this.state.headerData.max_weight_baking > 0
      )
    ) {
      delete fields.min_weight_baking;
      delete fields.max_weight_baking;
    }

    if (
      !(
        this.state.headerData &&
        this.state.headerData.min_diameter_baking > 0 &&
        this.state.headerData.max_diameter_baking > 0
      )
    ) {
      delete fields.min_diameter_baking;
      delete fields.max_diameter_baking;
    }
    if (
      !(
        this.state.headerData &&
        this.state.headerData.min_length_baking > 0 &&
        this.state.headerData.max_length_baking > 0
      )
    ) {
      delete fields.max_length_baking;
      delete fields.min_length_baking;
    }
    if (
      !(
        this.state.headerData &&
        this.state.headerData.min_wide_baking > 0 &&
        this.state.headerData.max_wide_baking > 0
      )
    ) {
      delete fields.min_wide_baking;
      delete fields.max_wide_baking;
    }
    if (
      !(
        this.state.headerData &&
        this.state.headerData.min_height_baking > 0 &&
        this.state.headerData.max_height_baking > 0
      )
    ) {
      delete fields.min_height_baking;
      delete fields.max_height_baking;
    }
    return fields;
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      (prevState.tanggal !== this.state.tanggal ||
        prevState.shiftr !== this.state.shiftr ||
        prevState.typeprod !== this.state.typeprod ||
        prevState.mesin !== this.state.mesin) &&
      this.state.tanggal &&
      this.state.shiftr &&
      this.state.typeprod &&
      this.state.mesin
    ) {
      const url = config.api + config.path.baking;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            type_produksi: this.state.typeprod,
            mesin: this.state.mesin,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          const headerData = {};
          if (data.header) {
            headerData.min_weight_baking = data.header.min_weight_baking;
            headerData.max_weight_baking = data.header.max_weight_baking;
            headerData.min_diameter_baking = data.header.min_diameter_baking;
            headerData.max_diameter_baking = data.header.max_diameter_baking;
            headerData.min_length_baking = data.header.min_length_baking;
            headerData.max_length_baking = data.header.max_length_baking;
            headerData.min_wide_baking = data.header.min_wide_baking;
            headerData.max_wide_baking = data.header.max_wide_baking;
            headerData.min_height_baking = data.header.min_height_baking;
            headerData.max_height_baking = data.header.max_height_baking;
          } else {
            for (let i = 0; i < this.productTypes.length; i++) {
              if (
                parseInt(this.productTypes[i].id) ===
                parseInt(this.state.typeprod)
              ) {
                headerData.min_weight_baking =
                  this.productTypes[i].min_weight_baking;
                headerData.max_weight_baking =
                  this.productTypes[i].max_weight_baking;
                headerData.min_diameter_baking =
                  this.productTypes[i].min_diameter_baking;
                headerData.max_diameter_baking =
                  this.productTypes[i].max_diameter_baking;
                headerData.min_length_baking =
                  this.productTypes[i].min_length_baking;
                headerData.max_length_baking =
                  this.productTypes[i].max_length_baking;
                headerData.min_wide_baking =
                  this.productTypes[i].min_wide_baking;
                headerData.max_wide_baking =
                  this.productTypes[i].max_wide_baking;
                headerData.min_height_baking =
                  this.productTypes[i].min_height_baking;
                headerData.max_height_baking =
                  this.productTypes[i].max_height_baking;
              }
            }
          }
          this.setState({
            creator_name: data.header ? data.header.creator_name : undefined,
            creator_title: data.header ? data.header.creator_title : undefined,
            creator_sign: data.header ? data.header.creator_sign : undefined,
            created: data.header ? data.header.created : undefined,
            verifier_name: data.header ? data.header.verifier_name : undefined,
            verifier_title: data.header
              ? data.header.verifier_title
              : undefined,
            verifier_sign: data.header ? data.header.verifier_sign : undefined,
            verified: data.header ? data.header.verified : undefined,
            checker_name: data.header ? data.header.checker_name : undefined,
            checker_title: data.header ? data.header.checker_title : undefined,
            checker_sign: data.header ? data.header.checker_sign : undefined,
            checked: data.header ? data.header.checked : undefined,
            knower_name: data.header ? data.header.knower_name : undefined,
            knower_title: data.header ? data.header.knower_title : undefined,
            knower_sign: data.header ? data.header.knower_sign : undefined,
            knowed: data.header ? data.header.knowed : undefined,
            loading: false,
            datas: data.datas,
            operator: data.header ? data.header.operator : "",
            headerData: headerData,
          });
        });
    } else if (
      prevState.typeprod !== this.state.typeprod &&
      this.state.typeprod
    ) {
      for (let i = 0; i < this.productTypes.length; i++) {
        if (
          parseInt(this.productTypes[i].id) === parseInt(this.state.typeprod)
        ) {
          const min_weight_baking = this.productTypes[i].min_weight_baking;
          const max_weight_baking = this.productTypes[i].max_weight_baking;
          const min_diameter_baking = this.productTypes[i].min_diameter_baking;
          const max_diameter_baking = this.productTypes[i].max_diameter_baking;
          const min_length_baking = this.productTypes[i].min_length_baking;
          const max_length_baking = this.productTypes[i].max_length_baking;
          const min_wide_baking = this.productTypes[i].min_wide_baking;
          const max_wide_baking = this.productTypes[i].max_wide_baking;
          const min_height_baking = this.productTypes[i].min_height_baking;
          const max_height_baking = this.productTypes[i].max_height_baking;
          this.setState({
            min_weight_baking: min_weight_baking,
            max_weight_baking: max_weight_baking,
            min_diameter_baking: min_diameter_baking,
            max_diameter_baking: max_diameter_baking,
            min_length_baking: min_length_baking,
            max_length_baking: max_length_baking,
            min_wide_baking: min_wide_baking,
            max_wide_baking: max_wide_baking,
            min_height_baking: min_height_baking,
            max_height_baking: max_height_baking,
          });
        }
      }
    }

    if (prevState.tanggal !== this.state.tanggal && this.state.tanggal) {
      const url = config.api + config.path.baking;
      axios
        .get(url, {
          params: {
            tanggal: this.state.tanggal,
          },
        })
        .then((response) => response.data)
        .then((data) => {
          this.setState({
            exists: data.datas,
          });
        });
    }
  }
  submitHeaderChange(datas) {
    if (
      this.state.tanggal &&
      this.state.shiftr &&
      this.state.typeprod &&
      this.state.mesin
    ) {
      this.setState({ loading: true }, () => {
        const formData = new FormData();
        for (const key in datas) {
          formData.append(key, datas[key]);
        }
        formData.append("no_doc", this.state.no_doc);
        axios
          .patch(config.api + config.path.baking, formData, {
            "Content-Type": "application/x-www-form-urlencoded",
            params: {
              tanggal: this.state.tanggal,
              shiftr: this.state.shiftr,
              mesin: this.state.mesin,
              type_produksi: this.state.typeprod,
            },
          })
          .then((res) => {
            const headerData = { ...this.state.headerData };
            for (const key in datas) {
              headerData[key] = datas[key];
            }
            this.setState({
              loading: false,
              headerData: headerData,
            });
          })
          .catch(() => {
            this.setState({
              loading: false,
            });
            alert("Gagal insert data");
          });
      });
    }
  }
  componentDidMount() {
    const yourDate = new Date();
    this.setState({ tanggal: yourDate.toISOString().split("T")[0] });
  }

  async getOptions() {
    const resShift = await axios.get(config.api + config.path.master_shift);
    const dataShift = resShift.data;
    const shiftOptions = dataShift.map((d) => ({
      value: d.id,
      label: d.judul,
    }));
    this.setState({ shiftOptions: shiftOptions });
    const resMesin = await axios.get(config.api + config.path.master_mesin, {
      params: { posisi: config.path.baking.posisi },
    });
    const dataMesin = resMesin.data;
    const mesinOptions = dataMesin.map((d) => ({
      value: d.id,
      label: d.name,
    }));
    this.setState({ mesinOptions: mesinOptions });
    const resProduct = await axios.get(config.api + config.path.master_product);
    const dataProduct = resProduct.data;
    const productOptions = dataProduct.map((d) => ({
      value: d.id,
      label: d.product_name,
    }));
    this.productTypes = [...dataProduct];
    this.setState({ productOptions: productOptions });
  }

  componentWillMount() {
    this.getOptions();
  }

  setEdit(ind) {
    this.setState({
      formData: this.state.datas[ind],
    });
  }

  submitCreatorApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("creator_name", data.name);
      formData.append("creator_title", data.title);
      formData.append("creator_sign", data.sign);
      axios
        .patch(config.api + config.path.baking, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveCreator: undefined,
            creator_name: res.data.header.creator_name,
            creator_title: res.data.header.creator_title,
            creator_sign: res.data.header.creator_sign,
            created: res.data.header.created,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveCreator: undefined,
            creator_name: undefined,
            creator_title: undefined,
            creator_sign: undefined,
            created: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitVerifierApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("verifier_name", data.name);
      formData.append("verifier_title", data.title);
      formData.append("verifier_sign", data.sign);
      axios
        .patch(config.api + config.path.baking, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: res.data.header.verifier_name,
            verifier_title: res.data.header.verifier_title,
            verifier_sign: res.data.header.verifier_sign,
            verified: res.data.header.verified,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveVerifier: undefined,
            verifier_name: undefined,
            verifier_title: undefined,
            verifier_sign: undefined,
            verified: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitCheckerApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("checker_name", data.name);
      formData.append("checker_title", data.title);
      formData.append("checker_sign", data.sign);
      axios
        .patch(config.api + config.path.baking, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveChecker: undefined,
            checker_name: res.data.header.checker_name,
            checker_title: res.data.header.checker_title,
            checker_sign: res.data.header.checker_sign,
            checked: res.data.header.checked,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveCreator: undefined,
            checker_name: undefined,
            checker_title: undefined,
            checker_sign: undefined,
            checked: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  submitKnowerApproval(data) {
    this.setState({ loading: true }, () => {
      const formData = new FormData();
      formData.append("knower_name", data.name);
      formData.append("knower_title", data.title);
      formData.append("knower_sign", data.sign);
      axios
        .patch(config.api + config.path.baking, formData, {
          params: {
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            mesin: this.state.mesin,
            type_produksi: this.state.typeprod,
          },
        })
        .then((res) => {
          this.setState({
            approveKnower: undefined,
            knower_name: res.data.header.knower_name,
            knower_title: res.data.header.knower_title,
            knower_sign: res.data.header.knower_sign,
            knowed: res.data.header.knowed,
            loading: false,
          });
        })
        .catch(() => {
          this.setState({
            approveKnower: undefined,
            knower_name: undefined,
            knower_title: undefined,
            knower_sign: undefined,
            knowed: undefined,
            loading: false,
          });
          alert("Gagal approve data");
        });
    });
  }
  getProductOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.typeprod) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
            parseInt(exists[j]["type_produksi"]) ===
              parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getProductOptions() {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
          parseInt(exists[j]["mesin"]) === parseInt(this.state.mesin) &&
          parseInt(exists[j]["type_produksi"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getMesinName(id) {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getMesinOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.mesin) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
            parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getMesinOptions() {
    const options = JSON.parse(JSON.stringify(this.state.mesinOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));

    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(this.state.shiftr) &&
          parseInt(exists[j]["mesin"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getShiftOptionValue() {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    let selected = undefined;
    for (let i = 0; i < options.length; i++) {
      if (parseInt(this.state.shiftr) === parseInt(options[i].value)) {
        selected = options[i];
        for (let j = 0; j < exists.length; j++) {
          if (
            parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
            parseInt(exists[j]["qty"]) > 0
          ) {
            selected.label = <b>{selected.label}</b>;
          }
        }
      }
    }
    return selected;
  }
  getProductName(id) {
    const options = JSON.parse(JSON.stringify(this.state.productOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getShiftName(id) {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    for (let i = 0; i < options.length; i++) {
      if (options[i].value === id) {
        return options[i].label;
      }
    }
    return "";
  }
  getShiftOptions() {
    const options = JSON.parse(JSON.stringify(this.state.shiftOptions));
    const exists = JSON.parse(JSON.stringify(this.state.exists));
    for (let i = 0; i < options.length; i++) {
      for (let j = 0; j < exists.length; j++) {
        if (
          parseInt(exists[j]["shift"]) === parseInt(options[i].value) &&
          parseInt(exists[j]["qty"]) > 0
        ) {
          options[i].label = <b>{options[i].label}</b>;
        }
      }
    }
    return options;
  }
  getGraphData(fields, min, max, title) {
    const labels = [];
    const dataMin = [];
    const dataMax = [];
    const datas = {};
    for (const key in fields) {
      datas[key] = [];
    }
    for (var i = 0; i < this.state.datas.length; i++) {
      for (const key in fields) {
        if (this.state.datas[i][key] > 0) {
          datas[key].push(this.state.datas[i][key]);
        } else {
          datas[key].push(null);
        }
      }

      labels.push(this.state.datas[i].time_check);
      dataMin.push(min);
      dataMax.push(max);
    }
    const datasets = [];
    for (const key in fields) {
      datasets.push({
        label: fields[key].label,
        data: datas[key],
        borderColor: fields[key].color,
      });
    }

    return {
      title: title,
      data: {
        labels: labels,
        datasets: [
          ...datasets,
          {
            label: "Over",
            fill: "end",
            data: dataMax,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
          },
          {
            label: "Under",
            data: dataMin,
            borderColor: "#721c24",
            backgroundColor: "#f8d7da",
            pointStyle: "line",
            borderWidth: 1,
            fill: "start",
          },
        ],
      },
    };
  }
  removeExistData(typeprod, mesin, shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (
        exists[i].type_produksi === typeprod &&
        exists[i].mesin === mesin &&
        exists[i].shift === shift
      ) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) - 1;
      }
    }
    if (isExist === false) {
      exists.push({
        type_produksi: typeprod,
        mesin: mesin,
        shift: shift,
        qty: "0",
      });
    }
    return exists;
  }
  addExistData(typeprod, mesin, shift) {
    const exists = [...this.state.exists];
    let isExist = false;
    for (let i = 0; i < exists.length; i++) {
      if (
        exists[i].type_produksi === typeprod &&
        exists[i].mesin === mesin &&
        exists[i].shift === shift
      ) {
        isExist = true;
        exists[i].qty = parseInt(exists[i].qty) + 1;
      }
    }
    if (isExist === false) {
      exists.push({
        type_produksi: typeprod,
        mesin: mesin,
        shift: shift,
        qty: "1",
      });
    }
    return exists;
  }
  getProductType() {
    if (this.productTypes) {
      for (let i = 0; i < this.productTypes.length; i++) {
        if (
          parseInt(this.productTypes[i].id) === parseInt(this.state.typeprod)
        ) {
          return this.productTypes[i];
        }
      }
    }
    return null;
  }
  render() {
    const productType = this.getProductType();
    const min_diameter_baking = productType
      ? parseFloat(productType.min_diameter_baking)
      : 0;
    const max_diameter_baking = productType
      ? parseFloat(productType.max_diameter_baking)
      : 0;
    const min_weight_baking = productType
      ? parseFloat(productType.min_weight_baking)
      : 0;
    const max_weight_baking = productType
      ? parseFloat(productType.max_weight_baking)
      : 0;
    const min_length_baking = productType
      ? parseFloat(productType.min_length_baking)
      : 0;
    const max_length_baking = productType
      ? parseFloat(productType.max_length_baking)
      : 0;
    const min_wide_baking = productType
      ? parseFloat(productType.min_wide_baking)
      : 0;
    const max_wide_baking = productType
      ? parseFloat(productType.max_wide_baking)
      : 0;
    const min_height_baking = productType
      ? parseFloat(productType.min_height_baking)
      : 0;
    const max_height_baking = productType
      ? parseFloat(productType.max_height_baking)
      : 0;

    const hd_min_diameter_baking = this.state.headerData
      ? parseFloat(this.state.headerData.min_diameter_baking)
      : 0;
    const hd_max_diameter_baking = this.state.headerData
      ? parseFloat(this.state.headerData.max_diameter_baking)
      : 0;
    const hd_min_weight_baking = this.state.headerData
      ? parseFloat(this.state.headerData.min_weight_baking)
      : 0;
    const hd_max_weight_baking = this.state.headerData
      ? parseFloat(this.state.headerData.max_weight_baking)
      : 0;
    const hd_min_length_baking = this.state.headerData
      ? parseFloat(this.state.headerData.min_length_baking)
      : 0;
    const hd_max_length_baking = this.state.headerData
      ? parseFloat(this.state.headerData.max_length_baking)
      : 0;
    const hd_min_wide_baking = this.state.headerData
      ? parseFloat(this.state.headerData.min_wide_baking)
      : 0;
    const hd_max_wide_baking = this.state.headerData
      ? parseFloat(this.state.headerData.max_wide_baking)
      : 0;
    const hd_min_height_baking = this.state.headerData
      ? parseFloat(this.state.headerData.min_height_baking)
      : 0;
    const hd_max_height_baking = this.state.headerData
      ? parseFloat(this.state.headerData.max_height_baking)
      : 0;

    let colspannum = 30;
    if (min_weight_baking > 0 && max_weight_baking > 0)
      colspannum = colspannum + 3;
    if (min_diameter_baking > 0 && max_diameter_baking > 0)
      colspannum = colspannum + 3;
    if (min_length_baking > 0 && max_length_baking > 0)
      colspannum = colspannum + 3;
    if (min_wide_baking > 0 && max_wide_baking > 0) colspannum = colspannum + 3;
    if (min_height_baking > 0 && max_height_baking > 0)
      colspannum = colspannum + 3;
    return (
      <>
        <Main>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm">
                <h4 className="page-header text-center">
                  POST BAKING PRODUCT CHECK
                </h4>
                <h6 className="page-header text-center">
                  PEMERIKSAAN HASIL PRODUKSI OVEN
                </h6>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-2 form-group">
                <label htmlFor="tanggal">Date :</label>
                <input
                  disabled={this.state.loading}
                  required
                  type="date"
                  name="tanggal"
                  id="tanggal"
                  className="form-control"
                  value={this.state.tanggal}
                  onChange={(e) => this.setState({ tanggal: e.target.value })}
                />
              </div>
              <div className="col-sm-2 form-group">
                <label htmlFor="shiftr">Shift :</label>
                <Select
                  isDisabled={this.state.loading}
                  name="shiftr"
                  id="shiftr"
                  options={this.getShiftOptions()}
                  value={this.getShiftOptionValue()}
                  onChange={(e) => {
                    this.setState({ shiftr: e.value });
                  }}
                />
              </div>
              <div className="col-sm-4 form-group">
                <label htmlFor="shiftr">Mesin :</label>
                <Select
                  isDisabled={this.state.loading}
                  name="mesin"
                  id="mesin"
                  options={this.getMesinOptions()}
                  value={this.getMesinOptionValue()}
                  onChange={(e) => {
                    this.setState({ mesin: e.value });
                  }}
                />
              </div>
              <div className="col-sm-4 form-group">
                <label htmlFor="typeprod">Type Production</label>
                <Select
                  isDisabled={this.state.loading}
                  name="typeprod"
                  id="typeprod"
                  options={this.getProductOptions()}
                  value={this.getProductOptionValue()}
                  onChange={(e) => {
                    this.setState({ typeprod: e.value });
                  }}
                />
              </div>
            </div>

            {this.state.tanggal &&
            this.state.typeprod &&
            this.state.mesin &&
            this.state.shiftr ? (
              <>
                <div className="row">
                  <div className="col-sm-4">
                    <font size="2">
                      Pengambilan sample setiap 30 Menit, sebanyak 10 pcs per
                      sisi
                    </font>
                  </div>
                  <div className="col-sm-4 text-center">
                    <font size="2">Note : L : Left, C : Center & R: Right</font>
                  </div>
                  <div className="col-sm-4 text-right">
                    <font size="2">No. Dok :{this.state.no_doc}</font>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12">
                    {this.fields ? (
                      <Form
                        fields={this.getFields()}
                        defaultValues={this.state.headerData}
                        loading={this.state.loading}
                        onSubmit={(data) => {
                          this.submitHeaderChange(data);
                        }}>
                        <FormContext.Consumer>
                          {({ submit, validate, errors }) => (
                            <table className="table data table-bordered table-hovered table-striped table-hover baking">
                              <thead>
                                <tr>
                                  <th colSpan="2" className="title">
                                    Target
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Moisture (&lt; 1%)</span>
                                  </th>

                                  {min_weight_baking > 0 &&
                                  max_weight_baking > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_weight_baking" />
                                          <Field
                                            name="min_weight_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_weight_baking" />
                                          <Field
                                            name="max_weight_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  {min_diameter_baking > 0 &&
                                  max_diameter_baking > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_diameter_baking" />
                                          <Field
                                            name="min_diameter_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_diameter_baking" />
                                          <Field
                                            name="max_diameter_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  {min_length_baking > 0 &&
                                  max_length_baking > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_length_baking" />
                                          <Field
                                            name="min_length_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_length_baking" />
                                          <Field
                                            name="max_length_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}

                                  {min_wide_baking > 0 &&
                                  max_wide_baking > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_wide_baking" />
                                          <Field
                                            name="min_wide_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_wide_baking" />
                                          <Field
                                            name="max_wide_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}
                                  {min_height_baking > 0 &&
                                  max_height_baking > 0 ? (
                                    <th colSpan="3">
                                      <div className="row">
                                        <div className="col-sm-6 form-group">
                                          <Label name="min_height_baking" />
                                          <Field
                                            name="min_height_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                        <div className="col-sm-6 form-group">
                                          <Label name="max_height_baking" />
                                          <Field
                                            name="max_height_baking"
                                            className="form-control-plaintext"
                                            onChange={(e) => {
                                              submit();
                                            }}
                                          />
                                        </div>
                                      </div>
                                    </th>
                                  ) : undefined}

                                  <th rowSpan="3" className="vertical">
                                    <span>Bentuk Potongan (√/x)</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Color VS QAS (√/x)</span>
                                  </th>
                                  <th rowSpan="3" className="vertical">
                                    <span>Tampilan keseluruhan (√/x)</span>
                                  </th>

                                  <th rowSpan="3" className="vertical">
                                    <span>Status (√/x)</span>
                                  </th>
                                  <th rowSpan="3">Corrective Actions</th>
                                </tr>
                                <tr>
                                  <th rowSpan="2" className="title">
                                    Time Check
                                  </th>
                                  <th rowSpan="2" className="title">
                                    No. Lot
                                  </th>
                                  {min_weight_baking > 0 &&
                                  max_weight_baking > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{ fontWeight: "bold" }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                weight_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                weight_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                weight_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData
                                                .min_weight_baking,
                                              this.state.headerData
                                                .max_weight_baking,
                                              "Weight Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Weight
                                        <br />
                                        (gr)
                                      </button>
                                    </th>
                                  ) : undefined}
                                  {min_diameter_baking > 0 &&
                                  max_diameter_baking > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                diameter_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                diameter_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                diameter_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData
                                                .min_diameter_baking,
                                              this.state.headerData
                                                .max_diameter_baking,
                                              "Diameter Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Diameter
                                        <br />
                                        (mm)
                                      </button>
                                    </th>
                                  ) : undefined}

                                  {min_length_baking > 0 &&
                                  max_length_baking > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                length_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                length_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                length_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData
                                                .min_length_baking,
                                              this.state.headerData
                                                .max_length_baking,
                                              "Length Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Length <br /> (mm)
                                      </button>
                                    </th>
                                  ) : undefined}
                                  {min_wide_baking > 0 &&
                                  max_wide_baking > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                wide_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                wide_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                wide_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData
                                                .min_wide_baking,
                                              this.state.headerData
                                                .max_wide_baking,
                                              "Wide Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Wide <br /> (mm)
                                      </button>
                                    </th>
                                  ) : undefined}
                                  {min_height_baking > 0 &&
                                  max_height_baking > 0 ? (
                                    <th colSpan="3">
                                      <button
                                        style={{
                                          fontWeight: "bold",
                                          whiteSpace: "nowrap",
                                        }}
                                        type="button"
                                        className="btn btn-outline-light btn-sm"
                                        onClick={() => {
                                          this.setState({
                                            graph: this.getGraphData(
                                              {
                                                stack_left: {
                                                  label: "Left",
                                                  color: "#28a745",
                                                },
                                                stack_center: {
                                                  label: "Center",
                                                  color: "#007bff",
                                                },
                                                stack_right: {
                                                  label: "Right",
                                                  color: "#ffc107",
                                                },
                                              },
                                              this.state.headerData
                                                .min_height_baking,
                                              this.state.headerData
                                                .max_height_baking,
                                              "Stack Graph"
                                            ),
                                          });
                                        }}>
                                        &#128201; Stack Height <br /> (mm)
                                      </button>
                                    </th>
                                  ) : undefined}
                                </tr>

                                <tr>
                                  {min_weight_baking > 0 &&
                                  max_weight_baking > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  {min_diameter_baking > 0 &&
                                  max_diameter_baking > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  {min_length_baking > 0 &&
                                  max_length_baking > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  {min_wide_baking > 0 &&
                                  max_wide_baking > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                  {min_height_baking > 0 &&
                                  max_height_baking > 0 ? (
                                    <>
                                      <th scope="col">L</th>
                                      <th scope="col">C</th>
                                      <th scope="col">R</th>
                                    </>
                                  ) : undefined}
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.datas.map((baking, index) => {
                                  const weight_left = parseFloat(
                                    baking.weight_left
                                  );
                                  const weight_center = parseFloat(
                                    baking.weight_center
                                  );
                                  const weight_right = parseFloat(
                                    baking.weight_right
                                  );
                                  const diameter_left = parseFloat(
                                    baking.diameter_left
                                  );
                                  const diameter_center = parseFloat(
                                    baking.diameter_center
                                  );
                                  const diameter_right = parseFloat(
                                    baking.diameter_right
                                  );
                                  const length_left = parseFloat(
                                    baking.length_left
                                  );
                                  const length_center = parseFloat(
                                    baking.length_center
                                  );
                                  const length_right = parseFloat(
                                    baking.length_right
                                  );
                                  const wide_left = parseFloat(
                                    baking.wide_left
                                  );
                                  const wide_center = parseFloat(
                                    baking.wide_center
                                  );
                                  const wide_right = parseFloat(
                                    baking.wide_right
                                  );
                                  const stack_left = parseFloat(
                                    baking.stack_left
                                  );
                                  const stack_center = parseFloat(
                                    baking.stack_center
                                  );
                                  const stack_right = parseFloat(
                                    baking.stack_right
                                  );

                                  return (
                                    <tr key={index}>
                                      <td className="action">
                                        {!this.state.verifier_name ? (
                                          <>
                                            <button
                                              type="button"
                                              className="btn btn-outline-info btn-block btn-sm"
                                              onClick={() => {
                                                this.setEdit(index);
                                              }}>
                                              &#128269;
                                            </button>
                                            {formatTime(baking.time_check)}
                                          </>
                                        ) : (
                                          <>{formatTime(baking.time_check)}</>
                                        )}
                                      </td>
                                      <td>{baking.no_lot}</td>
                                      <td>{baking.mouisture}</td>
                                      {min_weight_baking > 0 &&
                                      max_weight_baking > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                weight_left <
                                                hd_min_weight_baking
                                                  ? "red"
                                                  : weight_left >
                                                    hd_max_weight_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {weight_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                weight_center <
                                                hd_min_weight_baking
                                                  ? "red"
                                                  : weight_center >
                                                    hd_max_weight_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {weight_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                weight_right <
                                                hd_min_weight_baking
                                                  ? "red"
                                                  : weight_right >
                                                    hd_max_weight_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {weight_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_diameter_baking > 0 &&
                                      max_diameter_baking > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                diameter_left <
                                                hd_min_diameter_baking
                                                  ? "red"
                                                  : diameter_left >
                                                    hd_max_diameter_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {diameter_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                diameter_center <
                                                hd_min_diameter_baking
                                                  ? "red"
                                                  : diameter_center >
                                                    hd_max_diameter_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {diameter_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                diameter_right <
                                                hd_min_diameter_baking
                                                  ? "red"
                                                  : diameter_right >
                                                    hd_max_diameter_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {diameter_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {min_length_baking > 0 &&
                                      max_length_baking > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                length_left <
                                                hd_min_length_baking
                                                  ? "red"
                                                  : length_left >
                                                    hd_max_length_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {length_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                length_center <
                                                hd_min_length_baking
                                                  ? "red"
                                                  : length_center >
                                                    hd_max_length_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {length_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                length_right <
                                                hd_min_length_baking
                                                  ? "red"
                                                  : length_right >
                                                    hd_max_length_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {length_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}

                                      {min_wide_baking > 0 &&
                                      max_wide_baking > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                wide_left < hd_min_wide_baking
                                                  ? "red"
                                                  : wide_left >
                                                    hd_max_wide_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {wide_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                wide_center < hd_min_wide_baking
                                                  ? "red"
                                                  : wide_center >
                                                    hd_max_wide_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {wide_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                wide_right < hd_min_wide_baking
                                                  ? "red"
                                                  : wide_right >
                                                    hd_max_wide_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {wide_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}
                                      {max_height_baking > 0 &&
                                      min_height_baking > 0 ? (
                                        <>
                                          <td align="right">
                                            <span
                                              className={
                                                stack_left <
                                                hd_min_height_baking
                                                  ? "red"
                                                  : stack_left >
                                                    hd_max_height_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {stack_left.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                stack_center <
                                                hd_min_height_baking
                                                  ? "red"
                                                  : stack_center >
                                                    hd_max_height_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {stack_center.toFixed(1)}
                                            </span>
                                          </td>
                                          <td align="right">
                                            <span
                                              className={
                                                stack_right <
                                                hd_min_height_baking
                                                  ? "red"
                                                  : stack_right >
                                                    hd_max_height_baking
                                                  ? "green"
                                                  : undefined
                                              }>
                                              {stack_right.toFixed(1)}
                                            </span>
                                          </td>
                                        </>
                                      ) : undefined}

                                      <td align="center">
                                        {baking.bentuk_potongan === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {baking.color_vs_qas === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {baking.tampilan_seluruh === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>
                                      <td align="center">
                                        {baking.status === "1" ? (
                                          <>&#10003;</>
                                        ) : (
                                          <>&#10005;</>
                                        )}
                                      </td>

                                      <td>{baking.tindakan}</td>
                                    </tr>
                                  );
                                })}
                              </tbody>
                              {!this.state.verifier_name ? (
                                <tfoot>
                                  <tr>
                                    <td colSpan={colspannum} align="center">
                                      <button
                                        type="button"
                                        className="btn btn-primary  "
                                        onClick={(e) => {
                                          if (validate()) {
                                            this.setState({
                                              formData: null,
                                            });
                                          } else {
                                            alert(
                                              "Silahkan perbaiki data minimum dan maximum"
                                            );
                                          }
                                        }}
                                        value="Add">
                                        &#10010; Add
                                      </button>
                                    </td>
                                  </tr>
                                </tfoot>
                              ) : undefined}
                            </table>
                          )}
                        </FormContext.Consumer>
                      </Form>
                    ) : undefined}
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6 xs-12"></div>
                  <div className="col-md-6 xs-12">
                    <table className="sign" cellPadding={3}>
                      <tbody>
                        <tr>
                          <th width={"20%"}>&nbsp;</th>
                          <th width={"20%"}> Dibuat Oleh,</th>
                          <th width={"20%"}> Diverifikasi Oleh,</th>
                          <th width={"20%"}> Diperiksa Oleh,</th>
                          <th width={"20%"}>Mengetahui Oleh, </th>
                        </tr>
                        <tr>
                          <th>Tanda Tangan</th>
                          {this.state.creator_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.creator_sign}
                                alt="creator sign"
                                width={"100%"}
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 || this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveCreator: {
                                      name: "",
                                      title: "Operator",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.verifier_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.verifier_sign}
                                width={"100%"}
                                alt="verifier sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveVerifier: {
                                      name: "",
                                      title: "Quality Control",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.checker_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.checker_sign}
                                width={"100%"}
                                alt="checker sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  !this.state.verifier_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveChecker: {
                                      name: "",
                                      title: "Supervisor QC",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                          {this.state.knower_sign ? (
                            <td align="center" valign="middle">
                              <img
                                src={this.state.knower_sign}
                                width={"100%"}
                                alt="knower sign"
                              />
                            </td>
                          ) : (
                            <td rowSpan={4} align="center" valign="middle">
                              <button
                                className="btn btn-primary"
                                disabled={
                                  this.state.datas.length < 1 ||
                                  !this.state.creator_name ||
                                  !this.state.verifier_name ||
                                  !this.state.checker_name ||
                                  this.state.edit
                                }
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.setState({
                                    approveKnower: {
                                      name: "",
                                      title: "Manager QA",
                                      sign: undefined,
                                    },
                                  });
                                }}>
                                Approve
                              </button>
                            </td>
                          )}
                        </tr>
                        <tr>
                          <th>Nama</th>
                          {this.state.creator_name ? (
                            <td align="center" valign="middle">
                              {this.state.creator_name}
                            </td>
                          ) : undefined}
                          {this.state.verifier_name ? (
                            <td align="center" valign="middle">
                              {this.state.verifier_name}
                            </td>
                          ) : undefined}
                          {this.state.checker_name ? (
                            <td align="center" valign="middle">
                              {this.state.checker_name}
                            </td>
                          ) : undefined}
                          {this.state.knower_name ? (
                            <td align="center" valign="middle">
                              {this.state.knower_name}
                            </td>
                          ) : undefined}
                        </tr>
                        <tr>
                          <th>Jabatan</th>
                          {this.state.creator_title ? (
                            <td align="center" valign="middle">
                              {this.state.creator_title}
                            </td>
                          ) : undefined}
                          {this.state.verifier_title ? (
                            <td align="center" valign="middle">
                              {this.state.verifier_title}
                            </td>
                          ) : undefined}
                           {this.state.checker_title ? (
                            <td align="center" valign="middle">
                              {this.state.checker_title}
                            </td>
                          ) : undefined}
                          {this.state.knower_title ? (
                            <td align="center" valign="middle">
                              {this.state.knower_title}
                            </td>
                          ) : undefined}
                        </tr>
                        <tr>
                          <th>Tanggal</th>
                          {this.state.created ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.created).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.verified ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.verified).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.checked ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.verified).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                          {this.state.knowed ? (
                            <td align="center" valign="middle">
                              {new Date(this.state.knowed).toLocaleDateString(
                                "id-ID",
                                {
                                  year: "numeric",
                                  month: "short",
                                  day: "numeric",
                                  hour: "numeric",
                                  minute: "numeric",
                                }
                              )}
                            </td>
                          ) : undefined}
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </>
            ) : undefined}
          </div>
        </Main>
        <Approval
          loading={this.state.loading}
          title="Dibuat oleh,"
          show={this.state.approveCreator ? true : false}
          data={this.state.approveCreator}
          onSubmit={(data) => {
            this.submitCreatorApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveCreator: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Diverifikasi oleh,"
          show={this.state.approveVerifier ? true : false}
          data={this.state.approveVerifier}
          onSubmit={(data) => {
            this.submitVerifierApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveVerifier: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Diperiksa oleh,"
          show={this.state.approveChecker ? true : false}
          data={this.state.approveChecker}
          onSubmit={(data) => {
            this.submitCheckerApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveChecker: undefined,
            });
          }}
        />
        <Approval
          loading={this.state.loading}
          title="Mengetahui oleh,"
          show={this.state.approveKnower ? true : false}
          data={this.state.approveKnower}
          onSubmit={(data) => {
            this.submitKnowerApproval(data);
          }}
          onClose={(e) => {
            e.preventDefault();
            this.setState({
              approveKnower: undefined,
            });
          }}
        />
        <BakingGraph
          title="POST BAKING PRODUCT CHECK"
          tanggal={this.state.tanggal}
          shift={this.getShiftName(this.state.shiftr)}
          mesin={this.getMesinName(this.state.mesin)}
          product={this.getProductName(this.state.typeprod)}
          show={this.state.graph ? true : false}
          data={this.state.graph ? this.state.graph.data : undefined}
          subtitle={this.state.graph ? this.state.graph.title : undefined}
          onClose={() => {
            this.setState({
              graph: undefined,
            });
          }}
        />
        <BakingForm
          header={{
            tanggal: this.state.tanggal,
            shiftr: this.state.shiftr,
            shiftLabel: this.getShiftName(this.state.shiftr),
            typeprod: this.state.typeprod,
            typeprodLabel: this.getProductName(this.state.typeprod),
            mesin: this.state.mesin,
            mesinLabel: this.getMesinName(this.state.mesin),
            min_weight_baking: this.state.headerData
              ? this.state.headerData.min_weight_baking
              : undefined,
            max_weight_baking: this.state.headerData
              ? this.state.headerData.max_weight_baking
              : undefined,
            min_diameter_baking: this.state.headerData
              ? this.state.headerData.min_diameter_baking
              : undefined,
            max_diameter_baking: this.state.headerData
              ? this.state.headerData.max_diameter_baking
              : undefined,
            min_length_baking: this.state.headerData
              ? this.state.headerData.min_length_baking
              : undefined,
            max_length_baking: this.state.headerData
              ? this.state.headerData.max_length_baking
              : undefined,
            min_wide_baking: this.state.headerData
              ? this.state.headerData.min_wide_baking
              : undefined,
            max_wide_baking: this.state.headerData
              ? this.state.headerData.max_wide_baking
              : undefined,
            min_height_baking: this.state.headerData
              ? this.state.headerData.min_height_baking
              : undefined,
            max_height_baking: this.state.headerData
              ? this.state.headerData.max_height_baking
              : undefined,
          }}
          data={this.state.formData}
          show={this.state.formData !== undefined}
          onAfterDelete={(id_baking_detail) => {
            const currDatas = [...this.state.datas];
            const newDatas = [];
            for (let i = 0; i < currDatas.length; i++) {
              if (
                parseInt(currDatas[i].id_baking_detail) !==
                parseInt(id_baking_detail)
              ) {
                newDatas.push(currDatas[i]);
              }
            }
            this.setState({
              exists: this.removeExistData(
                this.state.typeprod,
                this.state.mesin,
                this.state.shiftr
              ),
              datas: newDatas,
              formData: undefined,
            });
          }}
          onAfterEdit={(data) => {
            const currDatas = [...this.state.datas];
            for (let i = 0; i < currDatas.length; i++) {
              if (currDatas[i].id === data.id_baking_detail) {
                currDatas[i] = data;
              }
            }
            this.setState({
              datas: currDatas,
              formData: undefined,
            });
          }}
          onAfterInsert={(datas) => {
            this.setState({
              exists: this.addExistData(
                this.state.typeprod,
                this.state.mesin,
                this.state.shiftr
              ),
              datas: datas,
              formData: undefined,
            });
          }}
          onClose={() => {
            this.setState({
              formData: undefined,
            });
          }}
        />
      </>
    );
  }
}

export default baking;
