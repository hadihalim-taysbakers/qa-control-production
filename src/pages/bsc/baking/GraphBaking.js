import React from "react";
import { Line } from "react-chartjs-2";
import moment from "moment";
import "moment/locale/id";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler,
} from "chart.js";
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler
);
class Graph extends React.Component {
  render() {
    return (
      <div
        className={this.props.show ? "modal  fade show" : "modal"}
        style={this.props.show ? { display: "block" } : undefined}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{this.props.title}</h5>
              {this.props.onClose ? (
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                  disabled={this.props.loading}
                  onClick={this.props.onClose}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              ) : undefined}
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-sm-3">
                  <b>Date : </b>
                  <br />
                  {moment(this.props.tanggal)
                    .locale("id")
                    .format("dddd, DD MMM YYYY")}
                </div>
                <div className="col-sm-3">
                  <b>Shift : </b>
                  <br />
                  {this.props.shift}
                </div>
                <div className="col-sm-3">
                  <b>Mesin : </b>
                  <br />
                  {this.props.mesin}
                </div>
                <div className="col-sm-3">
                  <b>Type Production : </b>
                  <br />
                  {this.props.product}
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12">
                  {this.props.data ? (
                    <Line
                      data={this.props.data}
                      options={{
                        responsive: true,
                        plugins: {
                          legend: {
                            position: "top",
                          },
                          title: {
                            display: true,
                            text: this.props.subtitle,
                          },
                        },
                      }}
                    />
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Graph;
