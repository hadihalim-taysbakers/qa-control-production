import React from "react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";

class menu extends React.Component {
  render() {
    return (
      <ul className="navbar-nav">
        <li className="nav-item">
          <Link
            to="/"
            className={
              this.props.location.pathname === "/"
                ? "nav-link active"
                : "nav-link"
            }
          >
            Dashboard
          </Link>
        </li>
        <li className="nav-item dropdown">
          <a
            className={
              this.props.location.pathname.startsWith("/bsc") === false
                ? "nav-link dropdown-toggle"
                : "nav-link dropdown-toggle active"
            }
            href="/"
            onClick={(e) => {
              e.preventDefault();
              return false;
            }}
            id="navbarDropdownMenuLink"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            BSC
          </a>
          <ul
            className="dropdown-menu"
            aria-labelledby="navbarDropdownMenuLink"
          >
            <li>
              <Link
                to="/bsc/dashboard"
                className={
                  this.props.location.pathname === "/bsc/dashboard"
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Dashboard
              </Link>
            </li>
            <li>
              <Link
                to="/bsc/raw-material"
                className={
                  this.props.location.pathname === "/bsc/raw-material"
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Proses Bahan Baku
              </Link>
            </li>
            <li>
              <Link
                to="/bsc/moulding"
                className={
                  this.props.location.pathname === "/bsc/moulding"
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Proses Molding
              </Link>
            </li>
            <li>
              <Link
                to="/bsc/baking"
                className={
                  this.props.location.pathname === "/bsc/baking"
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Proses Baking
              </Link>
            </li>
            <li>
              <Link
                to="/bsc/production-result"
                className={
                  this.props.location.pathname === "/bsc/production-result"
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Proses Oil & Seasoning
              </Link>
            </li>
            <li>
              <Link
                to="/bsc/sealing"
                className={
                  this.props.location.pathname === "/bsc/sealing"
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Proses Sealing
              </Link>
            </li>
            <li>
              <Link
                to="/bsc/finish-goods"
                className={
                  this.props.location.pathname === "/bsc/finish-goods"
                    ? "dropdown-item active"
                    : "dropdown-item"
                }
              >
                Proses Finish Good
              </Link>
            </li>
          </ul>
        </li>
      </ul>
    );
  }
}
export default withRouter(menu);
