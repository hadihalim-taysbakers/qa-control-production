import React from "react";
import Menu from "./menu";
class main extends React.Component {
  render() {
    return (
      <>
        <nav className="navbar sticky-top navbar-expand-lg  navbar-dark bg-primary">
          <div className="container-fluid">
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNavDropdown"
              aria-controls="navbarNavDropdown"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavDropdown">
              <Menu />
            </div>
          </div>
        </nav>
        {this.props.children}
      </>
    );
  }
}
export default main;
