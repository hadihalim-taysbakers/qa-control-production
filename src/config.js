const config = {
  // api: "http://localhost/buatqc/",
  api: "https://twistxd.com/apiqcjs/",
  path: {
    sealing: "koneksi_sealing.php",
    rm: "koneksi_rm.php",
    season: "koneksi_season.php",
    baking: "koneksi_baking.php",
    moulding: "koneksi_qc.php",
    dashboard_bsc: "dashboard_bsc.php",
    master_shift: "select_shift.php",
    master_mesin: "select_mesin.php",
    master_product: "select_product.php",
    master_product2: "select_product2.php",
    master_variant: "select_variant.php",
    master_bahanbaku: "select_bahanbaku.php",
    fg: "koneksi_fg.php",
  },
  page: {
    sealing: {
      posisi: "bscpacking",
    },
    fg: {
      posisi: "bscpacking",
    },
    baking: {
      posisi: "bscoven",
    },
    moulding: {
      posisi: "bscoven",
    },
    season: {
      posisi: "bscoven",
    },
  },
};

export default config;
