import React from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import Raw_Material from "./../../pages/bsc/rm/raw_material";
import Qc from "./../../pages/bsc/qc/qc";
import Baking from "./../../pages/bsc/baking/baking";
import ProductionResult from "./../../pages/bsc/pr/production_result";
import Sealing from "./../../pages/bsc/sealing/sealing";
import FinishGoods from "./../../pages/bsc/fg/finish_good";
import BscDashboard from "../../pages/bsc/dashboard";
import Page404 from "./../../pages/Page404";
function BscRoute() {
  const { path } = useRouteMatch();

  return (
    <>
      <Switch>
        <Route path={`${path}/dashboard`} component={BscDashboard} />
        <Route exact path={`${path}/raw-material`} component={Raw_Material} />
        <Route exact path={`${path}/moulding`}>
          <Qc />
        </Route>
        <Route exact path={`${path}/baking`}>
          <Baking />
        </Route>
        <Route exact path={`${path}/production-result`}>
          <ProductionResult />
        </Route>
        <Route exact path={`${path}/finish-goods`}>
          <FinishGoods />
        </Route>
        <Route exact path={`${path}/sealing`}>
          <Sealing />
        </Route>
        <Route path="*">
          <Page404 />
        </Route>
      </Switch>
    </>
  );
}
export default BscRoute;
