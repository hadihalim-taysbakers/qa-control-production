import React from "react";
import * as Validator from "validatorjs";

class Field extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: undefined,
      errors: {},
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    if (this.props.rules) {
      const check = this.validateField(name, value);
      if (check.valid === true) {
        this.setState({
          errors: { ...this.state.errors, [name]: [] },
        });
      } else {
        this.setState({
          errors: { ...this.state.errors, [name]: check.errors[name] },
        });
      }
    }
  };
  validateField(name, value) {
    const messages = Validator.getMessages("id");
    Validator.setMessages("id", messages);
    const validation = new Validator(
      { [name]: value },
      { [name]: this.props.rules },
      messages
    );
    validation.setAttributeNames({ [this.props.name]: this.props.label });
    if (validation.passes()) {
      return { valid: true, errors: null };
    } else {
      const errors = validation.errors.all();
      return {
        valid: false,
        errors: errors,
      };
    }
  }
  generateClassName() {
    return this.state.errors && this.state.errors[this.props.name]
      ? this.state.errors[this.props.name].length === 0
        ? this.props.className
          ? this.props.className + " is-valid"
          : "is-valid"
        : this.props.className
        ? this.props.className + " is-invalid"
        : "is-invalid"
      : this.props.className;
  }
  renderInput() {
    switch (this.props.type) {
      case "time":
        return (
          <input
            {...this.props.attributes}
            type="time"
            name={this.props.name}
            onChange={this.handleChange}
            className={this.generateClassName()}
          />
        );
      default:
        return (
          <input
            {...this.props.attributes}
            name={this.props.name}
            onChange={this.handleChange}
            className={this.generateClassName()}
          />
        );
    }
  }
  renderError() {
    const arrcomp = [];
    const errors = this.state.errors
      ? this.state.errors[this.props.name]
      : undefined;
    if (errors) {
      errors.map(function (currentValue) {
        arrcomp.push(<div className="invalid-feedback">{currentValue}</div>);
        return currentValue;
      });
    }
    return arrcomp;
  }
  render() {
    return (
      <>
        {this.renderInput()}
        {this.renderError()}
      </>
    );
  }
}
export default Field;
