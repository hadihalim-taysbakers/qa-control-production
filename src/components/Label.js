import { FormContext } from "./Form";
import React from "react";
export default function Label(props) {
  return (
    <FormContext.Consumer>
      {({ fields }) =>
        fields[props.name] ? (
          <label {...props} htmlFor={fields[props.name].id} forHtml={props.name}>
            {fields[props.name].label}
          </label>
        ) : (
          undefined
        )
      }
    </FormContext.Consumer>
  );
}
