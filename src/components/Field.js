import { FormContext } from "./Form";
import React from "react";
import Select from "react-select";
export default function Field(props) {
  const getAttributes = (field) => {
    const attr = { ...field, ...props };

    delete attr.rules;
    delete attr.label;
    delete attr.attributeName;
    delete attr.values;
    delete attr.options;
    delete attr.container;
    return attr;
  };

  const handleChange = (validateField) => {
    return (event) => {
      const name = event.target.name;
      validateField(name);
      if (props.onChange) props.onChange(event);
    };
  };

  // onChange={(e) => {
  //   this.setState({ mesin: e.value });
  // }}
  const generateClassName = (className, touched, errors, rules) => {
    if (rules && touched) {
      if (errors === undefined) {
        return className ? className + " is-valid" : "is-valid";
      } else {
        if (errors.length > 0) {
          return className ? className + " is-invalid" : "is-invalid";
        } else {
          return className ? className + " is-valid" : "is-valid";
        }
      }
    } else {
      return className;
    }
  };

  const renderField = (
    field,
    errors,
    touched,
    validateField,
    defaultValue,
    loading
  ) => {
    switch (field.type) {
      case "radio":
        const values = field.values
          ? JSON.parse(JSON.stringify(field.values))
          : {};
        return (
          <div
            className={generateClassName(
              props.className,
              touched,
              errors,
              field.rules
            )}
          >
            {Object.keys(values).map((key) => (
              <div
                className={generateClassName(
                  "form-check form-check-inline",
                  touched,
                  errors,
                  field.rules
                )}
                key={field.name + "-" + key}
              >
                <input
                  {...getAttributes(field)}
                  id={field.name + "-" + key}
                  value={key}
                  disabled={loading}
                  type="radio"
                  defaultChecked={defaultValue === key ? true : false}
                  onChange={handleChange(validateField)}
                  className={generateClassName(
                    "form-check-input",
                    touched,
                    errors,
                    field.rules
                  )}
                />
                <label
                  className={generateClassName(
                    "form-check-label",
                    touched,
                    errors,
                    field.rules
                  )}
                  htmlFor={field.name + "-" + key}
                >
                  {values[key]}
                </label>
              </div>
            ))}
            {errors ? renderError(errors) : undefined}
          </div>
        );
      case "checkbox":
        return (
          <>
            <input name={field.name} value={field.values[0]} type="hidden" />
            <input
              {...getAttributes(field)}
              value={field.values[1]}
              disabled={loading}
              type="checkbox"
              defaultChecked={defaultValue === field.values[1] ? true : false}
              onChange={handleChange(validateField)}
              className={generateClassName(
                props.className,
                touched,
                errors,
                field.rules
              )}
            />
          </>
        );

      case "time":
        return (
          <input
            {...getAttributes(field)}
            type="time"
            readOnly={loading}
            defaultValue={defaultValue}
            onChange={handleChange(validateField)}
            className={generateClassName(
              props.className,
              touched,
              errors,
              field.rules
            )}
          />
        );
      case "text":
        return (
          <input
            {...getAttributes(field)}
            type="text"
            readOnly={loading}
            defaultValue={defaultValue}
            onChange={handleChange(validateField)}
            className={generateClassName(
              props.className,
              touched,
              errors,
              field.rules
            )}
          />
        );
      case "textarea":
        return (
          <textarea
            {...getAttributes(field)}
            type="text"
            readOnly={loading}
            onChange={handleChange(validateField)}
            className={generateClassName(
              props.className,
              touched,
              errors,
              field.rules
            )}
            defaultValue={defaultValue}
          ></textarea>
        );
      case "select":
        const options = [...props.options];
        return (
          <select
            {...getAttributes(field)}
            readOnly={loading}
            className={generateClassName(
              props.className
                ? props.className + " form-select"
                : "form-select",
              touched,
              errors,
              field.rules
            )}
            defaultValue={defaultValue}
            onChange={handleChange(validateField)}
          >
            <option value="">Please select</option>
            {options.map((option, index) => {
              return <option value={option.value}>{option.label}</option>;
            })}
          </select>
          // <Select
          //   {...getAttributes(field)}
          //   readOnly={loading}
          //   defaultValue={getValueSelect(options,defaultValue)}
          //   onChange={handleChangeSelect(validateField)}
          //   options={options}
          //   className={generateClassName(
          //     props.className,
          //     touched,
          //     errors,
          //     field.rules
          //   )}
          //   // onChange={(e) => {
          //   //   this.setState({ mesin: e.value });
          //   // }}
          // />
        );
      case "label":
        return (
          <input
            {...getAttributes(field)}
            type="text"
            readOnly
            defaultValue={defaultValue}
            onChange={handleChange(validateField)}
            className={generateClassName(
              props.className,
              touched,
              errors,
              field.rules
            )}
          />
        );
      default:
        return (
          <input
            {...getAttributes(field)}
            defaultValue={defaultValue}
            readOnly={loading}
            onChange={handleChange(validateField)}
            className={generateClassName(
              props.className,
              touched,
              errors,
              field.rules
            )}
          />
        );
    }
  };
  const renderError = (errors) => {
    const arrcomp = [];

    errors.map(function (currentValue, index) {
      arrcomp.push(
        <div
          className="invalid-feedback"
          key={"error-" + props.name + "-" + index}
        >
          {currentValue}
        </div>
      );
      return currentValue;
    });

    return arrcomp;
  };

  return (
    <FormContext.Consumer>
      {({ fields, validateField, errors, touches, defaultValues, loading }) => (
        <>
          {fields && fields[props.name]
            ? renderField(
                fields[props.name],
                errors ? errors[props.name] : undefined,
                touches[props.name],
                validateField,
                defaultValues ? defaultValues[props.name] : undefined,
                loading
              )
            : undefined}
          {errors &&
          errors[props.name] &&
          errors[props.name].length > 0 &&
          fields[props.name].type !== "radio"
            ? renderError(errors[props.name])
            : undefined}
        </>
      )}
    </FormContext.Consumer>
  );
}
