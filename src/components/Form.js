import React, { useEffect, useState, useCallback, useRef } from "react";
import usePrevious from "../utils/usePrevious";
import Validator from "../utils/validator";

export const FormContext = React.createContext({
  loading: false,
  rules: undefined,
  data: undefined,
  touches: undefined,
  validateField: undefined,
  submit: undefined,
  validate: undefined,
  errors: {},
});

const Form = ({ children, fields, onSubmit, loading, defaultValues }) => {
  const messages = Validator.getMessages("id");
  const [errors, setErrors] = useState({});
  const [touches, setTouches] = useState({});
  const refForm = useRef(null);

  const prevFields = usePrevious(fields);
  const getAttibuteNames = useCallback(() => {
    const fieldNames = {};
    Object.keys(fields).forEach((key) => {
      fieldNames[key] = fields[key].attributeName;
    });
    return fieldNames;
  }, [fields]);
  const getDatas = useCallback(() => {
    const formData = new FormData(refForm.current);
    const datas = {};
    formData.forEach(function (value, key) {
      datas[key] = value;
    });

    //refFormF.current.onSubmit = handleFormSubmit;
    return datas;
  }, [refForm]);
  const validateField = useCallback(
    (name) => {
      setTouches((t) => ({ ...t, [name]: true }));
      if (fields[name] && fields[name].rules) {
        const datas = getDatas();
        const rules = { [name]: fields[name].rules };
        const validation = new Validator(datas, rules, messages);
        validation.setAttributeNames(getAttibuteNames());
        if (validation.passes()) {
          setErrors((t) => ({ ...t, [name]: undefined }));
        } else {
          const validationerrors = validation.errors.all();
          setErrors((t) => ({ ...t, [name]: validationerrors[name] }));
        }
      }
    },
    [fields, getAttibuteNames, messages, getDatas]
  );

  const onReset = () => {
    if (fields) {
      const newerrors = {};
      const newtouches = {};
      Object.keys(fields).forEach((key) => {
        newerrors[key] = errors[key] !== undefined ? errors[key] : undefined;
        newtouches[key] = newtouches[key] !== undefined ? touches[key] : false;
      });
      setErrors(newerrors);
      setTouches(newtouches);
    }
  };
  useEffect(() => {
    if (JSON.stringify(fields) !== JSON.stringify(prevFields)) {
      const newerrors = {};
      const newtouches = {};
      Object.keys(fields).forEach((key) => {
        newerrors[key] = errors[key] !== undefined ? errors[key] : undefined;
        newtouches[key] = newtouches[key] !== undefined ? touches[key] : false;
      });
      setErrors(newerrors);
      setTouches(newtouches);
    }
  }, [fields, prevFields, errors, touches]);
  const runvalidate = (datas) => {
    const rules = {};
    Object.entries(fields).forEach((entry) => {
      const [key, value] = entry;
      if (value.rules) rules[key] = value.rules;
    });
    const validation = new Validator(datas, rules, messages);
    validation.setAttributeNames(getAttibuteNames());
    const newerrors = {};
    const newtouches = {};
    Object.keys(fields).forEach((key) => {
      newerrors[key] = undefined;
      newtouches[key] = true;
    });
    setTouches(newtouches);
    if (validation.passes()) {
      setErrors(newerrors);
      return true;
    } else {
      const validationerrors = validation.errors.all();
      setErrors({ ...newerrors, ...validationerrors });
      return false;
    }
  };
  const submit = () => {
    const datas = getDatas();
    if (runvalidate(datas)) {
      if (onSubmit) onSubmit(datas);
    }
  };
  const validate = () => {
    const datas = getDatas();
    return runvalidate(datas);
  };
  const handleFormSubmit = (e) => {
    e.preventDefault();
    submit();
  };
  return (
    <FormContext.Provider
      value={{
        fields,
        defaultValues,
        errors,
        touches,
        loading,
        validateField,
        submit,
        validate,
      }}
    >
      <form
        ref={refForm}
        onSubmit={handleFormSubmit}
        onReset={onReset}
        noValidate
      >
        {children}
      </form>
    </FormContext.Provider>
  );
};
export default Form;
